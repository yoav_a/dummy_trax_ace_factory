from Trax.Algo.Calculations.Core.Shortcuts import BaseCalculationsGroup
from Trax.Utils.Logging.Logger import Log


class Survey(object):

    """
    this class handles all general operation needed for Survey KPI
    """

    def __init__(self, data_provider, output, survey_template=None, **kwargs):
        """

        :param data_provider:
        :param output:
        :param survey_template:  data frame object of Survey
        :param kwargs:
        """
        self.data_provider = data_provider
        self.k_engine = BaseCalculationsGroup(self.data_provider, output)
        self.survey_response = self.data_provider.survey_responses
        self.survey_template = survey_template
        for data in kwargs.keys():
            setattr(self, data, kwargs[data])

    def get_survey_answer(self, survey_data, answer_field=None):
        """
        :param survey_data:     1) str - The name of the survey in the DB.
                                2) tuple - (The field name, the field value). For example: ('question_fk', 13)
        :param answer_field: The DB field from which the answer is extracted. Default is the usual hierarchy.
        :return: The required survey response.
        """
        if not isinstance(survey_data, (list, tuple)):
            entity = 'question_text'
            value = survey_data
        else:
            entity, value = survey_data
        survey = self.survey_response[self.survey_response[entity] == value]
        if survey.empty:
            return None
        survey = survey.iloc[0]
        if answer_field is None or answer_field not in survey.keys():
            answer_field = 'selected_option_text' if survey['selected_option_text'] else 'number_value'
        survey_answer = survey[answer_field]
        return survey_answer

    def check_survey_answer(self, survey_text, target_answer, min_required_answer=None):
        """
        :param min_required_answer:
        :param survey_text:     1) str - The name of the survey in the DB.
                                2) tuple - (The field name, the field value). For example: ('question_fk', 13)
        :param target_answer: The required answer/s for the KPI to pass.
        :return: True if the answer matches the target; otherwise - False.
        """
        if not isinstance(survey_text, (list, tuple)):
            entity = 'question_text'
            value = survey_text
        else:
            entity, value = survey_text
        value = [value] if not isinstance(value, list) else value
        survey_data = self.survey_response[self.survey_response[entity].isin(value)]
        if survey_data.empty:
            Log.warning('Survey with {} = {} doesn\'t exist'.format(entity, value))
            return False
        answer_field = 'selected_option_text' if not survey_data['selected_option_text'].empty else 'number_value'
        if ',' in target_answer:
            target_answers = target_answer.split(',')
        else:
            target_answers = [target_answer] if not isinstance(target_answer, (list, tuple)) else target_answer
        survey_answers = survey_data[answer_field].values.tolist()
        count_successful = 0
        if min_required_answer is not None:
            for answer in target_answers:
                if answer in survey_answers:
                    return True
            return False
        else:
            for answer in target_answers:
                if answer in survey_answers:
                    count_successful += 1
            if count_successful == len(survey_answers):
                return True
            else:
                return False
