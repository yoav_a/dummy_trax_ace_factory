from Trax.Algo.Calculations.Core.DataProvider import Data

from KPIUtils_v2.Calculations.CalculationsUtils.GENERALToolBoxCalculations import GENERALToolBox
from KPIUtils_v2.Calculations.PositionGraphsCalculations import PositionGraphs
from Trax.Utils.Logging.Logger import Log


class Block(object):
    EXCLUDE_FILTER = 0
    INCLUDE_FILTER = 1
    CONTAIN_FILTER = 2
    EXCLUDE_EMPTY = False
    INCLUDE_EMPTY = True

    STRICT_MODE = ALL = 1000

    EMPTY = 'Empty'
    DEFAULT = 'Default'
    TOP = 'Top'
    BOTTOM = 'Bottom'

    def __init__(self, data_provider, ignore_stacking=True, rds_conn=None, front_facing=False):
        self.data_provider = data_provider
        self._position_graphs = PositionGraphs(self.data_provider)
        self.ignore_stacking = ignore_stacking
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.rds_conn = rds_conn
        self.scenes_info = self.data_provider[Data.SCENES_INFO]
        self.front_facing = front_facing
        self.match_product_in_scene = self._position_graphs.match_product_in_scene
        self.toolbox = GENERALToolBox(self.data_provider)

    @property
    def position_graphs(self):
        if not hasattr(self, '_position_graphs'):
            self._position_graphs = PositionGraphs(self.data_provider, rds_conn=self.rds_conn)
        return self._position_graphs

    def calculate_block_edges(self, minimum_block_ratio=0.9, allowed_products_filters=None,
                              include_empty=EXCLUDE_EMPTY, **filters):
        """
        :param minimum_block_ratio:
        :param number_of_allowed_others: Number of allowed irrelevant facings between two cluster of relevant facings.
        :param filters: The relevant facings of the block.
        :return: This function calculates the number of 'flexible blocks' per scene, meaning, blocks which are allowed
                 to have a given number of irrelevant facings between actual chunks of relevant facings.
        """
        edges = []
        filters, relevant_scenes = self.toolbox.separate_location_filters_from_product_filters(**filters)
        product_list = self.toolbox._get_group_product_list(filters)

        if len(relevant_scenes) == 0:
            Log.debug('Block Together: No relevant SKUs were found for these filters {}'.format(product_list))
        for scene in relevant_scenes:

            scene_graph = self.position_graphs.get(scene).copy()
            clusters, scene_graph = self.get_scene_blocks(scene_graph,
                                                          allowed_products_filters=allowed_products_filters,
                                                          include_empty=include_empty, **{'product_fk': product_list})
            new_relevant_vertices = self.filter_vertices_from_graph(scene_graph, **{'product_fk': product_list})

            for cluster in clusters:
                relevant_vertices_in_cluster = set(cluster).intersection(new_relevant_vertices)
                if len(new_relevant_vertices) > 0:
                    cluster_ratio = len(relevant_vertices_in_cluster) / float(len(new_relevant_vertices))
                else:
                    cluster_ratio = 0
                if cluster_ratio >= minimum_block_ratio:
                    cluster.sort(reverse=True)
                    edges = self.get_block_edges(scene_graph.copy().vs[cluster])
                    break
        return edges

    def get_block_edges(self, graph):
        """
        This function receives one or more vertex data of a block's graph, and returns the range of its edges -
        The far most top, bottom, left and right pixels of its facings.
        """
        top = right = bottom = left = None

            # top = graph.get_attribute_values(self.position_graphs.TOP)
            # top_index = min(xrange(len(top)), key=top.__getitem__)
            # graph.get_attribute_values(self.position_graphs.RECT_X)[top_index]
        top = min(graph.get_attribute_values(self.position_graphs.RECT_Y))
        right = max(graph.get_attribute_values(self.position_graphs.RECT_X))
        bottom = max(graph.get_attribute_values(self.position_graphs.RECT_Y))
        left = min(graph.get_attribute_values(self.position_graphs.RECT_X))
        result = {'visual': {'top': top, 'right': right, 'bottom': bottom, 'left': left}}
        result.update({'shelfs': list(set(graph.get_attribute_values('shelf_number')))})
        return result

    def get_scene_blocks(self, graph, allowed_products_filters=None, include_empty=EXCLUDE_EMPTY, **filters):
        """
        This function is a sub-function for Block Together. It receives a graph and filters and returns a list of
        clusters.
        """
        relevant_vertices = set(self.filter_vertices_from_graph(graph, **filters))
        if allowed_products_filters:
            allowed_vertices = self.filter_vertices_from_graph(graph, **allowed_products_filters)
        else:
            allowed_vertices = set()

        if include_empty == self.EXCLUDE_EMPTY:
            empty_vertices = {v.index for v in graph.vs.select(product_type='Empty')}
            allowed_vertices = set(allowed_vertices).union(empty_vertices)

        all_vertices = {v.index for v in graph.vs}
        vertices_to_remove = all_vertices.difference(relevant_vertices.union(allowed_vertices))
        graph.delete_vertices(vertices_to_remove)
        # removing clusters including 'allowed' SKUs only
        blocks = [block for block in graph.clusters() if set(block).difference(allowed_vertices)]
        return blocks, graph

    def filter_vertices_from_graph(self, graph, **filters):
        """
        This function is given a graph and returns a set of vertices calculated by a given set of filters.
        """
        vertices_indexes = None
        for field in filters.keys():
            field_vertices = set()
            values = filters[field] if isinstance(filters[field], (list, tuple)) else [filters[field]]
            for value in values:
                vertices = [v.index for v in graph.vs.select(**{field: value})]
                field_vertices = field_vertices.union(vertices)
            if vertices_indexes is None:
                vertices_indexes = field_vertices
            else:
                vertices_indexes = vertices_indexes.intersection(field_vertices)
        vertices_indexes = vertices_indexes if vertices_indexes is not None else [v.index for v in graph.vs]
        if self.front_facing:
            front_facing_vertices = [v.index for v in graph.vs.select(front_facing='Y')]
            vertices_indexes = set(vertices_indexes).intersection(front_facing_vertices)
        return list(vertices_indexes)

    def calculate_block_together(self, allowed_products_filters=None, include_empty=EXCLUDE_EMPTY,
                                 minimum_block_ratio=0.9, result_by_scene=False, block_of_blocks=False,
                                 block_products1=None, block_products2=None, vertical=False, biggest_block=False,
                                 n_cluster=None, **filters):
        """
        :param biggest_block:
        :param block_products1:
        :param block_products2:
        :param block_of_blocks:
        :param vertical: if needed to check vertical block by average shelf
        :param allowed_products_filters: These are the parameters which are allowed to corrupt the block without failing it.
        :param include_empty: This parameter dictates whether or not to discard Empty-typed products.
        :param minimum_block_ratio: The minimum (block number of facings / total number of relevant facings) ratio
                                    in order for KPI to pass (if ratio=1, then only one block is allowed).
        :param result_by_scene: True - The result is a tuple of (number of passed scenes, total relevant scenes);
                                False - The result is True if at least one scene has a block, False - otherwise.
        :param filters: These are the parameters which the blocks are checked for.
        :return: see 'result_by_scene' above.
        """
        filters, relevant_scenes = self.toolbox.separate_location_filters_from_product_filters(**filters)
        if len(relevant_scenes) == 0:
            if result_by_scene:
                return 0, 0
            elif vertical:
                return False, 0
            else:
                Log.debug('Block Together: No relevant SKUs were found for these filters {}'.format(filters))
                return False
        number_of_blocked_scenes = 0
        cluster_ratios = []
        for scene in relevant_scenes:
            scene_graph = self.position_graphs.get(scene).copy()
            clusters, scene_graph = self.get_scene_blocks(scene_graph,
                                                          allowed_products_filters=allowed_products_filters,
                                                          include_empty=include_empty, **filters)

            if block_of_blocks:
                new_relevant_vertices1 = self.filter_vertices_from_graph(scene_graph, **block_products1)
                new_relevant_vertices2 = self.filter_vertices_from_graph(scene_graph, **block_products2)
            else:
                new_relevant_vertices = self.filter_vertices_from_graph(scene_graph, **filters)
            for cluster in clusters:
                if block_of_blocks:
                    relevant_vertices_in_cluster1 = set(cluster).intersection(new_relevant_vertices1)
                    if len(new_relevant_vertices1) > 0:
                        cluster_ratio1 = len(relevant_vertices_in_cluster1) / float(len(new_relevant_vertices1))
                    else:
                        cluster_ratio1 = 0
                    relevant_vertices_in_cluster2 = set(cluster).intersection(new_relevant_vertices2)
                    if len(new_relevant_vertices2) > 0:
                        cluster_ratio2 = len(relevant_vertices_in_cluster2) / float(len(new_relevant_vertices2))
                    else:
                        cluster_ratio2 = 0
                    if cluster_ratio1 >= minimum_block_ratio and cluster_ratio2 >= minimum_block_ratio:
                        return True
                else:
                    relevant_vertices_in_cluster = set(cluster).intersection(new_relevant_vertices)
                    if len(new_relevant_vertices) > 0:
                        cluster_ratio = len(relevant_vertices_in_cluster) / float(len(new_relevant_vertices))
                    else:
                        cluster_ratio = 0
                    cluster_ratios.append(cluster_ratio)
                    if biggest_block:
                        continue
                    if cluster_ratio >= minimum_block_ratio:
                        if result_by_scene:
                            number_of_blocked_scenes += 1
                            break
                        else:
                            all_vertices = {v.index for v in scene_graph.vs}
                            non_cluster_vertices = all_vertices.difference(list(relevant_vertices_in_cluster))
                            scene_graph.delete_vertices(non_cluster_vertices)
                            if vertical:
                                return True, len(
                                    set(scene_graph.vs['shelf_number']))
                            return True
            if n_cluster is not None:
                copy_of_cluster_ratios = cluster_ratios[:]
                largest_cluster = max(copy_of_cluster_ratios)  # 39
                copy_of_cluster_ratios.remove(largest_cluster)
                if len(copy_of_cluster_ratios) > 0:
                    second_largest_integer = max(copy_of_cluster_ratios)
                else:
                    second_largest_integer = 0
                cluster_ratio = largest_cluster + second_largest_integer
                if cluster_ratio >= minimum_block_ratio:
                    if vertical:
                        return {'block': True}

            if biggest_block:
                max_ratio = max(cluster_ratios)
                biggest_cluster = clusters[cluster_ratios.index(max_ratio)]
                relevant_vertices_in_cluster = set(biggest_cluster).intersection(new_relevant_vertices)
                all_vertices = {v.index for v in scene_graph.vs}
                non_cluster_vertices = all_vertices.difference(list(relevant_vertices_in_cluster))
                scene_graph.delete_vertices(non_cluster_vertices)
                return {'block': True, 'shelf_numbers': set(scene_graph.vs['shelf_number'])}
            if result_by_scene:
                return number_of_blocked_scenes, len(relevant_scenes)
            elif vertical:
                return False, 0
            else:
                return False
