from Trax.Algo.Calculations.Core.DataProvider import Data

from KPIUtils_v2.Calculations.CalculationsUtils.GENERALToolBoxCalculations import GENERALToolBox
from KPIUtils_v2.Calculations.PositionGraphsCalculations import PositionGraphs


class Availability(object):
    EXCLUDE_FILTER = 0
    INCLUDE_FILTER = 1
    CONTAIN_FILTER = 2
    EXCLUDE_EMPTY = False
    INCLUDE_EMPTY = True

    STRICT_MODE = ALL = 1000

    EMPTY = 'Empty'
    DEFAULT = 'Default'
    TOP = 'Top'
    BOTTOM = 'Bottom'

    def __init__(self, data_provider, ignore_stacking=False, rds_conn=None, front_facing=False):
        self.data_provider = data_provider
        self._position_graphs = PositionGraphs(self.data_provider)
        self.ignore_stacking = ignore_stacking
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.facings_field = 'facings' if not self.ignore_stacking else 'facings_ign_stack'
        self.brand_field = 'brand_name'
        self.rds_conn = rds_conn
        self.scenes_info = self.data_provider[Data.SCENES_INFO]
        self.front_facing = front_facing
        self.match_product_in_scene = self._position_graphs.match_product_in_scene
        self.toolbox = GENERALToolBox(self.data_provider)

    def calculate_availability(self, **filters):
        """
        :param filters: These are the parameters which the data frame is filtered by.
        :return: Total number of SKUs facings appeared in the filtered Scene Item Facts data frame.
        """
        if set(filters.keys()).difference(self.scif.keys()):
            filtered_df = \
                self.match_product_in_scene[self.toolbox.get_filter_condition(self.match_product_in_scene, **filters)]
        else:
            filtered_df = self.scif[self.toolbox.get_filter_condition(self.scif, **filters)]
        if self.facings_field in filtered_df.columns:
            availability = filtered_df[self.facings_field].sum()
        else:
            availability = len(filtered_df)
        return availability

    def calculate_availability_by_scene(self, **filters):
        """
        :param filters: These are the parameters which the data frame is filtered by.
        :return: data frame with scene and total facings per scene.
        """
        filtered_df = self.scif[self.toolbox.get_filter_condition(self.scif, **filters)]
        availability_df = filtered_df[['scene_fk', self.facings_field]].groupby(['scene_fk']).sum()

        return availability_df
