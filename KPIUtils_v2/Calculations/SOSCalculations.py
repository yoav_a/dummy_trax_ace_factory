from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Algo.Calculations.Core.Shortcuts import BaseCalculationsGroup
from Trax.Utils.Logging.Logger import Log
from KPIUtils_v2.Calculations.CalculationsUtils.GENERALToolBoxCalculations import GENERALToolBox
from KPIUtils_v2.Calculations.PositionGraphsCalculations import PositionGraphs


class SOS:

    EXCLUDE_FILTER = 0
    INCLUDE_FILTER = 1
    CONTAIN_FILTER = 2
    EXCLUDE_EMPTY = False
    INCLUDE_EMPTY = True

    STRICT_MODE = ALL = 1000

    EMPTY = 'Empty'
    DEFAULT = 'Default'
    TOP = 'Top'
    BOTTOM = 'Bottom'

    def __init__(self, data_provider, output, ignore_stacking=False, front_facing=False, rds_conn=None):
        self.rds_conn = rds_conn
        self.data_provider = data_provider
        self._position_graphs = PositionGraphs(self.data_provider)
        self.ignore_stacking = ignore_stacking
        self.k_engine = BaseCalculationsGroup(data_provider, output)
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.facings_field = 'facings' if not self.ignore_stacking else 'facings_ign_stack'
        self.scenes_info = self.data_provider[Data.SCENES_INFO]
        self.front_facing = front_facing
        self.match_product_in_scene = self._position_graphs.match_product_in_scene
        self.toolbox = GENERALToolBox(self.data_provider)

    def calculate_sos_facing_by_scene(self, sos_filters, include_empty=False, **general_filters):
        sos_by_scene_results = {}
        if include_empty == self.EXCLUDE_EMPTY and 'product_type' not in sos_filters.keys() + general_filters.keys():
            general_filters['product_type'] = (self.EMPTY, self.EXCLUDE_FILTER)

        for scene in self.scenes_info['scene_fk'].tolist():
            try:
                scene_filter = {'scene_fk': scene}
                general_filters = self.toolbox.merge_two_dicts(general_filters, scene_filter)
                #  denominator
                pop_filter = self.toolbox.get_filter_condition(self.scif, **general_filters)
                #  nominator
                subset_filter = self.toolbox.get_filter_condition(self.scif, **sos_filters)
                try:
                    sos_by_scene_results[scene] = \
                        self.k_engine.calculate_sos_by_facings(pop_filter=pop_filter, subset_filter=subset_filter)
                except Exception as e:
                    Log.debug('calculate_sos_facing_by_scene can not be calculated for scene'
                              ' ''{} : {}'.format(scene, e.message))
                    Log.info('calculate_sos_facing_by_scene results: {}'.format(sos_by_scene_results))
                    return sos_by_scene_results

            except Exception as e:
                Log.debug('calculate_sos_facing_by_scene can not be calculated for scene ''{} : '
                          '{}'.format(scene, e.message))

    def calculate_share_of_shelf(self, sos_filters=None, include_empty=EXCLUDE_EMPTY, **general_filters):
        """
        :param sos_filters: These are the parameters on which ths SOS is calculated (out of the general DF).
        :param include_empty: This dictates whether Empty-typed SKUs are included in the calculation.
        :param general_filters: These are the parameters which the general data frame is filtered by.
        :return: The ratio of the Facings SOS.
        """
        if include_empty == self.EXCLUDE_EMPTY and 'product_type' not in sos_filters.keys() + general_filters.keys():
            general_filters['product_type'] = (self.EMPTY, self.EXCLUDE_FILTER)
        pop_filter = self.toolbox.get_filter_condition(self.scif, **general_filters)
        subset_filter = self.toolbox.get_filter_condition(self.scif, **sos_filters)

        try:
            ratio = self.k_engine.calculate_sos_by_facings(pop_filter=pop_filter, subset_filter=subset_filter)
        except Exception as e:
            Log.error(e.message)
            ratio = 0

        if not isinstance(ratio, (float, int)):
            ratio = 0
        return ratio

    def calculate_share_space_length(self, **filters):
        """
        :param filters: These are the parameters which the data frame is filtered by.
        :return: The total shelf width (in mm) the relevant facings occupy.
        """
        filtered_matches = \
            self.match_product_in_scene[self.toolbox.get_filter_condition(self.match_product_in_scene, **filters)]
        space_length = filtered_matches['width_mm_advance'].sum()
        return space_length

    def calculate_linear_share_of_shelf(self, sos_filters, include_empty=EXCLUDE_EMPTY, **general_filters):
        """
        :param sos_filters: These are the parameters on which ths SOS is calculated (out of the general DF).
        :param include_empty: This dictates whether Empty-typed SKUs are included in the calculation.
        :param general_filters: These are the parameters which the general data frame is filtered by.
        :return: The Linear SOS ratio.
        """
        if include_empty == self.EXCLUDE_EMPTY:
            general_filters['product_type'] = (self.EMPTY, self.EXCLUDE_FILTER)

        numerator_width = self.calculate_share_space_length(**dict(sos_filters, **general_filters))
        denominator_width = self.calculate_share_space_length(**general_filters)

        if denominator_width == 0:
            ratio = 0
        else:
            ratio = numerator_width / float(denominator_width)
        return ratio
