from datetime import datetime
import json
import pandas as pd
from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Logging.Logger import Log

from KPIUtils_v2.GlobalDataProvider.PsDataProvider import PsDataProvider
from KPIUtils_v2.Calculations.CalculationsUtils.GENERALToolBoxCalculations import GENERALToolBox
from KPIUtils_v2.Calculations.PositionGraphsCalculations import PositionGraphs


class Assortment(object):
    EMPTY = 'Empty'
    DEFAULT = 'Default'
    TOP = 'Top'
    BOTTOM = 'Bottom'

    STRICT_MODE = ALL = 1000

    EXCLUDE_FILTER = 0
    INCLUDE_FILTER = 1
    CONTAIN_FILTER = 2

    def __init__(self, data_provider, output, ps_data_provider=None, ignore_stacking=False):
        self.output = output
        self.data_provider = data_provider
        self.project_name = self.data_provider.project_name
        self.position_graph = PositionGraphs(self.data_provider)
        self.match_product_in_scene = self.position_graph.match_product_in_scene
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.all_products = self.data_provider[Data.ALL_PRODUCTS]
        self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.kpi_results_new_tables_queries = []
        self.ps_data_provider = ps_data_provider
        if ps_data_provider is None:
            self.ps_data_provider = PsDataProvider(data_provider, output)
        self.store_assortment = self.ps_data_provider.store_ass
        self.store_info = self.data_provider[Data.STORE_INFO]
        self.store_info = self.ps_data_provider.get_ps_store_info(self.store_info)
        self.current_date = datetime.now()
        self.LVL3_HEADERS = ['assortment_group_fk', 'assortment_fk', 'target', 'product_fk', 'in_store', 'kpi_fk_lvl1',
                             'kpi_fk_lvl2', 'kpi_fk_lvl3', 'group_target_date', 'assortment_super_group_fk',
                             'super_group_target']
        self.LVL2_HEADERS = ['assortment_super_group_fk', 'assortment_group_fk', 'assortment_fk', 'target', 'passes',
                             'total', 'kpi_fk_lvl1', 'kpi_fk_lvl2',
                             'group_target_date', 'super_group_target']
        self.LVL1_HEADERS = ['assortment_super_group_fk', 'assortment_group_fk', 'super_group_target', 'passes',
                             'total', 'kpi_fk_lvl1']
        self.ASSORTMENT_FK = 'assortment_fk'
        self.ASSORTMENT_GROUP_FK = 'assortment_group_fk'
        self.ASSORTMENT_SUPER_GROUP_FK = 'assortment_super_group_fk'
        self.ignore_stacking = ignore_stacking
        self.facings_field = 'facings' if not self.ignore_stacking else 'facings_ign_stack'
        self.toolbox = GENERALToolBox(self.data_provider)

    def get_lvl3_relevant_ass(self):
        assortment_result = pd.DataFrame(columns=self.LVL3_HEADERS)
        filters = self.LVL3_HEADERS
        if 'in_store' in filters:
            filters.remove('in_store')
        for row in self.store_assortment[['policy', 'assortment_group_fk']].drop_duplicates().itertuples():
            policies = json.loads(row.policy)
            df = self.store_info
            existing_key = True
            for key, value in policies.items():
                try:
                    if key == 'store_fk':
                        value = [int(i) for i in value]
                    df = df[df[key].isin(value)]
                except KeyError:
                    existing_key = False
                    break
            if not df.empty and existing_key:
                assortments = self.store_assortment[(self.store_assortment[self.ASSORTMENT_GROUP_FK]
                                                     == row.assortment_group_fk) & (self.store_assortment
                                                                                    [
                                                                                        'start_date'] <= self.current_date)][
                    filters]
                assortments['in_store'] = 0
                if assortment_result.empty:
                    assortment_result = assortments
                else:
                    assortment_result = assortment_result.append(assortments, ignore_index=True)
        return assortment_result.drop_duplicates(subset=[self.ASSORTMENT_GROUP_FK,
                                                                      self.ASSORTMENT_FK, 'product_fk'], keep='last')

    def calculate_lvl3_assortment(self):
        assortment_result = self.get_lvl3_relevant_ass()
        products_in_session = self.match_product_in_scene['product_fk'].values
        assortment_result.loc[assortment_result['product_fk'].isin(products_in_session), 'in_store'] = 1
        return assortment_result

    def calculate_lvl2_assortment(self, lvl3_assortment):
        lvl2_res = pd.DataFrame(columns=self.LVL2_HEADERS)
        assortments = lvl3_assortment[[self.ASSORTMENT_GROUP_FK, self.ASSORTMENT_FK, self.ASSORTMENT_SUPER_GROUP_FK]]. \
            drop_duplicates(subset=[self.ASSORTMENT_GROUP_FK, self.ASSORTMENT_FK, self.ASSORTMENT_SUPER_GROUP_FK],
                            keep='last')
        filters = self.LVL2_HEADERS
        filters.remove('passes')
        filters.remove('total')
        lvl2_res[filters] = lvl3_assortment[filters]
        lvl2_res = lvl2_res.drop_duplicates(subset=[self.ASSORTMENT_SUPER_GROUP_FK, self.ASSORTMENT_GROUP_FK, 'target'],
                                            keep='last')
        for ass in assortments.itertuples():
            lvl2_res.loc[(lvl2_res[self.ASSORTMENT_FK] == ass.assortment_fk) &
                         (lvl2_res[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk), 'passes'] = \
                len(lvl3_assortment[(lvl3_assortment[self.ASSORTMENT_FK] == ass.assortment_fk) &
                                    (lvl3_assortment[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk) &
                                    (lvl3_assortment['in_store'] == 1)])
            lvl2_res.loc[(lvl2_res[self.ASSORTMENT_FK] == ass.assortment_fk) &
                         (lvl2_res[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk), 'total'] = \
                len(lvl3_assortment[(lvl3_assortment[self.ASSORTMENT_FK] == ass.assortment_fk) &
                                    (lvl3_assortment[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk)])
        return lvl2_res

    def calculate_lvl1_assortment(self, lvl2_result):
        lvl1_res = pd.DataFrame(columns=self.LVL1_HEADERS)
        ass_super_groups = lvl2_result[~lvl2_result['kpi_fk_lvl1'].isnull()][self.ASSORTMENT_SUPER_GROUP_FK].unique()
        filters = self.LVL1_HEADERS
        filters.remove('passes')
        filters.remove('total')
        lvl1_res[filters] = \
            lvl2_result[lvl2_result[self.ASSORTMENT_SUPER_GROUP_FK].isin(ass_super_groups)][filters]
        lvl1_res = lvl1_res.drop_duplicates(subset=[self.ASSORTMENT_SUPER_GROUP_FK],
                                            keep='last')
        for group in ass_super_groups:
            lvl1_res.loc[lvl1_res[self.ASSORTMENT_SUPER_GROUP_FK] == group, 'passes'] = \
                len(lvl2_result[(lvl2_result[self.ASSORTMENT_SUPER_GROUP_FK] == group) &
                                (lvl2_result['passes'] >= lvl2_result['target'])])
            lvl1_res.loc[lvl1_res[self.ASSORTMENT_SUPER_GROUP_FK] == group, 'total'] = \
                len(lvl2_result[lvl2_result[self.ASSORTMENT_SUPER_GROUP_FK] == group])
        return lvl1_res

    def calculate_eye_level_assortment(self, eye_level_configurations=DEFAULT, min_number_of_products=ALL, **filters):
        """
        :param eye_level_configurations: A data frame containing information about shelves to ignore (==not eye level)
                                         for every number of shelves in each bay.
        :param min_number_of_products: Minimum number of eye level unique SKUs for KPI to pass.
        :param filters: This are the parameters which dictate the relevant SKUs for the eye-level calculation.
        :return: A tuple: (Number of scenes which pass, Total number of relevant scenes)
        """
        filters, relevant_scenes = self.separate_location_filters_from_product_filters(**filters)
        if len(relevant_scenes) == 0:
            return 0, 0
        if eye_level_configurations == self.DEFAULT:
            if hasattr(self, 'eye_level_configurations'):
                eye_level_configurations = self.eye_level_configurations
            else:
                Log.error('Eye-level configurations are not set up')
                return False
        number_of_products = \
            len(self.all_products[self.toolbox.get_filter_condition(self.all_products, **filters)]['product_ean_code'])
        min_shelf, max_shelf, min_ignore, max_ignore = eye_level_configurations.columns
        number_of_eye_level_scenes = 0
        for scene in relevant_scenes:
            eye_level_facings = pd.DataFrame(columns=self.match_product_in_scene.columns)
            matches = self.match_product_in_scene[self.match_product_in_scene['scene_fk'] == scene]
            for bay in matches['bay_number'].unique():
                bay_matches = matches[matches['bay_number'] == bay]
                number_of_shelves = bay_matches['shelf_number'].max()
                configuration = eye_level_configurations[(eye_level_configurations[min_shelf] <= number_of_shelves) &
                                                         (eye_level_configurations[max_shelf] >= number_of_shelves)]
                if not configuration.empty:
                    configuration = configuration.iloc[0]
                else:
                    configuration = {min_ignore: 0, max_ignore: 0}
                min_include = configuration[min_ignore] + 1
                max_include = number_of_shelves - configuration[max_ignore]
                eye_level_shelves = bay_matches[bay_matches['shelf_number'].between(min_include, max_include)]
                eye_level_facings = eye_level_facings.append(eye_level_shelves)
            eye_level_assortment = \
                len(eye_level_facings[self.toolbox.get_filter_condition(eye_level_facings,
                                                                        **filters)]['product_ean_code'])
            if min_number_of_products == self.ALL:
                min_number_of_products = number_of_products
            if eye_level_assortment >= min_number_of_products:
                number_of_eye_level_scenes += 1
        return number_of_eye_level_scenes, len(relevant_scenes)

    def shelf_level_assortment(self, min_number_of_products, shelf_target, **filters):
        filters, relevant_scenes = self.separate_location_filters_from_product_filters(**filters)
        if len(relevant_scenes) == 0:
            relevant_scenes = self.scif['scene_fk'].unique().tolist()
        number_of_products = \
            len(self.all_products[self.toolbox.get_filter_condition(self.all_products,
                                                                    **filters)]['product_ean_code'])
        result = 0  # Default score is FALSE
        for scene in relevant_scenes:
            eye_level_facings = pd.DataFrame(columns=self.match_product_in_scene.columns)
            matches = pd.merge(self.match_product_in_scene[self.match_product_in_scene['scene_fk'] == scene],
                               self.all_products, on=['product_fk'])
            for bay in matches['bay_number'].unique():
                bay_matches = matches[matches['bay_number'] == bay]
                products_in_target_shelf = bay_matches[(bay_matches['shelf_number'].isin(shelf_target)) & (
                    bay_matches['product_ean_code'].isin(number_of_products))]
                eye_level_facings = eye_level_facings.append(products_in_target_shelf)
            eye_level_assortment = \
                len(eye_level_facings[self.toolbox.get_filter_condition(eye_level_facings,
                                                                        **filters)]['product_ean_code'])
            if eye_level_assortment >= min_number_of_products:
                result = 1
        return result

    def calculate_shelf_level_assortment(self, shelves, from_top_or_bottom=TOP, **filters):
        """
        :param shelves: A shelf number (of type int or string), or a list of shelves (of type int or string).
        :param from_top_or_bottom: TOP for default shelf number (counted from top)
                                    or BOTTOM for shelf number counted from bottom.
        :param filters: These are the parameters which the data frame is filtered by.
        :return: Number of unique SKUs appeared in the filtered condition.
        """
        shelves = shelves if isinstance(shelves, list) else [shelves]
        shelves = [int(shelf) for shelf in shelves]
        if from_top_or_bottom == self.TOP:
            assortment = self.calculate_assortment(shelf_number=shelves, **filters)
        else:
            assortment = self.calculate_assortment(shelf_number_from_bottom=shelves, **filters)
        return assortment

    def separate_location_filters_from_product_filters(self, **filters):
        """
        This function gets scene-item-facts filters of all kinds, extracts the relevant scenes by the location filters,
        and returns them along with the product filters only.
        """
        relevant_scenes = \
            self.scif[self.toolbox.get_filter_condition(self.scif, **filters)]['scene_id'].unique()
        location_filters = {}
        for field in filters.keys():
            if field not in self.all_products.columns and field in self.scif.columns:
                location_filters[field] = filters.pop(field)
        return filters, relevant_scenes

    def calculate_assortment(self, assortment_entity='product_ean_code', minimum_assortment_for_entity=1, **filters):
        """
        :param assortment_entity: This is the entity on which the assortment is calculated.
        :param minimum_assortment_for_entity: This is the number of assortment per each unique entity in order for it
                                              to be counted in the final assortment result (default is 1).
        :param filters: These are the parameters which the data frame is filtered by.
        :return: Number of unique SKUs appeared in the filtered Scene Item Facts data frame.
        """
        if set(filters.keys()).difference(self.scif.keys()):
            filtered_df = \
                self.match_product_in_scene[self.toolbox.get_filter_condition(self.match_product_in_scene, **filters)]
        else:
            filtered_df = self.scif[self.toolbox.get_filter_condition(self.scif, **filters)]
        if minimum_assortment_for_entity == 1:
            assortment = len(filtered_df[assortment_entity].unique())
        else:
            assortment = 0
            for entity_id in filtered_df[assortment_entity].unique():
                assortment_for_entity = filtered_df[filtered_df[assortment_entity] == entity_id]
                if self.facings_field in filtered_df.columns:
                    assortment_for_entity = assortment_for_entity[self.facings_field].sum()
                else:
                    assortment_for_entity = len(assortment_for_entity)
                if assortment_for_entity >= minimum_assortment_for_entity:
                    assortment += 1
        return assortment

    def calculate_products_on_edge(self, min_number_of_facings=1, min_number_of_shelves=1, category=None,
                                   exclude_filter=None, **filters):
        """
        :param exclude_filter:
        :param category:
        :param min_number_of_facings: Minimum number of edge facings for KPI to pass.
        :param min_number_of_shelves: Minimum number of different shelves with edge facings for KPI to pass.
        :param filters: This are the parameters which dictate the relevant SKUs for the edge calculation.
        :return: A tuple: (Number of scenes which pass, Total number of relevant scenes)
        """
        filters, relevant_scenes = self.separate_location_filters_from_product_filters(**filters)
        if len(relevant_scenes) == 0:
            return 0, 0
        number_of_edge_scenes = 0
        for scene in relevant_scenes:
            edge_facings = pd.DataFrame(columns=self.match_product_in_scene.columns)
            matches = self.match_product_in_scene[self.match_product_in_scene['scene_fk'] == scene]
            if category:
                matches = self.match_product_in_scene[self.match_product_in_scene['category'] == category]
            if exclude_filter:
                for exclude_item in exclude_filter.keys():
                    matches = self.match_product_in_scene[self.match_product_in_scene[exclude_item] ==
                                                          exclude_filter[exclude_item]]
            for shelf in matches['shelf_number'].unique():
                shelf_matches = matches[matches['shelf_number'] == shelf]
                if not shelf_matches.empty:
                    shelf_matches = shelf_matches.sort_values(by=['bay_number', 'facing_sequence_number'])
                    edge_facings = edge_facings.append(shelf_matches.iloc[0])
                    if len(edge_facings) > 1:
                        edge_facings = edge_facings.append(shelf_matches.iloc[-1])
            edge_facings = edge_facings[self.toolbox.get_filter_condition(edge_facings, **filters)]
            if len(edge_facings) >= min_number_of_facings \
                    and len(edge_facings['shelf_number'].unique()) >= min_number_of_shelves:
                number_of_edge_scenes += 1
        return number_of_edge_scenes, len(relevant_scenes)
