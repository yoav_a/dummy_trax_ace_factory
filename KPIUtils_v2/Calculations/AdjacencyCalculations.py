from Trax.Algo.Calculations.Core.DataProvider import Data

from KPIUtils_v2.Calculations.CalculationsUtils.GENERALToolBoxCalculations import GENERALToolBox
from KPIUtils_v2.Calculations.PositionGraphsCalculations import PositionGraphs
from KPIUtils_v2.Calculations.BlockCalculations import Block
from Trax.Utils.Logging.Logger import Log


class Adjancency(object):
    EXCLUDE_FILTER = 0
    INCLUDE_FILTER = 1
    CONTAIN_FILTER = 2
    EXCLUDE_EMPTY = False
    INCLUDE_EMPTY = True

    STRICT_MODE = ALL = 1000

    EMPTY = 'Empty'
    DEFAULT = 'Default'
    TOP = 'Top'
    BOTTOM = 'Bottom'

    def __init__(self, data_provider, ignore_stacking=False, rds_conn=None, front_facing=False):
        self.data_provider = data_provider
        self._position_graphs = PositionGraphs(self.data_provider)
        self.ignore_stacking = ignore_stacking
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.rds_conn = rds_conn
        self.scenes_info = self.data_provider[Data.SCENES_INFO]
        self.front_facing = front_facing
        self.match_product_in_scene = self._position_graphs.match_product_in_scene
        self.toolbox = GENERALToolBox(self.data_provider)
        self.block = Block(data_provider=self.data_provider, rds_conn=self.rds_conn)

    def calculate_adjacency(self, filter_group_a, filter_group_b, scene_type_filter, allowed_filter,
                            allowed_filter_without_other, a_target, b_target, target):

        a_product_list = self.toolbox._get_group_product_list(filter_group_a)
        b_product_list = self.toolbox._get_group_product_list(filter_group_b)

        adjacency = self._check_groups_adjacency(a_product_list, b_product_list, scene_type_filter, allowed_filter,
                                                 allowed_filter_without_other, a_target, b_target, target)
        if adjacency:
            return 100
        return 0

    def _check_groups_adjacency(self, a_product_list, b_product_list, scene_type_filter, allowed_filter,
                                allowed_filter_without_other, a_target, b_target, target):
        a_b_union = list(set(a_product_list) | set(b_product_list))

        a_filter = {'product_fk': a_product_list}
        b_filter = {'product_fk': b_product_list}
        a_b_filter = {'product_fk': a_b_union}
        a_b_filter.update(scene_type_filter)

        matches = self.data_provider.matches
        relevant_scenes = matches[self.toolbox.get_filter_condition(matches, **a_b_filter)][
            'scene_fk'].unique().tolist()

        result = False
        for scene in relevant_scenes:
            a_filter_for_block = a_filter.copy()
            a_filter_for_block.update({'scene_fk': scene})
            b_filter_for_block = b_filter.copy()
            b_filter_for_block.update({'scene_fk': scene})
            try:
                a_products = self.toolbox.get_products_by_filters('product_fk', **a_filter_for_block)
                b_products = self.toolbox.get_products_by_filters('product_fk', **b_filter_for_block)
                if sorted(a_products.tolist()) == sorted(b_products.tolist()):
                    return False
            except:
                pass
            if a_target:
                brand_a_blocked = self.block.calculate_block_together(allowed_products_filters=allowed_filter,
                                                                      minimum_block_ratio=a_target,
                                                                      vertical=False, **a_filter_for_block)
                if not brand_a_blocked:
                    continue

            if b_target:
                brand_b_blocked = self.block.calculate_block_together(allowed_products_filters=allowed_filter,
                                                                      minimum_block_ratio=b_target,
                                                                      vertical=False, **b_filter_for_block)
                if not brand_b_blocked:
                    continue

            a_b_filter_for_block = a_b_filter.copy()
            a_b_filter_for_block.update({'scene_fk': scene})

            block = self.block.calculate_block_together(allowed_products_filters=allowed_filter_without_other,
                                                        minimum_block_ratio=target, block_of_blocks=True,
                                                        block_products1=a_filter, block_products2=b_filter,
                                                        **a_b_filter_for_block)
            if block:
                return True
        return result
