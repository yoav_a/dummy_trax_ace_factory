from datetime import datetime
from Trax.Utils.Logging.Logger import Log


class LogHandler(object):
    '''
    methods to warp the Trax logger
    '''

    def __init__(self):
        pass

    def log_runtime(self, description, log_start=False):
        '''
        decorator for logging activities

        :param description:
        :param log_start:
        :return:
        '''

        def decorator(func):
            def wrapper(*args, **kwargs):
                calc_start_time = datetime.utcnow()
                if log_start:
                    Log.info('{} started at {}'.format(description, calc_start_time))
                result = func(*args, **kwargs)
                calc_end_time = datetime.utcnow()
                Log.info('{} took {}'.format(description, calc_end_time - calc_start_time))
                return result

            return wrapper
        return decorator

log_handler = LogHandler()
