import pandas as pd
from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Algo.Calculations.Core.Shortcuts import SessionInfo, BaseCalculationsGroup
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Cloud.Services.Connector.Keys import DbUsers
from Trax.Utils.Logging.Logger import Log

__author__ = 'shanim'


class PsDataProvider:
    def __init__(self, data_provider,
                 output):  # All relevant session data with KPI static info will trigger the KPI calculation
        self.k_engine = BaseCalculationsGroup(data_provider, output)
        self.data_provider = data_provider
        self.project_name = data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.session_fk = self.data_provider.session_id
        self.rds_conn = self.rds_connection()
        self.store_area = self.get_store_area_df()
        self.output = output
        self.visit_date = self.data_provider[Data.VISIT_DATE]
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        # self.rds_conn = AwsProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.session_info = SessionInfo(data_provider)
        self.store_id = self.data_provider[Data.STORE_FK]
        self.store_area = self.get_store_area_df()
        self.store_ass = self.get_store_assortment()

    def get_store_area_df(self):
        query = """
                select sst.scene_fk, st.name, sc.session_uid from probedata.scene_store_task_area_group_items sst
                join static.store_task_area_group_items st on st.pk=sst.store_task_area_group_item_fk
                join probedata.scene sc on sc.pk=sst.scene_fk
                where sc.delete_time is null and sc.session_uid = '{}';
                """.format(self.session_uid)

        cur = self.rds_conn.db.cursor()
        cur.execute(query)
        res = cur.fetchall()

        df = pd.DataFrame(list(res), columns=['scene_fk', 'store_area_name', 'session_uid'])

        return df

    def get_store_assortment(self):
        '''
        fetch all active assortment for store type in current date

        :return:
        '''

        # TODO parse store policy and to where 1.date 2. store attreibute to match policy
        Log.info("get_store_assortment query_v2")
        query = """select p.pk as product_fk , p.name ,p.ean_code ,
                   atp.start_date,
                   pol.policy, pol.policy_type, ass.*
            from (select group3.*, ass3.kpi_fk as kpi_fk_lvl1
                from
                    (SELECT
                        group2.assortment_fk, group2.assortment_name, group2.kpi_fk as kpi_fk_lvl3, coalesce(group2.store_policy_group_fk, 
                            (select store_policy_group_fk from pservice.assortment where pk = group2.assortment_group_fk)) as store_policy_group_fk,
                        group2.assortment_group_fk, coalesce(group1.kpi_fk, 
                            (select kpi_fk from pservice.assortment where pk = group2.assortment_group_fk)) as kpi_fk_lvl2, 
                        group1.target, group1.start_date as group_target_date, group1.assortment_group_fk as assortment_super_group_fk, group1.super_group_target
                    FROM
                        (select atag1.*, ass1.assortment_name, ass1.kpi_fk, ass1.store_policy_group_fk, ass1.target as super_group_target 
                        from pservice.assortment_to_assortment_group as atag1 , 
                        pservice.assortment as ass1
                        where atag1.assortment_fk = ass1.pk and ((atag1.end_date is null and '{0}' >= atag1.start_date) or ('{0}' between atag1.start_date and atag1.end_date))) as group1
                        right join
                        (select atag2.*, ass2.assortment_name, ass2.kpi_fk, ass2.store_policy_group_fk 
                        from pservice.assortment_to_assortment_group as atag2,
                        pservice.assortment as ass2
                        where atag2.assortment_fk = ass2.pk and ((atag2.end_date is null and '{0}' >= atag2.start_date) or ('{0}' between atag2.start_date and atag2.end_date))) as group2
                        on group1.assortment_fk = group2.assortment_group_fk) as group3
                    left join
                    pservice.assortment as ass3
                    on ass3.pk = group3.assortment_super_group_fk) as ass,
                pservice.assortment_to_product as atp,
                static_new.product as p,
                pservice.policy as pol
            where atp.product_fk = p.pk and
            atp.assortment_fk = ass.assortment_fk and
            pol.pk = ass.store_policy_group_fk
            and ((atp.end_date is null and '{0}' >= atp.start_date) or ('{0}' between atp.start_date and atp.end_date));
        """.format(self.visit_date)
        cur = self.rds_conn.db.cursor()
        cur.execute(query)
        res = cur.fetchall()
        headers = ['product_fk', 'name', 'ean_code', 'start_date', 'policy', 'policy_type', 'assortment_fk',
                   'assortment_name', 'kpi_fk_lvl3', 'store_policy_group_fk', 'assortment_group_fk',
                   'kpi_fk_lvl2', 'target', 'group_target_date', 'assortment_super_group_fk', 'super_group_target', 'kpi_fk_lvl1']
        df = pd.DataFrame(list(res), columns=headers)
        return df

    def get_ps_store_info(self, store_info):
        query = """
                    SELECT s.pk as store_fk, s.additional_attribute_3, r.name as retailer, s.store_number_1,
                    s.additional_attribute_5, s.district_fk, s.test_store
                    FROM static.stores s
                    left join static.retailer r
                    on r.pk = s.retailer_fk where s.pk = '{}'
                """.format(self.store_id)
        store_data = pd.read_sql_query(query, self.rds_conn.db)
        store_info = store_info.merge(store_data, how='left', left_on='store_fk', right_on='store_fk', suffixes=['', '_1'])
        return store_info

    def get_price(self, session_id):
        query = """select product_fk, value from probedata.manual_price_collection where session_fk = {};
                """.format(session_id)
        prices_per_session = pd.read_sql_query(query, self.rds_conn.db)
        return prices_per_session

    def get_store_policies(self):
        query = """select ktp.*, p.policy as store_policy from
                    (select kt.kpi, kt.sku_name, kt.target, kt.target_validity_start_date, kt.target_validity_end_date, 
                            kt.store_policy_fk, p.policy as sos_policy 
                     from pservice.kpi_targets kt,
                          pservice.policy p
                     where kt.store_policy_fk = p.pk
                     and kt.target_validity_end_date is null) as ktp,
                   pservice.policy p
                   where ktp.sku_name = p.policy_name;
        """
        store_policies = pd.read_sql_query(query, self.rds_conn.db)
        return store_policies

    def get_sub_category(self, all_products, field ='sub_category'):
        query = """select pk as sub_category_fk, name as sub_category_name
        from static_new.sub_category
        """
        sub_categories = pd.read_sql_query(query, self.rds_conn.db)
        all_products = sub_categories.merge(all_products, how='left', left_on='sub_category_name', right_on=field, suffixes=['', '_1'])
        return all_products

    # def get_product_labels(self, all_products):
    #     query = """select pk, labels
    #     from static_new.product
    #     """
    #     sub_categories = pd.read_sql_query(query, self.rds_conn.db)
    #     all_products =
    # sub_categories.merge(all_products, how='left', left_on='pk', right_on='pk', suffixes=['', '_1'])
    #     return all_products

    def get_labels(self):
        query = """select pk, labels
        from static_new.product
        """
        labels = pd.read_sql_query(query, self.rds_conn.db)
        return labels

    def get_products_in_promotion(self):
        query = """
            select session_fk, product_fk, is_promotion from probedata.manual_price_collection where delete_time is NULL and session_fk = {}
        """.format(self.session_fk)
        cur = self.rds_conn.db.cursor()
        cur.execute(query)
        res = cur.fetchall()
        df = pd.DataFrame(list(res), columns=['session_fk', 'product_fk', 'is_promotion'])
        return df

    def get_store_last_x_visit_date(self, limit):
        query = """select visit_date from probedata.session where store_fk = {} and pk <> {} and visit_date <= '{}'
        ORDER BY visit_date desc LIMIT {}
        """.format(self.store_id, self.session_fk, self.visit_date, limit)
        store_last_x_visit_date = pd.read_sql_query(query, self.rds_conn.db)
        return store_last_x_visit_date

    def rds_connection(self):
        if not hasattr(self, '_rds_conn'):
            self._rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        try:
            pd.read_sql_query('select pk from probedata.session limit 1', self._rds_conn.db)
        except Exception as e:
            self._rds_conn.disconnect_rds()
            self._rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
            Log.error(e)
        return self._rds_conn
