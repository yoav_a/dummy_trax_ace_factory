__author__ = 'yoava'

from setuptools import setup, find_packages

setup(
  name='KPIUtils-v2',
  version='0.1',
  packages=find_packages(),
  description='all the utilities you need',
  author='Yoav Amir',
  author_email='yoava@traxretail.com',
  keywords=['trax', 'utils'],
  long_description=open('README.txt').read(),
 # url = 'https://s3.console.aws.amazon.com/s3/buckets/trax-pypi/Trax'
)
