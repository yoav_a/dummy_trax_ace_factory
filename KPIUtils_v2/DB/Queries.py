__author__ = 'Ilan'


class Queries(object):
    @staticmethod
    def get_all_kpi_data_by_kpi_set(set_name):
        return """
            select api.name as atomic_kpi_name, api.pk as atomic_kpi_fk,
                   kpi.display_text as kpi_name, kpi.pk as kpi_fk,
                   kps.name as kpi_set_name, kps.pk as kpi_set_fk
            from static.atomic_kpi api
            left join static.kpi kpi on kpi.pk = api.kpi_fk
            join static.kpi_set kps on kps.pk = kpi.kpi_set_fk
            where kps.name = '{}'
        """.format(set_name)

    @staticmethod
    def get_all_kpi_data():
        return """
            select api.name as atomic_kpi_name, api.pk as atomic_kpi_fk,
                   kpi.display_text as kpi_name, kpi.pk as kpi_fk,
                   kps.name as kpi_set_name, kps.pk as kpi_set_fk
            from static.atomic_kpi api
            left join static.kpi kpi on kpi.pk = api.kpi_fk
            join static.kpi_set kps on kps.pk = kpi.kpi_set_fk
        """

    @staticmethod
    def get_segmentation_and_region_data(store_id):
        return """
            select st.additional_attribute_2 as segmentation, r.name as region
            from static.stores st
            join static.regions r on r.pk = st.region_fk
            where st.pk = {}
        """.format(store_id)

    @staticmethod
    def get_delete_session_results_query(session_uid):
        return ("delete from report.kps_results where session_uid = '{}';".format(session_uid),
                "delete from report.kpk_results where session_uid = '{}';".format(session_uid),
                "delete from report.kpi_results where session_uid = '{}';".format(session_uid))

    @staticmethod
    def get_delete_session_results_query_by_kpi(session_uid, kpi_set):
        return (
            "delete from report.kps_results where session_uid = '{}' and kpi_set_fk = '{}';".format(session_uid,
                                                                                                    kpi_set),
            "delete report.kpk_results from report.kpk_results join static.kpi k on k.pk = "
            "report.kpk_results.kpi_fk where session_uid = '{}' and kpi_set_fk = {};".format(session_uid, kpi_set),
            "delete report.kpi_results from report.kpi_results join static.kpi k on k.pk = "
            "report.kpi_results.kpi_fk where session_uid = '{}' and kpi_set_fk = {};".format(session_uid, kpi_set))

    @staticmethod
    def get_session_id_query(session_pk):
        return """
               select session_uid from probedata.session where pk = {};  """.format(session_pk)

    @staticmethod
    def get_delete_session_results_query_from_new_tables(session_id):
        return ("delete from report.kpi_level_2_results where session_fk = '{}' and (kpi_level_2_fk "
                "in (select pk from static.kpi_level_2 where kpi_calculation_stage_fk = '3'));".format(session_id))

    @staticmethod
    def get_new_kpi_data():
        return """
                select *
                from static.kpi_level_2
            """
