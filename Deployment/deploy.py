from Deployment import fabfile
import sys

if __name__ == '__main__':
    project = sys.argv[3]
    print 'starting deployment for ', project
    fabfile.deploy(project=project)
    pass
