from Trax.Cloud.Services.Connector.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Logging.Logger import Log
from Trax.Cloud.Services.Connector.Logger import LoggerInitializer
__author__ = 'yoava'


"""
This script loads new templates to tables kpi_level_2 and kpi_view_configuration
it gets it's input from template file
It also can preform update queries
Finally , it can preform delete query on kpi_view_configuration table
"""
import pandas as pd


class LoadTables(object):

    def __init__(self, excel_path, project=None):
        self.kpi_level_2_template = pd.read_excel(excel_path, sheetname='Sheet1')
        self.kpi_view_configuration_template = pd.read_excel(excel_path, sheetname='Sheet2')
        if project is not None:
            self.project_name = project
            self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.KPI_LEVEL_2 = 'kpi_level_2'
        self.KPI_VIEW_CONFIGURATION = 'kpi_view_configuration'

    def kpi_level_2_insert_query(self, df):
        """
        this function creates query from given df to insert to kpi_level_2
        :param df: dataframe from template
        :return: query
        """
        query = "insert into static." + self.KPI_LEVEL_2 + " (client_name ," \
                                                           " kpi_family_fk , numerator_type_fk , denominator_type_fk" \
                                                           " , context_type_fk) values "
        for index, row in df.iterrows():
            query += "('" + str(row.loc['client_name']) + "' , "
            query += "'" + str(row.loc['kpi_family_fk']) + "' , "
            query += "'" + str(row.loc['numerator_type_fk']) + "' , "
            query += "'" + str(row.loc['denominator_type_fk']) + "' , "
            query += "'3') ,"
        query = query[:-1]
        query += ";"
        return query

    @staticmethod
    def get_groups(df):
        """
        get unique groups from dataframe
        :param df: original dataframe
        :return: groups
        """
        groups = set()
        for group in df['group'].unique():
            groups.add(group)
        return groups

    @staticmethod
    def get_sons_of_grandfather(group_df, top_level):
        """
        this function return all "parents" in the df
        """
        grandfather = top_level['kpi_level_2_fk']
        parents = group_df.loc[group_df['parent_kpi_fk'] == grandfather.values[0]]
        return parents

    @staticmethod
    def get_sons_of_parents(group_df, parents_df):
        """
        this function return all "sons" in the df
        """
        parents = parents_df['kpi_level_2_fk']
        sons = group_df.loc[group_df['parent_kpi_fk'].isin(parents)]
        return sons

    @staticmethod
    def prepare_query_for_kpi_view_configuration(df, query, top_level=False):
        """
        generic method to add to query values fom dataframe
        :param df: generic df to add
        :param query: query to add to
        :return: query
        """
        for index, row in df.iterrows():
            query += "('" + str(row.loc['application']) + "' ,"
            query += "'" + str(row.loc['page']) + "' ,"
            query += "'" + str(row.loc['module']) + "' ,"
            query += "'" + str(row.loc['kpi_level_2_fk']) + "' ,"
            if top_level:
                query += "NULL) ,"
            else:
                query += "'" + str(row.loc['parent_kpi_fk']) + "') ,"
            query = query[:-1]
            query += ",\n"
        return query

    def kpi_view_configuration_insert_query(self, df):
        """
        this method iterates the given template ,
        first it's extracts all unique groups in the template
        than it iterate each group in an hierarchic order (grandfather->parents->sons)
        and for each level add to the query to appropriate query
        :param df: the given template
        :return: built query
        """
        groups = self.get_groups(df)
        # iterate all groups in df
        query = self.init_insert_query()
        for group in groups:
            # get a specific group
            group_df = df.loc[df['group'] == group]
            # get 'top level' or 'grandfather' of this group
            top_level = group_df.loc[group_df['parent_kpi_fk'].isnull()]
            # build query for grandfather
            query = self.prepare_query_for_kpi_view_configuration(top_level, query, top_level=True)
            # get all sons of grandfather (parents)
            parents_df = self.get_sons_of_grandfather(group_df, top_level)
            # build query for parents
            query = self.prepare_query_for_kpi_view_configuration(parents_df, query)
            # get all sons of parents
            sons_df = self.get_sons_of_parents(group_df, parents_df)
            # build query for sons
            query = self.prepare_query_for_kpi_view_configuration(sons_df, query)

        query = query[:-3]
        query += ";"
        return query

    def init_insert_query(self):
        """
        initialize insert query to kpi_view_configuration table
        :return: initialized query
        """
        return "insert into static." + self.KPI_VIEW_CONFIGURATION + " (application , page , module ," \
                                                                     " kpi_level_2_fk , parent_kpi_fk) values"

    @staticmethod
    def update_query(table_name, condition, updates=None):
        query = "update static." + table_name
        set_query = " set "
        for key, value in updates.iteritems():
            set_query += str(key) + " = '" + str(value) + "' ,"

        query += set_query
        query = query[:-1]
        query += condition + ";"
        return query

    @staticmethod
    def delete_query(table, condition=None):
        if table == loader.KPI_LEVEL_2:
            Log.error("Can't delete from " + loader.KPI_LEVEL_2 + " table")
            return
        if condition:
            query = "delete from static." + table + " " + condition
        else:
            query = "delete from static." + table
        return query

    def execute_query(self, query):
        cur = self.rds_conn.db.cursor()
        cur.execute(query)


if __name__ == '__main__':
    LoggerInitializer.init('')
    excel_file_path = '/home/yoava/Documents/PROS-4241/template.xlsx'
    loader = LoadTables(excel_file_path, project='rbus-sand')
    first_query = loader.kpi_level_2_insert_query(loader.kpi_level_2_template)
    # second_query = loader.kpi_view_configuration_insert_query(loader.kpi_view_configuration_template)
    Log.info(first_query)
    # Log.info(second_query)
    # updates_dict = {'a': 'b', 'c': 'd', 'e': 'f'}
    # cond = "where a = '1'"
    # Log.info(loader.update_query('kpi_view_configuration', cond, updates_dict))
    # Log.info(loader.delete_query(loader.KPI_VIEW_CONFIGURATION))
    loader.execute_query(first_query)
    loader.rds_conn.db.commit()
    loader.rds_conn.disconnect_rds()
