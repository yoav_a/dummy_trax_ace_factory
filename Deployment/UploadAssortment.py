from Trax.Data.Utils.MySQLservices import get_table_insertion_query as insert
import os
import sys
import pandas as pd
import json
from datetime import datetime, timedelta

from Trax.Aws.S3Connector import BucketConnector
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Utils.Logging.Logger import Log
from Trax.Cloud.Services.Connector.Logger import LoggerInitializer
BUCKET = 'traxuscalc'

TEMPLATES_PATH = ''
SEPERATOR = 'SEPPERATOR'
STORE_ASSORTMENT_TABLE = 'pservice.custom_osa'
STORE_FK = 'store_fk'
PRODUCT_EAN = 'product_ean_code'
PRODUCT_FK = 'product_fk'
ATTRIBUTE_IN_TEMPLATE = 'ATTRIBUTE_IN_TEMPLATE'
ATTRIBUTE_IN_DB = 'ATTRIBUTE_IN_DB'
BY_STORE = 'by store'

ATTRIBUTES = {'batru':{ATTRIBUTE_IN_DB: 'store_number_1', ATTRIBUTE_IN_TEMPLATE: 'Outlet ID', SEPERATOR:'\t', BY_STORE:True},
              'marsru-prod':{ATTRIBUTE_IN_DB: 'additional_attribute_6', ATTRIBUTE_IN_TEMPLATE: 'Attribute_6', SEPERATOR:',', BY_STORE:False}}

class NewTemplate:

    def __init__(self, project, pb=False):
        self.project = project
        self.pb = pb
        self.log_suffix = '{}: '.format(self.project)
        self.update_queries = []
        self.products = self.get_products()
        self.store_data = self.get_stores_data()
        self.invalid_stores = []

    @property
    def amz_conn(self):
        if not hasattr(self, '_amz_conn'):
            self._amz_conn = BucketConnector(BUCKET)
        return self._amz_conn

    @property
    def rds_conn(self):
        if not hasattr(self, '_rds_conn'):
            self._rds_conn = ProjectConnector(self.project, DbUsers.CalculationEng)
        return self._rds_conn

    @property
    def kpi_static_data(self):
        if not hasattr(self, '_kpi_static_data'):
            self._kpi_static_data = self.get_kpi_data()
        return self._kpi_static_data

    def get_kpi_data(self):
        query = """
            select api.name as atomic_kpi_name, api.pk as atomic_kpi_fk, api.description,
                   kpi.display_text as kpi_name, kpi.pk as kpi_fk,
                   kps.name as kpi_set_name, kps.pk as kpi_set_fk
            from static.kpi_set kps
            left join static.kpi kpi on kps.pk = kpi.kpi_set_fk
            left join static.atomic_kpi api on kpi.pk = api.kpi_fk
        """
        kpi_data = pd.read_sql_query(query, self.rds_conn.db)
        return kpi_data

    def get_existing_data_per_attr(self, attribute):
        query = """SELECT {1}, p.store_fk, pr.pk as product_fk, pr.ean_code as product_ean_code
                        FROM {0} p LEFT JOIN static.stores s ON s.pk = p.store_fk
                            JOIN static_new.product pr ON pr.pk = p.product_fk
                            WHERE end_date IS NULL AND s.{1}= '{2}';""".format(
            STORE_ASSORTMENT_TABLE,ATTRIBUTES[self.project][ATTRIBUTE_IN_DB], attribute)

        self.rds_conn.disconnect_rds()
        self.rds_conn.connect_rds()
        osa_table = pd.read_sql_query(query, self.rds_conn.db)
        self.rds_conn.disconnect_rds()
        return osa_table

    def get_products(self):
        if self.pb:
            query = "select pk as {}, ean_code as {} from static_new.product where delete_date is null".format(PRODUCT_FK, PRODUCT_EAN)
        else:
            query = "select pk as {}, product_ean_code as {} from static.product where delete_date is null".format(
                PRODUCT_FK, PRODUCT_EAN)
        data = pd.read_sql_query(query, self.rds_conn.db)
        data.dropna(axis=0, inplace=True)
        try:
            data[PRODUCT_EAN] = data[PRODUCT_EAN].astype(int)
        except ValueError:
            data[PRODUCT_EAN] = data[PRODUCT_EAN]
        return data

    def get_stores_data(self):
        query = """select pk as store_fk, {0}
        from static.stores
        where delete_date is null and {0} is not null""".format(ATTRIBUTES[self.project]['ATTRIBUTE_IN_DB'])
        return pd.read_sql_query(query, self.rds_conn.db)

    @staticmethod
    def get_tuples_from_df(df):
        """Gets a dataFrame and return a list of tuples, each tuple  represent a row"""
        # df = df[[STORE_FK, 'product_fk']]
        tuples = [tuple(x) for x in df.values]
        return tuples

    def get_store_fk_for_attr(self,store_to_attr_map):
        """

        :param attr: the attribute related to store
        :param store_to_attr_map: a DF containing store_fk and the attribute. assuming different store_fk for each attribute
        :return: store_fk
        """
        return store_to_attr_map[STORE_FK].iloc[0]

    def get_data_from_csv(self, csv_path, sep=None):
        Log.info('Loading data from csv')
        seperator = sep if sep else ATTRIBUTES[self.project][SEPERATOR]
        data = pd.read_csv(csv_path, sep=seperator)
        data = data.drop_duplicates(subset=data.columns, keep='first')
        data = data.fillna('')
        missing = self.get_missing_products(data[PRODUCT_EAN].unique())
        self.missing_products = missing
        data = data.loc[~data[PRODUCT_EAN].isin(missing)]
        data_merged = data.merge(self.products[[PRODUCT_FK, PRODUCT_EAN]], how='left', left_on=PRODUCT_EAN,
                                 right_on=PRODUCT_EAN)
        data_merged = data_merged.merge(self.store_data[[STORE_FK, ATTRIBUTES[self.project][ATTRIBUTE_IN_DB]]], how='left',
                                        left_on=ATTRIBUTES[self.project][ATTRIBUTE_IN_TEMPLATE],
                                        right_on=ATTRIBUTES[self.project][ATTRIBUTE_IN_DB])
        Log.info('CSV data is loaded')
        return data_merged

    @staticmethod
    def _convert_to_int(num):
        if isinstance(num, float):
            return int(num)
        elif isinstance(num, str):
            if num.isdigit():
                return int(num)
        return num


    def add_activation_query(self,attr, product_fk, date):
        attributes = pd.DataFrame([(attr, product_fk, str(date), 1)],
                                  columns=['store_fk', 'product_fk', 'start_date', 'is_current'])
        query = insert(attributes.to_dict(), STORE_ASSORTMENT_TABLE)
        return query

    def add_deactivation_query(self, store_fk, product_fk, date):
        query = """update {} set end_date = '{}', is_current = '0'
                       where store_fk = {} and product_fk = {} and end_date is null""".format(STORE_ASSORTMENT_TABLE,
                                                                                              date,
                                                                                              store_fk, product_fk)
        return query

    def get_queries_for_assortmets(self, assortments, store_to_attr_map=None, insertion=True, immediate_change=False):
        """
        This gets an assortments and creates queries for it.
        :param assortments: list of tuples
        :param insertion: is it new in db or need to be updated
        :param immediate_change:
        :return:
        """
        current_date = datetime.now().date()
        if immediate_change:
            deactivate_date = current_date - timedelta(1)
            activate_date = current_date
        else:
            deactivate_date = current_date
            activate_date = current_date + timedelta(1)

        if insertion:
            for assortment in assortments:
                attribute, product_fk = assortment
                if ATTRIBUTES[self.project][BY_STORE]:
                    # If by store - gets the relevant store_fk to enter to db
                    attribute = self.get_store_fk_for_attr(store_to_attr_map)
                attribute = self._convert_to_int(attribute)
                product_fk = self._convert_to_int(product_fk)

                self.update_queries.append(self.add_activation_query(attribute, product_fk, activate_date))
        else:
            for assortment in assortments:
                attribute, product_fk = assortment
                if ATTRIBUTES[self.project][BY_STORE]:
                    # If by store - gets the relevant store_fk to enter to db
                    attribute = self.get_store_fk_for_attr(store_to_attr_map)
                attribute = self._convert_to_int(attribute)
                product_fk = self._convert_to_int(product_fk)
                self.update_queries.append(self.add_deactivation_query(attribute, product_fk, deactivate_date))

    def get_missing_products(self, products):
        new_products = set(products)
        existing_products = set(self.products[PRODUCT_EAN])
        missing = list(new_products.difference(existing_products))
        print "Found missing products: {}".format(missing)
        return missing

    def upload_assortment(self, csv_path):
        new_osa = self.get_data_from_csv(csv_path)
        attributes_in_file = new_osa[ATTRIBUTES[self.project][ATTRIBUTE_IN_TEMPLATE]].unique().tolist()
        try:
            valid_stores = self.store_data[ATTRIBUTES[self.project][ATTRIBUTE_IN_DB]].dropna().astype(int).tolist()
        except ValueError:
            valid_stores = self.store_data[ATTRIBUTES[self.project][ATTRIBUTE_IN_DB]].dropna().tolist()

        for value in attributes_in_file:
            Log.info("Working on store: {}".format(value))
            template_store_assortment = new_osa.loc[new_osa[ATTRIBUTES[self.project][ATTRIBUTE_IN_TEMPLATE]] == value]
            if value in valid_stores:

                new_osa_set = set(self.get_tuples_from_df(template_store_assortment[
                                                              [ATTRIBUTES[self.project][ATTRIBUTE_IN_TEMPLATE], PRODUCT_FK]]))
                current_osa = self.get_existing_data_per_attr(value)
                osa_set = set(self.get_tuples_from_df(current_osa[[ATTRIBUTES[self.project][ATTRIBUTE_IN_DB],PRODUCT_FK]]))

                # osa_set - new_osa_set: to be updated with end_ate
                old_assortments = osa_set.difference(new_osa_set)
                # new_osa_set - osa_set: to be inserted
                new_assortments = new_osa_set.difference(osa_set)
                Log.info("Got {} products to activate, and {} products to deactivate fot store {}". format(
                    len(new_assortments), len(old_assortments), value))

                #     value = int(template_store_assortment[STORE_FK].iloc[0])
                if old_assortments:
                    if ATTRIBUTES[self.project][BY_STORE]:
                        self.get_queries_for_assortmets(old_assortments,template_store_assortment,
                                                        insertion=False, immediate_change=True)
                    else:
                        self.get_queries_for_assortmets(old_assortments, insertion=False,
                                                        immediate_change=True)

                if new_assortments:
                    if ATTRIBUTES[self.project][BY_STORE]:
                        self.get_queries_for_assortmets(new_assortments, template_store_assortment, insertion=True, immediate_change=True)
                    else:
                        self.get_queries_for_assortmets(new_assortments, insertion=True, immediate_change=True)
            else:
                self.invalid_stores.append(value)
                Log.info("Store {} is invalid".format(value))
        print 'Missing products: {}'.format(self.missing_products)
        print 'Invalid stores: {}'.format(self.invalid_stores)

        if self.update_queries:
            self.commit_results()

    def commit_results(self):
        Log.info("Updating database... ")
        self.rds_conn.connect_rds()
        cur = self.rds_conn.db.cursor()
        Log.info('{} quaries are to be commited'.format(len(self.update_queries)))
        for query in self.update_queries:
            cur.execute(query)
        self.rds_conn.db.commit()
        self.rds_conn.disconnect_rds()

if __name__ == '__main__':
    LoggerInitializer.init('Assortment')
    Config.init('Assortment Upload')
    project = sys.argv[1]
    path = sys.argv[1]
    l = NewTemplate(project, pb=True)
    l.upload_assortment(path)