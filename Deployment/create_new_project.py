
import os
import shutil
from copy_project_to_integ import MoveCodeToINTEG

__author__ = 'Nimrod'

MAIN_FILE_NAME = 'Calculations'
GENERATOR_FILE_NAME = 'KPIGenerator'
FETCHER_FILE_NAME = 'Fetcher'
TOOL_BOX_FILE_NAME = 'KPIToolBox'
GENERAL_TOOL_BOX_FILE_NAME = 'GeneralToolBox'
KPI_UTILS_DIRECTORY = 'KPIUtils'
KPI_UTILS_PATH = '{}/{}/'.format(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), KPI_UTILS_DIRECTORY)

MAIN_FUNCTION = """
from Trax.Algo.Calculations.Core.CalculationsScript import BaseCalculationsScript
# from Trax.Algo.Calculations.Core.DataProvider import KEngineDataProvider, Output
# from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Logging.Logger import Log
from Trax.Cloud.Services.Connector.Logger import LoggerInitializer

from Projects.%(project_capital)s.%(generator_file_name)s import %(generator_class_name)s

__author__ = '%(author)s'


class %(main_class_name)s(BaseCalculationsScript):
    def run_project_calculations(self):
        self.timer.start()
        %(generator_class_name)s(self.data_provider, self.output).main_function()
        self.timer.stop('%(generator_file_name)s.run_project_calculations')


# if __name__ == '__main__':
#     LoggerInitializer.init('%(project)s calculations')
#     Config.init()
#     project_name = '%(project)s'
#     data_provider = KEngineDataProvider(project_name)
#     session = ''
#     data_provider.load_session_data(session)
#     output = Output()
#     %(main_class_name)s(data_provider, output).run_project_calculations()
"""

TOOL_BOX = """
import pandas as pd
from datetime import datetime

from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Logging.Logger import Log
from Trax.Data.Utils.MySQLservices import get_table_insertion_query as insert

from Projects.%(project_capital)s.Utils.%(fetcher_file_name)s import %(fetcher_class_name)s
from Projects.%(project_capital)s.Utils.%(general_tool_box_file_name)s import %(general_tool_box_class_name)s

__author__ = '%(author)s'

KPI_RESULT = 'report.kpi_results'
KPK_RESULT = 'report.kpk_results'
KPS_RESULT = 'report.kps_results'


def log_runtime(description, log_start=False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            calc_start_time = datetime.utcnow()
            if log_start:
                Log.info('{} started at {}'.format(description, calc_start_time))
            result = func(*args, **kwargs)
            calc_end_time = datetime.utcnow()
            Log.info('{} took {}'.format(description, calc_end_time - calc_start_time))
            return result
        return wrapper
    return decorator


class %(tool_box_class_name)s:
    LEVEL1 = 1
    LEVEL2 = 2
    LEVEL3 = 3

    def __init__(self, data_provider, output):
        self.output = output
        self.data_provider = data_provider
        self.project_name = self.data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.products = self.data_provider[Data.PRODUCTS]
        self.all_products = self.data_provider[Data.ALL_PRODUCTS]
        self.match_product_in_scene = self.data_provider[Data.MATCHES]
        self.visit_date = self.data_provider[Data.VISIT_DATE]
        self.session_info = self.data_provider[Data.SESSION_INFO]
        self.scene_info = self.data_provider[Data.SCENES_INFO]
        self.store_id = self.data_provider[Data.STORE_FK]
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.tools = %(general_tool_box_class_name)s(self.data_provider, self.output, rds_conn=self.rds_conn)
        self.kpi_static_data = self.get_kpi_static_data()
        self.kpi_results_queries = []

    def get_kpi_static_data(self):
        \"""
        This function extracts the static KPI data and saves it into one global data frame.
        The data is taken from static.kpi / static.atomic_kpi / static.kpi_set.
        \"""
        query = %(fetcher_class_name)s.get_all_kpi_data()
        kpi_static_data = pd.read_sql_query(query, self.rds_conn.db)
        return kpi_static_data

    def main_calculation(self, *args, **kwargs):
        \"""
        This function calculates the KPI results.
        \"""
        score = 0
        return score

    def write_to_db_result(self, fk, score, level):
        \"""
        This function creates the result data frame of every KPI (atomic KPI/KPI/KPI set),
        and appends the insert SQL query into the queries' list, later to be written to the DB.
        \"""
        attributes = self.create_attributes_dict(fk, score, level)
        if level == self.LEVEL1:
            table = KPS_RESULT
        elif level == self.LEVEL2:
            table = KPK_RESULT
        elif level == self.LEVEL3:
            table = KPI_RESULT
        else:
            return
        query = insert(attributes, table)
        self.kpi_results_queries.append(query)

    def create_attributes_dict(self, fk, score, level):
        \"""
        This function creates a data frame with all attributes needed for saving in KPI results tables.

        \"""
        if level == self.LEVEL1:
            kpi_set_name = self.kpi_static_data[self.kpi_static_data['kpi_set_fk'] == fk]['kpi_set_name'].values[0]
            attributes = pd.DataFrame([(kpi_set_name, self.session_uid, self.store_id, self.visit_date.isoformat(),
                                        format(score, '.2f'), fk)],
                                      columns=['kps_name', 'session_uid', 'store_fk', 'visit_date', 'score_1',
                                               'kpi_set_fk'])
        elif level == self.LEVEL2:
            kpi_name = self.kpi_static_data[self.kpi_static_data['kpi_fk'] == fk]['kpi_name'].values[0]
            attributes = pd.DataFrame([(self.session_uid, self.store_id, self.visit_date.isoformat(),
                                        fk, kpi_name, score)],
                                      columns=['session_uid', 'store_fk', 'visit_date', 'kpi_fk', 'kpk_name', 'score'])
        elif level == self.LEVEL3:
            data = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]
            atomic_kpi_name = data['atomic_kpi_name'].values[0]
            kpi_fk = data['kpi_fk'].values[0]
            kpi_set_name = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]['kpi_set_name'].values[0]
            attributes = pd.DataFrame([(atomic_kpi_name, self.session_uid, kpi_set_name, self.store_id,
                                        self.visit_date.isoformat(), datetime.utcnow().isoformat(),
                                        score, kpi_fk, fk)],
                                      columns=['display_text', 'session_uid', 'kps_name', 'store_fk', 'visit_date',
                                               'calculation_time', 'score', 'kpi_fk', 'atomic_kpi_fk'])
        else:
            attributes = pd.DataFrame()
        return attributes.to_dict()

    @log_runtime('Saving to DB')
    def commit_results_data(self):
        \"""
        This function writes all KPI results to the DB, and commits the changes.
        \"""
        insert_queries = self.merge_insert_queries(self.kpi_results_queries)
        cur = self.rds_conn.db.cursor()
        delete_queries = %(fetcher_class_name)s.get_delete_session_results_query(self.session_uid)
        for query in delete_queries:
            cur.execute(query)
        for query in insert_queries:
            cur.execute(query)
        self.rds_conn.db.commit()

    @staticmethod
    def merge_insert_queries(insert_queries):
        query_groups = {}
        for query in insert_queries:
            static_data, inserted_data = query.split('VALUES ')
            if static_data not in query_groups:
                query_groups[static_data] = []
            query_groups[static_data].append(inserted_data)
        merged_queries = []
        for group in query_groups:
            merged_queries.append('{0} VALUES {1}'.format(group, ',\\n'.join(query_groups[group])))
        return merged_queries
"""

GENERATOR = """
from Trax.Utils.Logging.Logger import Log

from Projects.%(project_capital)s.Utils.%(tool_box_file_name)s import %(tool_box_class_name)s, log_runtime

__author__ = '%(author)s'


class %(generator_class_name)s:

    def __init__(self, data_provider, output):
        self.data_provider = data_provider
        self.output = output
        self.project_name = data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.tool_box = %(tool_box_class_name)s(self.data_provider, self.output)

    @log_runtime('Total Calculations', log_start=True)
    def main_function(self):
        \"""
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        \"""
        if self.tool_box.scif.empty:
            Log.warning('Scene item facts is empty for this session')
        for kpi_set_fk in self.tool_box.kpi_static_data['kpi_set_fk'].unique().tolist():
            score = self.tool_box.main_calculation(kpi_set_fk=kpi_set_fk)
            self.tool_box.write_to_db_result(kpi_set_fk, score, self.tool_box.LEVEL1)
        self.tool_box.commit_results_data()
"""

FETCHER = """
__author__ = '%(author)s'


class %(fetcher_class_name)s(object):

    @staticmethod
    def get_all_kpi_data():
        return \"""
            select api.name as atomic_kpi_name, api.pk as atomic_kpi_fk,
                   kpi.display_text as kpi_name, kpi.pk as kpi_fk,
                   kps.name as kpi_set_name, kps.pk as kpi_set_fk
            from static.atomic_kpi api
            left join static.kpi kpi on kpi.pk = api.kpi_fk
            join static.kpi_set kps on kps.pk = kpi.kpi_set_fk
        \"""

    @staticmethod
    def get_delete_session_results_query(session_uid):
        return ("delete from report.kps_results where session_uid = '{}';".format(session_uid),
                "delete from report.kpk_results where session_uid = '{}';".format(session_uid),
                "delete from report.kpi_results where session_uid = '{}';".format(session_uid))
"""


class CreateKPIProject:

    def __init__(self, project_name):
        self.project = project_name.lower().replace('_', '-')
        self.project_capital = self.project.upper().replace('-', '_')
        self.project_short = self.project_capital.split('_')[0]
        self.author = os.environ.get('USER', '')
        self.project_path = '{}/Projects/{}/'.format(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                                                     self.project_capital)
        self.create_project_directory()

    def create_project_directory(self):
        if os.path.exists(self.project_path):
            shutil.rmtree(self.project_path)
        os.mkdir(self.project_path)
        with open(self.project_path + '__init__.py', 'wb') as f:
            f.write('')

    def create_new_project(self):
        files_to_create = {'': [(MAIN_FILE_NAME, MAIN_FUNCTION),
                                (GENERATOR_FILE_NAME, GENERATOR)],
                           'Utils': [(TOOL_BOX_FILE_NAME, TOOL_BOX),
                                     (FETCHER_FILE_NAME, FETCHER)]}

        formatting_dict = {'author': self.author,
                           'project': self.project,
                           'project_capital': self.project_capital,
                           'generator_file_name': GENERATOR_FILE_NAME,
                           'generator_class_name': '{}Generator'.format(self.project_short),
                           'tool_box_file_name': TOOL_BOX_FILE_NAME,
                           'tool_box_class_name': '{}ToolBox'.format(self.project_short),
                           'fetcher_file_name': FETCHER_FILE_NAME,
                           'fetcher_class_name': '{}Queries'.format(self.project_short),
                           'main_file_name': MAIN_FILE_NAME,
                           'main_class_name': '{}Calculations'.format(self.project_short),
                           'general_tool_box_file_name': GENERAL_TOOL_BOX_FILE_NAME,
                           'general_tool_box_class_name': '{}GENERALToolBox'.format(self.project_short)}
        for directory in files_to_create.keys():
            if directory:
                directory_path = self.project_path + directory + '/'
                os.mkdir(directory_path)
                with open(directory_path + '__init__.py', 'wb') as f:
                    f.write('')
            else:
                directory_path = self.project_path
            for file_name, file_content in files_to_create[directory]:
                with open(directory_path + file_name + '.py', 'wb') as f:
                    f.write(file_content % formatting_dict)

        references_change = MoveCodeToINTEG('DUMMY', self.project_short)
        utils_directory = os.path.join(self.project_path, 'Utils')
        if not os.path.exists(utils_directory):
            utils_directory = self.project_path
        for utils_file in os.listdir(KPI_UTILS_PATH):
            if utils_file != '__init__.py' and not os.path.isdir(KPI_UTILS_PATH + utils_file):
                shutil.copyfile(os.path.join(KPI_UTILS_PATH, utils_file), os.path.join(utils_directory, utils_file))
                new_file = os.path.join(utils_directory, utils_file)
                references_change.change_new_project_files(new_file, False)
                references_change.change_new_project_files(new_file, True)
                with open(new_file, 'rb') as f:
                    content = f.read()
                if 'Utils' in utils_directory:
                    content = content.replace('{}.'.format(KPI_UTILS_DIRECTORY),
                                              'Projects.{}.Utils.'.format(self.project_capital))
                else:
                    content = content.replace('{}.'.format(KPI_UTILS_DIRECTORY),
                                              'Projects.{}.'.format(self.project_capital))
                with open(new_file, 'wb') as f:
                    f.write(content)

if __name__ == '__main__':
    project_name = 'cubau_sand'
    new = CreateKPIProject(project_name)
    new.create_new_project()
