
import re
import os
import pandas as pd

from Trax.Data.Projects.ProjectConnector import AwsProjectConnector
from Trax.Algo.Calculations.Core.DataProvider import KEngineDataProvider, Output
from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Logging.Logger import Log
from Trax.Cloud.Services.Connector.Logger import LoggerInitializer
from Trax.Cloud.Services.Connector.Keys import DbUsers

__author__ = 'Nimrod'

PROJECTS_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'Projects')


class ProjectTestings:

    def __init__(self, project):
        self.project = project
        self.project_path = os.path.join(PROJECTS_PATH, self.project.upper().replace('-', '_'))
        self.rds_conn = AwsProjectConnector(self.project.lower().replace('_', '-'), DbUsers.CalculationEng)
        self.session_uid = self.get_example_session()

    def get_example_session(self):
        query = """
                select se.session_uid
                from probedata.session se
                join report.kps_results kps on kps.session_uid = se.session_uid
                order by date_format(se.visit_date, '%Y-%m') desc, se.number_of_scenes desc
                limit 1
                """
        data = pd.read_sql_query(query, self.rds_conn.db)
        if data.empty:
            Log.warning("Could not find a session for project '{}'".format(self.project))
            return
        return data['session_uid'].iloc[0]

    def get_main_file_data(self):
        for file_name in os.listdir(self.project_path):
            file_path = os.path.join(self.project_path, file_name)
            if not os.path.isdir(file_path) and file_name.endswith('.py'):
                with open(file_path, 'rb') as f:
                    content = f.read()
                    if 'KEngineDataProvider' in content or 'ACEDataProvider' in content:
                        class_name, func_name = self.extract_class_and_func_names(content)
                        return file_name, class_name, func_name
        Log.warning("Could not find the main function for project '{}'".format(self.project))
        return

    def test_project(self):
        if self.session_uid is not None:
            data_provider = KEngineDataProvider(self.project.lower().replace('_', '-'))
            data_provider.load_session_data(self.session_uid)
            output = Output()
            file_data = self.get_main_file_data()
            if file_data is not None:
                file_name, class_name, func_name = file_data
                print 'from Projects.{0}.{1} import {2}'.format(self.project.upper().replace('-', '_'), file_name.replace('.py', ''), class_name)
                exec('from Projects.{0}.{1} import {2}'.format(self.project.upper().replace('-', '_'), file_name.replace('.py', ''), class_name))
                print '{0}(data_provider, output).{1}()'.format(class_name, func_name)
                exec('{0}(data_provider, output).{1}()'.format(class_name, func_name))
                return True

    @staticmethod
    def extract_class_and_func_names(file_content):
        class_name = re.findall('class\s+(\w+)', file_content)[0]
        func_name = re.findall('def\s+(\w+)', file_content)[0]
        return class_name, func_name


if __name__ == '__main__':
    LoggerInitializer.init('Project Testings')
    Config.init()
    successful_projects = []
    failed_projects = []
    special_projects = []
    project_list = ['inbevbe-sand', 'inbevlu-sand', 'inbevnl-sand']
    for project in project_list:
        try:
            if ProjectTestings(project).test_project():
                successful_projects.append(project)
                Log.info('{} ran successfully'.format(project))
            else:
                special_projects.append(project)
        except:
            Log.error('{} failed'.format(project))
            failed_projects.append(project)
    Log.info('Successful projects: {}'.format(', '.join(successful_projects)))
    Log.info('Failed projects: {}'.format(', '.join(failed_projects)))
    Log.info('Projects with other comments: {}'.format(', '.join(special_projects)))
