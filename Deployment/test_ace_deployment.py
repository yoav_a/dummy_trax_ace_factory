from Deployment import fabfile
from Trax.Utils.Conf.Configuration import Config

if __name__ == '__main__':
    Config.init()
    fabfile.deploy(project='batru')
    pass
