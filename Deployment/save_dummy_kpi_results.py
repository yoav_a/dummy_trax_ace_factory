
import numpy as np
import pandas as pd
from random import randint, choice
from datetime import datetime

from Trax.Data.Utils.MySQLservices import get_table_insertion_query as insert
from Trax.Data.Projects.ProjectConnector import AwsProjectConnector
from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Logging.Logger import Log
from Trax.Cloud.Services.Connector.Keys import DbUsers

__author__ = 'Nimrod'

KPI_RESULT = 'report.kpi_results'
KPK_RESULT = 'report.kpk_results'
KPS_RESULT = 'report.kps_results'

BINARY = 'Binary'
PROPORTIONAL = 'Proportional'


class Configurations(object):

    MAX_SCORE = 100
    SCORE_TYPE = BINARY
    THRESHOLD = False
    RESULT = True


class CreateDummyResults(Configurations):
    LEVEL1 = 'Level1'
    LEVEL2 = 'Level2'
    LEVEL3 = 'Level3'

    def __init__(self, project, session_uid, set_names=None):
        self.project = project
        self.session_uid = session_uid
        self.set_names = set_names
        self.aws_conn = AwsProjectConnector(self.project, DbUsers.CalculationEng)
        self.kpi_static_data = self.get_kpi_static_data()
        self.session_data = self.get_session_data()
        self.store_id = self.session_data['store_id']
        self.visit_date = self.session_data['visit_date']
        self.kpi_results_queries = []

    def get_kpi_static_data(self):
        """
        This function extracts the static KPI data and saves it into one global data frame.
        The data is taken from static.kpi / static.atomic_kpi / static.kpi_set.
        """
        query = """
                select api.name as atomic_kpi_name, api.pk as atomic_kpi_fk,
                       kpi.display_text as kpi_name, kpi.pk as kpi_fk,
                       kps.name as kpi_set_name, kps.pk as kpi_set_fk
                from static.atomic_kpi api
                left join static.kpi kpi on kpi.pk = api.kpi_fk
                join static.kpi_set kps on kps.pk = kpi.kpi_set_fk
                {}
                """
        if self.set_names:
            if len(self.set_names) == 1:
                query = query.format("where kps.name = '{}'".format(self.set_names[0]))
            else:
                query = query.format("where kps.name in {}".format(tuple(self.set_names)))
        else:
            query = query.format('')
        kpi_static_data = pd.read_sql_query(query, self.aws_conn.db)
        return kpi_static_data

    def get_session_data(self):
        query = """
                select se.visit_date, st.pk as store_id
                from probedata.session se
                join static.stores st on st.pk = se.store_fk
                where se.session_uid = '{}'""".format(self.session_uid)
        session_data = pd.read_sql_query(query, self.aws_conn.db)
        return session_data.iloc[0]

    def write_dummy_results(self):
        sets = self.kpi_static_data['kpi_set_fk'].unique().tolist()
        if sets:
            for set_fk in sets:
                set_scores = []
                kpis_for_set = self.kpi_static_data[self.kpi_static_data['kpi_set_fk'] == set_fk]
                for kpi_fk in kpis_for_set['kpi_fk'].unique():
                    kpi_scores = []
                    atomics_for_kpi = kpis_for_set[self.kpi_static_data['kpi_fk'] == kpi_fk]
                    for atomic_fk in atomics_for_kpi['atomic_kpi_fk'].unique():
                        if self.SCORE_TYPE == BINARY:
                            atomic_score = randint(0, 1) * self.MAX_SCORE
                            if atomic_score == 0:
                                threshold = randint(1, 5)
                                result = 0
                            else:
                                threshold = randint(1, 5)
                                result = randint(threshold, threshold + 2)
                        else:
                            atomic_score = choice([0, randint(1, self.MAX_SCORE), randint(1, self.MAX_SCORE)])
                            if atomic_score == 0:
                                threshold = randint(1, 5)
                                result = 0
                            else:
                                threshold = randint(1, 5)
                                result = max(1, int((atomic_score / float(self.MAX_SCORE)) * threshold))
                        self.write_to_db_result(atomic_fk, (atomic_score, result, threshold), self.LEVEL3)
                        kpi_scores.append(atomic_score)
                    if kpi_scores:
                        if self.SCORE_TYPE == BINARY:
                            if 0 not in kpi_scores:
                                kpi_score = self.MAX_SCORE
                            else:
                                kpi_score = 0
                        else:
                            kpi_score = np.mean(kpi_scores)
                        set_scores.append(kpi_score)
                        self.write_to_db_result(kpi_fk, kpi_score, self.LEVEL2)
                if set_scores:
                    set_score = np.mean(set_scores)
                    self.write_to_db_result(set_fk, set_score, self.LEVEL1)
            self.commit_results_data()

    def write_to_db_result(self, fk, score, level):
        """
        This function the result data frame of every KPI (atomic KPI/KPI/KPI set),
        and appends the insert SQL query into the queries' list, later to be written to the DB.
        """
        attributes = self.create_attributes_dict(fk, score, level)
        if level == self.LEVEL1:
            table = KPS_RESULT
        elif level == self.LEVEL2:
            table = KPK_RESULT
        elif level == self.LEVEL3:
            table = KPI_RESULT
        else:
            return
        query = insert(attributes, table)
        self.kpi_results_queries.append(query)

    def create_attributes_dict(self, fk, score, level):
        """
        This function creates a data frame with all attributes needed for saving in KPI results tables.
        """
        if level == self.LEVEL1:
            kpi_set_name = self.kpi_static_data[self.kpi_static_data['kpi_set_fk'] == fk]['kpi_set_name'].values[0]
            attributes = pd.DataFrame([(kpi_set_name, self.session_uid, self.store_id, self.visit_date.isoformat(),
                                        format(score, '.2f'), fk)],
                                      columns=['kps_name', 'session_uid', 'store_fk', 'visit_date', 'score_1',
                                               'kpi_set_fk'])

        elif level == self.LEVEL2:
            kpi_name = self.kpi_static_data[self.kpi_static_data['kpi_fk'] == fk]['kpi_name'].values[0].replace("'", "\\'")
            attributes = pd.DataFrame([(self.session_uid, self.store_id, self.visit_date.isoformat(),
                                        fk, kpi_name, score)],
                                      columns=['session_uid', 'store_fk', 'visit_date', 'kpi_fk', 'kpk_name',
                                               'score'])
        elif level == self.LEVEL3:
            score, result, threshold = score
            if not self.RESULT:
                result = None
            if not self.THRESHOLD:
                threshold = None
            data = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]
            atomic_kpi_name = data['atomic_kpi_name'].values[0].replace("'", "\\'")
            kpi_fk = data['kpi_fk'].values[0]
            kpi_set_name = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]['kpi_set_name'].values[0]
            attributes = pd.DataFrame([(atomic_kpi_name, self.session_uid, kpi_set_name, self.store_id,
                                        self.visit_date.isoformat(), datetime.utcnow().isoformat(),
                                        score, kpi_fk, fk, threshold, result)],
                                      columns=['display_text', 'session_uid', 'kps_name', 'store_fk', 'visit_date',
                                               'calculation_time', 'score', 'kpi_fk', 'atomic_kpi_fk', 'threshold',
                                               'result'])
        else:
            attributes = pd.DataFrame()
        return attributes.to_dict()

    def commit_results_data(self):
        """
        This function writes all KPI results to the DB, and commits the changes.
        """
        cur = self.aws_conn.db.cursor()
        delete_queries = ("delete from {} where session_uid = '{}';".format(KPS_RESULT, self.session_uid),
                          "delete from {} where session_uid = '{}';".format(KPK_RESULT, self.session_uid),
                          "delete from {} where session_uid = '{}';".format(KPI_RESULT, self.session_uid))
        for query in delete_queries:
            cur.execute(query)
        for query in self.kpi_results_queries:
            cur.execute(query)
        self.aws_conn.db.commit()


# if __name__ == '__main__':
#     Config.init()
#     project = 'diageogr-sand'
#     session = '3c5231e8-2508-11e7-b08e-12c1d33b5134'
#     CreateDummyResults(project, session).write_dummy_results()
