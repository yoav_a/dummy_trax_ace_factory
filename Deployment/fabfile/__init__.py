import getpass
import os
import shutil
import sys
import tempfile
import traceback
import time
from datetime import datetime
from Trax.Cloud.Services.Storage.Factory import StorageFactory
from Trax.Deployment.Tools.Mailer import DeploymentMailer
from Trax.Utils.Conf.Configuration import Config
from git import Actor
from Deployment.Git import create_new_tag, get_last_tag_on_current_commit, get_live_repository, get_factory_git_folder, \
    get_live_git_folder, get_factory_repository, SSH_CMD, re_arrange_folder
from Deployment.ProjectValidation import modules_checkup
from Deployment.Utils import make_tar_file_for_files, recursive_overwrite, save_file_stream
from Trax.Cloud.Services.Connector.Keys import EmailUsers
from Trax.Cloud.Services.Mailers.Factory import MailerFactory
from fabric.state import env

ROLLBACK_SUFFIX = '_rollback'
TRAX_ACE_LIVE = 'trax_ace_live'
S3_BUCKET = ['trax-k-engine-scripts']
S3_PATH = 'trax_ace_live_latest'
TAR_FILE_NAME = 'latest.tar.gz'


def ignore_files(files):
    return [file_name for file_name in files if '.pyc' in file_name]


def build_paths_and_perform_overwrite(live_git_folder, folder_to_copy):
    src_path = '{}/'.format(get_factory_git_folder()) + folder_to_copy
    dst_path = '{}/'.format(live_git_folder) + folder_to_copy

    print "overwrite from {} to {}".format(src_path, dst_path)
    recursive_overwrite(src_path, dst_path, ignore=ignore_files)


def make_tarfile(source_dir):
    files_and_dirs_to_copy = ['Projects', 'KPIUtils', 'KPIUtils_v2', 'version.txt']
    return make_tar_file_for_files(source_dir, files_and_dirs_to_copy)


def upload_to_live_git(project, project_tag):

    print "upload_to_live_git START"
    live_git_folder, live_repo = get_live_repository()

    project_path = 'Projects/{}'.format(project)
    build_paths_and_perform_overwrite(live_git_folder, project_path)
    author = Actor(getpass.getuser(), '')
    now = datetime.utcnow()
    now.replace(microsecond=0)
    version_details = '''tag: {}\n'project: {}\n'author: {}\n'time: {}'''.format(project_tag, project, author, now)
    update_version_file(os.path.join(live_git_folder, project_path), project_tag)

    utils_path = 'KPIUtils'
    build_paths_and_perform_overwrite(live_git_folder, utils_path)

    utils_path2 = 'KPIUtils_v2'
    build_paths_and_perform_overwrite(live_git_folder, utils_path2)
    try:
        with live_repo.git.custom_environment(GIT_SSH_COMMAND=SSH_CMD):
            live_repo.git.add(A=True)
            print "performing commit '{}'".format(version_details)
            live_repo.index.commit(version_details)
            print "Creating new tag"
            live_tag = create_new_tag('trax_ace_live', live_repo)
            live_repo.create_tag(path=live_tag, message=version_details)
            print "pushing tag: {}".format(live_tag)
            # live_repo.remotes.origin.push('master', tags=True)
            # print "upload_to_live_git FINISHED"

    except Exception as e:
        print 'upload_to_live_git Exception {}'.format(e)
        traceback.print_exc()
        live_repo.git.reset('--hard', 'origin/master')
        raise e

    finally:
        print 'deleting {}'.format(get_live_git_folder())
        shutil.rmtree(get_live_git_folder())


def update_version_file(path, tag):
    version_file_path = os.path.join(path, 'version.txt')
    with open(version_file_path, 'w+') as f:
        f.write('tag:{}'.format(tag))


def push_factory_new_tag(project, repo):
    print "push_new_tag START"
    # Filter relevant tags
    current_tag = get_last_tag_on_current_commit(repo, project)
    if current_tag:
        return current_tag

    new_tag = create_new_tag(project, repo)

    try:
        with repo.git.custom_environment(GIT_SSH_COMMAND=SSH_CMD):
            author = Actor(getpass.getuser(), '')
            repo.create_tag(new_tag, message='Author: {0}, data: {1}'.format(author, datetime.now()))
            print "pushing new tag: {}".format(new_tag)
            repo.remotes.origin.push('origin/master', tags=True)

    except Exception as e:
        print 'push_new_tag Exception {}'.format(e)
        traceback.print_exc()
        raise

    print "push_new_tag FINISHED"
    return new_tag


def deploy(project):
    try:
        # Create the mail object

        print 'deploy START project={}'.format(project)

        factory_git_folder, repo = get_factory_repository()
        root_path = os.path.join(factory_git_folder)
        converted_project_name = project.replace('-', '_').upper()
        root_path += '/Projects/{}'.format(converted_project_name)

        print "root path={}".format(root_path)
        re_arrange_folder()
        modules_checkup(root_path)

        tag = push_factory_new_tag(converted_project_name, repo)
        upload_to_live_git(converted_project_name, project_tag=tag)
        # copy_to_storage_server()

        print 'deleting {}'.format(factory_git_folder)
        shutil.rmtree(factory_git_folder)
        send_mail(project, tag)
        print 'deploy FINISHED SUCCESSFULLY {} tag={}'.format(converted_project_name, tag)
    except Exception as e:
        print e
        send_mail(project, tag, e)
        sys.exit(1)

    sys.exit(0)


def copy_to_storage_server():
    live_folder, live_repo = get_live_repository()
    Config.init()
    try:
        tag = get_last_tag_on_current_commit(live_repo, TRAX_ACE_LIVE)
        if not tag:
            raise Exception('LiveTagNotExistsOnCurrentCommit')
        update_version_file(live_folder, tag)
        tar_file_stream = make_tarfile(live_folder, )
        for bucket in S3_BUCKET:
            storage_connector = StorageFactory.get_connector(mybucket=bucket, region='us-east-1')
            save_file_stream(storage_connector, S3_PATH, TAR_FILE_NAME, tar_file_stream)
    except Exception as e:
        print e
        raise
    finally:
        shutil.rmtree(live_folder)


def send_mail(project, tag, error=None):
    mailing_list = ['ps_sw_team@Trax-Tech.com']

    deployment_summary = {'Project': project,
                          'Date': time.strftime("%Y-%m-%d"),
                          'User': getpass.getuser(),
                          'Language': 'Python',
                          'Tag': tag
                          }
    mailer = MailerFactory.get_mailer(EmailUsers.TraxMailer)

    mail_body = summary_table('', deployment_summary,fab_result=True)
    mailer_subject= 'Trax ACE Factory Deployment on {project}'.format(project=project)
    mailer.send_email(mailing_list, mail_body, mailer_subject)


def summary_table(email_body, deployment_summary, fab_result=None):
    if deployment_summary:
        bgcolor = '8FBC8F'
        if not fab_result:
            bgcolor = 'DC143C'

        summary = '<BR>Summary:<BR>'
        summary += '<TABLE border=3 width="50%" bgcolor=9FB8C6 style="color:Black;" >'

        for key in deployment_summary:
            summary += '<TR bgcolor="' + bgcolor + '">'
            summary += '<TD>' + str(key).replace('_', ' ') + '</TD>'
            summary += '<TD><B>' + str(deployment_summary[key]) + '</B></TD>'
            summary += '</TR>'

        summary += '</TABLE>'

        signature = '<BR>'
        signature += 'Sincerely yours, <BR>'
        signature += 'THE WALL BREAKERS TEAM <BR>'
        email_body += summary + signature

    return email_body
#
# if __name__ == '__main__':
#     copy_to_storage_server()
