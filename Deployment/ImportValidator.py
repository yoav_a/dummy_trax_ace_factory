import subprocess

import os
import pandas as pd
from StringIO import StringIO

COLUMN_PROJECT_NAME = 'Project_Name'
COLUMN_PYTHON_FILE = 'Python_File'
COLUMN_REFERENCED_FOLDER = 'Referred_Folder'
COLUMN_REFERRED_FILE = 'Referred_File'


class ValidateRelations():
    @staticmethod
    def validate_relations(project, relation_file):
        test = StringIO(relation_file)
        df = pd.read_csv(test, names=[COLUMN_PROJECT_NAME,
                                      COLUMN_PYTHON_FILE,
                                      COLUMN_REFERENCED_FOLDER,
                                      COLUMN_REFERRED_FILE])
        cleaned_df = ValidateRelations.cleanDataFrame(project, df)
        return cleaned_df

    @staticmethod
    def cleanDataFrame(project, df):
        return df[df.Referred_Folder != 'None'][df.Referred_Folder.str.contains('theGarage') == False][
            df.Referred_Folder.str.contains('miniconda') == False][df.Referred_File.str.contains('KPIUtils') == False][
            df.Referred_File.str.contains(project + "/") == False]


if __name__ == '__main__':
    sfood_home = os.path.join(os.environ['HOME'], 'miniconda/envs/garage/bin/sfood')
    trax_ace_projects = os.path.join(os.environ['HOME'], 'dev/trax_ace_factory/Projects')
    projects = os.listdir(trax_ace_projects)
    maindf = pd.DataFrame(columns=[COLUMN_PROJECT_NAME,
                                   COLUMN_PYTHON_FILE,
                                   COLUMN_REFERENCED_FOLDER,
                                   COLUMN_REFERRED_FILE])
    for project in projects:
        project_x = os.path.join(trax_ace_projects, project)
        p = subprocess.Popen([sfood_home, project_x], stdout=subprocess.PIPE)
        result = p.communicate()[0]
        res_df = ValidateRelations.validate_relations(project,
                                                      result.replace("(", "").
                                                      replace(")", "").
                                                      replace("'", "").
                                                      replace(" ", ""))
        maindf = maindf.append(res_df)

    export_df = maindf.drop([COLUMN_PROJECT_NAME, COLUMN_REFERENCED_FOLDER], axis=1)
    if len(maindf) > 0:
        with open(os.path.join('/tmp', 'bad_relations.csv'), "wb") as output_file:
            export_df.to_csv(path_or_buf=output_file,
                             encoding='utf-8',
                             index=False)
