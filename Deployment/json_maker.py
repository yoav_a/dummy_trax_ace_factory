
from random import randint
import pandas as pd

from Trax.Data.Projects.ProjectConnector import AwsProjectConnector
from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Logging.Logger import Log
from Trax.Cloud.Services.Connector.Logger import LoggerInitializer
from Trax.Cloud.Services.Connector.Keys import DbUsers


WITH_ATOMICS = u"""
{{
    "name": "{name}",
    "kpi_details": [
        {{
            "name": "{entity1}",
            "result": "{result1}",
            "score": "",
            "target": ""
        }},
        {{
            "name": "{entity2}",
            "result": "{result2}",
            "score": "",
            "target": ""
        }},
        {{
            "name": "{entity3}",
            "result": "{result3}",
            "score": "",
            "target": ""
        }}
    ],
    "kps_score": "{score}"
}},
"""

WITHOUT_ATOMICS = u"""
{{
    "name": "{name}",
    "kpi_details": [
        {{
            "name": "{name}",
            "result": "{score}",
            "score": "",
            "target": ""
        }},
    ],
    "kps_score": "{score}"
}},
"""

CATEGORY_FK = 5
NAME = 'Name'
ENTITY = 'Entity'
AGGREGATION = 'Aggregation'
TYPE = 'Type'


class JsonMaker:

    def __init__(self, project='pngau', path='/home/Nimrod/Desktop/PNGAU_JSON.xlsx'):
        self.project = project
        self.aws_conn = AwsProjectConnector(self.project, DbUsers.CalculationEng)
        self.data = self.get_category_data(CATEGORY_FK)
        self.category = self.data['category_name'].values[0]
        self.template = pd.read_excel(path).fillna('')

    def get_category_data(self, category_fk):
        query = """select p.product_ean_code, b.name as brand_name, m.name as manufacturer_name, c.name as category_name
                    from static.product p
                    join static.brand b on b.pk = p.brand_fk
                    join static.product_categories c on c.pk = b.category_fk
                    join static.manufacturers m on m.pk = b.manufacturer_fk
                    where c.pk = {}""".format(category_fk)
        return pd.read_sql_query(query, self.aws_conn.db)

    def create_json(self):
        json_data = []
        for t in xrange(len(self.template)):
            data = self.template.iloc[t]
            name = data[NAME].format(category=self.category)
            entity = data[ENTITY]
            score_type = data[TYPE]
            aggregation = data[AGGREGATION]
            if entity:
                results = []
                if entity in self.data.keys():
                    items = self.data[entity].unique().tolist()[:3]
                else:
                    items = ['{}{}'.format(entity, i) for i in xrange(1, 4)]
                remaining_score = 100
                for i, item in enumerate(items):
                    if score_type == 1:
                        result = randint(0, 1)
                    elif score_type == '%':
                        if i == 2:
                            result = remaining_score
                        else:
                            result = float('{}.{}'.format(randint(1, min(2, int(remaining_score) - 10)), randint(0, 99)))
                            remaining_score -= result
                    elif score_type == '#':
                        result = float('{}.{}'.format(randint(15, 60), randint(0, 99)))
                    elif score_type == '#!':
                        result = randint(1, 15)
                    else:
                        Log.warning('Something is wrong!')
                        continue
                    results.append((item, result))
                results_only = map(lambda x: x[1], results)
                if isinstance(aggregation, (float, int)):
                    score = aggregation
                elif aggregation == 'Sum':
                    score = sum(results_only)
                elif aggregation == '%':
                    score = round((results_only.count(1) / float(len(results))) * 100, 2)
                else:
                    Log.warning('Somthing is wrong!')
                    continue
                json_data.append(WITH_ATOMICS.format(name=name, entity1=results[0][0],
                                                     entity2=results[1][0], entity3=results[2][0],
                                                     result1=results[0][1], result2=results[1][1],
                                                     result3=results[2][1], score=score))
            else:
                if score_type == '#!':
                    score = randint(1, 15)
                elif score_type == '%':
                    score = float('{}.{}'.format(randint(1, 100), randint(0, 99)))
                else:
                    Log.warning('Somthing is wrong!')
                    continue
                json_data.append(WITHOUT_ATOMICS.format(name=name, score=score))
        return "\n".join(json_data)


if __name__ == '__main__':
    LoggerInitializer.init('')
    Config.init()
    json_maker = JsonMaker()
    print json_maker.create_json()