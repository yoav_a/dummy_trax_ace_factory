
import xlrd
import xlsxwriter as xls
import pandas as pd

from Trax.Cloud.Services.Connector.Keys import DbUsers
from Trax.Utils.Conf.Configuration import Config
from Trax.Data.Projects.ProjectConnector import AwsProjectConnector

__author__ = 'Nimrod'

REFERENCE = 'Reference'
FIELD_NAME = 'Field Name'
NOT_EMPTY = 'Not Empty'
OF_TYPE = 'Of Type'
TABLES_WITH_UNIQUE_NAMES = {'static.survey_question': 'question_text',
                            'static.kpi': 'display_text',
                            'static.product': 'product_name',
                            'static.display': 'display_name'}


class GeneralConsts(object):

    SEPARATOR = ','

    # Case sensitive
    TOP_LEFT_HEADER = 'KPI NAME'

    ENTITIES_TO_VALIDATE = {
        'TEST': 'brand'
    }

    ENTITIES_FROM_REFERENCES_TO_VALIDATE = {
        'Value1': 'Param1'
    }

    VALIDATE_VALUE_LIST = {
        'Score': ['Binary', 'Percentage']
    }

    VALIDATE_NON_EMPTY = ['TEST']

    VALIDATE_TYPES = {
        'Weight': (float, int)
    }

    ENTITY_TO_DB_TABLE_CONVERSION = {
        'sub_category': ('sub_category', 'static.product'),
        'product_ean_code': ('product_ean_code', 'static.product'),
        'product_type': ('product_type', 'static.product'),
        'display_text': ('display_text', 'static.kpi'),
        'question_text': ('question_text', 'static.survey_question'),
        'product_name': ('product_name', 'static.product'),
        'product': 'static.product',
        'brand': 'static.brand',
        'sub_brand': 'static.sub_brand_name',
        'category': 'static.product_categories',
        'survey_question': 'static.survey_question',
        'survey': 'static.survey_question',
        'question': 'static.survey_question',
        'location': 'static.location_types',
        'template': 'static.template',
        'scene': 'static.template',
        'display': 'static.display',
        'kpi_set': 'static.kpi_set',
        'kpi': 'static.kpi',
        'atomic_kpi': 'static.atomic_kpi'
    }

    REQUIRED_HEADERS = set(ENTITIES_TO_VALIDATE.keys()).union(ENTITIES_FROM_REFERENCES_TO_VALIDATE.keys()).union(
        VALIDATE_VALUE_LIST.keys()).union(VALIDATE_NON_EMPTY).union(VALIDATE_TYPES.keys()).union([])


class TemplateValidator:

    def __init__(self, project, template_path, sheet_name=None, correction_path=None):
        self.project = project
        self.template_path = template_path
        self.sheet_name = sheet_name
        self.correction_path = correction_path
        self.workbook = xlrd.open_workbook(self.template_path)
        self.rds_conn = AwsProjectConnector(self.project, DbUsers.CalculationEng)
        self.corrections = {}
        self.comments = {}

    def validate_template(self):
        sheets = [self.sheet_name] if self.sheet_name else [sheet.name for sheet in self.workbook.sheets()]
        for sheet in sheets:
            sheet_data = self.workbook.sheet_by_name(sheet)
            sheet_validation = SheetValidator(self.template_path, sheet, sheet_data, self.rds_conn)
            sheet_validation.validate_sheet()
            if sheet_validation.corrections:
                self.corrections[sheet] = sheet_validation.corrections
            if sheet_validation.comments:
                self.comments[sheet] = sheet_validation.comments

        self.print_comments()
        if not self.comments:
            if not self.corrections:
                print 'Validated successfully - No errors were found in the template.'
            else:
                self.create_file_with_corrections()
        else:
            print "Correction file was not created. Please address the comments first."

    def create_file_with_corrections(self):
        workbook = xls.Workbook(self.correction_path)
        for sheet in self.workbook.sheets():
            corrections = self.corrections.get(sheet.name, {})
            worksheet = workbook.add_worksheet(sheet.name)
            for y_index, row in enumerate(sheet.get_rows()):
                for x_index, cell in enumerate(row):
                    if (x_index, y_index) in corrections.keys():
                        cell_value, correction_type = corrections.get((x_index, y_index))
                        cell_format = workbook.add_format(correction_type)
                        worksheet.write(y_index, x_index, cell_value, cell_format)
                    else:
                        worksheet.write(y_index, x_index, cell.value)
        workbook.close()

    def print_comments(self):
        for sheet in self.workbook.sheets():
            name = sheet.name
            if self.comments.get(name):
                print "SHEET '{}':".format(name)
                for comment in self.comments.get(name):
                    print "- {}".format(comment)
            print "\n"


class SheetValidator(GeneralConsts):

    COSMETIC = {'bg_color': '#C1FFC1'}
    WRONG_DATA = {'bg_color': '#FFB6C1'}
    WRONG_TYPE = {'bg_color': '#FFD700'}
    UNKNOWN_ENTITY = {'bg_color': '#FF6EB4'}

    def __init__(self, template_path, sheet_name, sheet_data, rds_conn):
        self.template_path = template_path
        self.sheet_name = sheet_name
        self.sheet_data = sheet_data
        self.rds_conn = rds_conn
        self.corrections = {}
        self.comments = []
        self.padding_x, self.padding_y = self.get_paddings()
        self.data = self.read_data()
        self.static_object_lists = {}

    def get_paddings(self):
        rows = self.sheet_data.get_rows()
        padding_y = -1
        header_row = None
        while header_row is None:
            try:
                current_row = [cell.value for cell in rows.next()]
                if self.TOP_LEFT_HEADER in current_row:
                    header_row = current_row
                padding_y += 1
            except StopIteration:
                comment = "'{}' doesn't appear in the sheet".format(self.TOP_LEFT_HEADER)
                if comment not in self.comments:
                    self.comments.append(comment)
                return None, None
        padding_x = header_row.index(self.TOP_LEFT_HEADER)
        return padding_x, padding_y

    def validate_sheet(self):
        if self.data is None:
            return
        self.validate_headers()
        if not self.comments:
            self.validate_cells()

    def read_data(self):
        if self.padding_x is None or self.padding_y is None:
            return None
        data = pd.read_excel(self.template_path, self.sheet_name, skiprows=self.padding_y).fillna('').astype(str)
        data = data.apply(lambda x: x.str.strip())
        duplicated_columns = []
        cols_to_delete = []
        cols_to_rename = {}
        for index, column in enumerate(data.columns):
            if column.startswith('Unnamed: '):
                cols_to_delete.append(column)
            elif index < self.padding_x:
                cols_to_delete.append(column)
            elif isinstance(column, str):
                cols_to_rename[column] = str(column)
            elif column.endswith('.1'):
                duplicated_columns.append(column[:-2])
        if duplicated_columns:
            comment = 'The following column-titles appear more than once: {}'.format(', '.join(duplicated_columns))
            self.comments.append(comment)
            return None
        for column in cols_to_delete:
            del data[column]
        data = data.rename(columns=cols_to_rename)

        data = self.cosmetic_modification(data)
        return data

    def validate_headers(self):
        missing_headers = set(self.REQUIRED_HEADERS).difference(self.data.keys())
        if missing_headers:
            comment = 'Headers ({}) must appear in sheet'.format(', '.join(missing_headers))
            self.comments.append(comment)

    def validate_cells(self):
        for y in xrange(len(self.data)):
            row = self.data.iloc[y]
            for x, column in enumerate(self.data.columns):
                cell = row[column]
                unknown_entity = False
                wrong_data = False
                wrong_type = False

                if cell and column in self.ENTITIES_TO_VALIDATE.keys():
                    field = self.ENTITIES_TO_VALIDATE.get(column)
                    value_in_object_list = self.is_value_in_object_list(field, cell)
                    if value_in_object_list is None:
                        unknown_entity = True
                    elif not value_in_object_list:
                        wrong_data = True
                elif cell and column in self.ENTITIES_FROM_REFERENCES_TO_VALIDATE.keys():
                    reference_column = self.ENTITIES_FROM_REFERENCES_TO_VALIDATE.get(column)
                    field = row[reference_column]
                    value_in_object_list = self.is_value_in_object_list(field, cell)
                    if value_in_object_list is None:
                        unknown_entity = reference_column
                    elif not value_in_object_list:
                        wrong_data = True
                elif column in self.VALIDATE_VALUE_LIST.keys():
                    value_list = self.VALIDATE_VALUE_LIST.get(column)
                    if cell not in value_list:
                        wrong_data = True

                if not wrong_data and not unknown_entity:
                    if column in self.VALIDATE_NON_EMPTY:
                        if not cell:
                            wrong_type = True
                    elif column in self.VALIDATE_TYPES:
                        try:
                            instances = self.VALIDATE_TYPES.get(column)
                            if not isinstance(eval(str(cell)), instances):
                                wrong_type = True
                        except:
                            wrong_type = True

                if unknown_entity:
                    self.add_correction(x, y + 1, cell, self.UNKNOWN_ENTITY)
                    if unknown_entity is not True:
                        entity_index = self.data.columns.tolist().index(unknown_entity)
                        self.add_correction(entity_index, y + 1, row[reference_column], self.UNKNOWN_ENTITY)
                elif wrong_data:
                    self.add_correction(x, y + 1, cell, self.WRONG_DATA)
                elif wrong_type:
                    self.add_correction(x, y + 1, cell, self.WRONG_TYPE)

    def is_value_in_object_list(self, field, value):
        if '_' in field:
            basic_form_of_field = '_'.join(field.split('_')[:-1])
        else:
            basic_form_of_field = field
        if isinstance(self.ENTITY_TO_DB_TABLE_CONVERSION.get(field), tuple):
            field, table = self.ENTITY_TO_DB_TABLE_CONVERSION.get(field)
        elif isinstance(self.ENTITY_TO_DB_TABLE_CONVERSION.get(basic_form_of_field), tuple):
            field, table = self.ENTITY_TO_DB_TABLE_CONVERSION.get(basic_form_of_field)
        elif basic_form_of_field in self.ENTITY_TO_DB_TABLE_CONVERSION.keys():
            table = self.ENTITY_TO_DB_TABLE_CONVERSION.get(basic_form_of_field)
            if field.endswith('_id') or field.endswith('_fk'):
                field = 'pk'
            else:
                field = TABLES_WITH_UNIQUE_NAMES.get(table, 'name')
        else:
            return None

        if (field, value) not in self.static_object_lists.keys():
            query = 'select {} from {}'.format(field, table)
            object_list = pd.read_sql_query(query, self.rds_conn.db)[field].unique().tolist()
            self.static_object_lists[(field, table)] = object_list
        else:
            object_list = self.static_object_lists.get((field, value))

        values = value.split(self.SEPARATOR)
        for value in values:
            if value.isdigit():
                if value not in object_list and int(value) not in object_list:
                    return False
            else:
                if value not in object_list:
                    return False
        return True

    def cosmetic_modification(self, data):
        capital_required_fields = {field.upper(): field for field in self.REQUIRED_HEADERS}
        for index, header in enumerate(data.columns):
            if header not in self.REQUIRED_HEADERS:
                if header.upper() in capital_required_fields.keys():
                    new_header = capital_required_fields[header.upper()]
                    data = data.rename(columns={header: new_header})
                    self.add_correction(index, 0, new_header, self.COSMETIC)

        columns = data.columns
        for y in xrange(len(data)):
            row = data.iloc[y]
            for x, column in enumerate(columns):
                if column in self.REQUIRED_HEADERS:
                    value = unicode(row[column])
                    if value:
                        separator = self.SEPARATOR.strip()
                        new_value = self.SEPARATOR.join([v.strip() for v in value.split(separator)])
                        if value != new_value:
                            data.iloc[y, x] = new_value
                            self.add_correction(x, y + 1, new_value, self.COSMETIC)
        return data

    def add_correction(self, x, y, new_value=None, correction_type=WRONG_DATA):
        cell_position = (self.padding_x + x, self.padding_y + y)
        correction = [new_value, correction_type]
        self.corrections[cell_position] = correction


# if __name__ == '__main__':
#     Config.init()
#     project = 'gilletteus'
#     input_path = '/home/Nimrod/Desktop/Test.xlsx'
#     output_path = '/home/Nimrod/Desktop/Tes2t.xlsx'
#     TemplateValidator(project, input_path, correction_path=output_path).validate_template()