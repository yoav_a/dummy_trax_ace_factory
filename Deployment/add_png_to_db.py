
import pandas as pd

from Trax.Data.Projects.ProjectConnector import AwsProjectConnector
from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Logging.Logger import Log
from Trax.Cloud.Services.Connector.Logger import LoggerInitializer
from Trax.Cloud.Services.Connector.Keys import DbUsers

__author__ = 'Nimrod'


class AddKPIs:
    """
    This module writes all levels of KPIs to the DB, given a template.

    - The template file must include a unique row for every Atomic KPI (and ONLY Atomics)
    - Each row much include a set-name and a KPI-name columns (configured in 'Consts')
    - For every level of KPI (set, KPI or atomic) - only the ones who do not already exist would be added to the DB

    (Example for template: GILLETTEUS/Data/Template.xlsx)
    """
    def __init__(self, project, template_path, custom_mode=False):
        self.project = project
        self.aws_conn = AwsProjectConnector(self.project, DbUsers.CalculationEng)
        self.kpi_static_data = self.get_kpi_static_data()
        self.data = pd.read_excel(template_path, sheetname='KPIs')
        self.custom_mode = custom_mode
        self.sets_added = {}
        self.kpis_added = {}
        self.kpi_counter = {'set': 0, 'kpi': 0, 'atomic': 0}

    def get_kpi_static_data(self):
        """
        This function extracts the static KPI data and saves it into one global data frame.
        The data is taken from static.kpi / static.atomic_kpi / static.kpi_set.
        """
        query = """
                select api.name as atomic_kpi_name, api.pk as atomic_kpi_fk,
                       kpi.display_text as kpi_name, kpi.pk as kpi_fk,
                       kps.name as kpi_set_name, kps.pk as kpi_set_fk
                from static.atomic_kpi api
                left join static.kpi kpi on kpi.pk = api.kpi_fk
                join static.kpi_set kps on kps.pk = kpi.kpi_set_fk
                """
        kpi_static_data = pd.read_sql_query(query, self.aws_conn.db)
        return kpi_static_data

    def add_kpis_from_template(self, categories=[]):
        for category in categories:
            self.add_sets_to_static(category)
            self.add_kpis_to_static(category)
        Log.info('{} Sets, {} KPIs have been added'.format(self.kpi_counter['set'],
                                                           self.kpi_counter['kpi']))

    def add_sets_to_static(self, category):
        set_names = self.data[self.data.columns[0]]
        set_names = [name.format(category=category) for name in set_names]
        existing_set_names = self.kpi_static_data['kpi_set_name'].unique().tolist()
        set_names_to_add = set(set_names).difference(existing_set_names)
        if set_names_to_add:
            cur = self.aws_conn.db.cursor()
            for set_name in set_names_to_add:
                level1_query = """
                               INSERT INTO static.kpi_set (name, missing_kpi_score, enable, normalize_weight,
                                                           expose_to_api, is_in_weekly_report)
                               VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');""".format(set_name.encode('utf-8'), 'Bad', 'Y',
                                                                                            'N', 'N', 'N')
                cur.execute(level1_query)
                self.sets_added[set_name] = cur.lastrowid
                self.kpi_counter['set'] += 1
            self.aws_conn.db.commit()

    def add_kpis_to_static(self, category):
        kpis = self.data[self.data.columns[0]]
        kpis = [name.format(category=category) for name in kpis]
        cur = self.aws_conn.db.cursor()
        for kpi_name in kpis:
            if self.kpi_static_data[(self.kpi_static_data['kpi_set_name'] == kpi_name) &
                                    (self.kpi_static_data['kpi_name'] == kpi_name)].empty:
                if kpi_name in self.sets_added.keys():
                    set_fk = self.sets_added[kpi_name]
                else:
                    set_fk = self.kpi_static_data[self.kpi_static_data['kpi_set_name'] == kpi_name]['kpi_set_fk'].values[0]
                level2_query = """
                       INSERT INTO static.kpi (kpi_set_fk, display_text)
                       VALUES ('{0}', '{1}');""".format(set_fk, kpi_name.encode('utf-8'))
                cur.execute(level2_query)
                if kpi_name in self.kpis_added.keys():
                    self.kpis_added[kpi_name][kpi_name] = cur.lastrowid
                else:
                    self.kpis_added[kpi_name] = {kpi_name: cur.lastrowid}
                self.kpi_counter['kpi'] += 1
        self.aws_conn.db.commit()

if __name__ == '__main__':
    LoggerInitializer.init('')
    Config.init()
    kpi = AddKPIs('pngau', '/home/Nimrod/Documents/PNGAU_STATIC.xlsx')
    kpi.add_kpis_from_template(categories=['Air Care'])
