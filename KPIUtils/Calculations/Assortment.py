from datetime import datetime
import json
import pandas as pd
import numpy as np
from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from KPIUtils.GlobalDataProvider.PsDataProvider import PsDataProvider
from KPIUtils_v2.GlobalDataProvider.PsDataProvider import PsDataProvider


class Assortment(object):

    def __init__(self, data_provider, output, ps_data_provider=None):
        self.output = output
        self.data_provider = data_provider
        self.project_name = self.data_provider.project_name
        # self.session_uid = self.data_provider.session_uid
        # self.session_id = self.data_provider.session_id
        # self.products = self.data_provider[Data.PRODUCTS]
        # self.all_products = self.data_provider[Data.ALL_PRODUCTS]
        self.match_product_in_scene = self.data_provider[Data.MATCHES]
        # self.visit_date = self.data_provider[Data.VISIT_DATE]
        # self.session_info = self.data_provider[Data.SESSION_INFO]
        # self.scene_info = self.data_provider[Data.SCENES_INFO]
        # self.store_id = self.data_provider[Data.STORE_FK]
        # self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        # self.templates = self.data_provider[Data.ALL_TEMPLATES]
        self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.kpi_results_new_tables_queries = []
        self.ps_data_provider = ps_data_provider
        if ps_data_provider is None:
            self.ps_data_provider = PsDataProvider(data_provider, output)
        self.store_assortment = self.ps_data_provider.store_ass
        self.store_info = self.data_provider[Data.STORE_INFO]
        self.store_info = self.ps_data_provider.get_ps_store_info(self.store_info)
        self.current_date = datetime.now()
        self.LVL3_HEADERS = ['assortment_group_fk', 'assortment_fk', 'target', 'product_fk',
                             'in_store', 'kpi_fk_lvl1', 'kpi_fk_lvl2', 'kpi_fk_lvl3',
                             'group_target_date', 'assortment_super_group_fk', 'super_group_target']
        self.LVL2_HEADERS = ['assortment_super_group_fk', 'assortment_group_fk', 'assortment_fk',
                             'target', 'passes', 'total', 'kpi_fk_lvl1', 'kpi_fk_lvl2',
                             'group_target_date', 'super_group_target']
        self.LVL1_HEADERS = ['assortment_super_group_fk', 'assortment_group_fk',
                             'super_group_target', 'passes', 'total', 'kpi_fk_lvl1']
        self.ASSORTMENT_FK = 'assortment_fk'
        self.ASSORTMENT_GROUP_FK = 'assortment_group_fk'
        self.ASSORTMENT_SUPER_GROUP_FK = 'assortment_super_group_fk'

    def get_lvl3_relevant_ass(self):
        assortment_result = pd.DataFrame(columns=self.LVL3_HEADERS)
        filters = self.LVL3_HEADERS
        if 'in_store' in filters:
            filters.remove('in_store')
        for row in self.store_assortment[['policy', 'assortment_group_fk']].drop_duplicates().itertuples():
            policies = json.loads(row.policy)
            df = self.store_info
            existing_key = True
            for key, value in policies.items():
                try:
                    if key == 'store_fk':
                        value = [int(i) for i in value]
                    df = df[df[key].isin(value)]
                except KeyError:
                    existing_key = False
                    break
            if not df.empty and existing_key:
                assortments = self.store_assortment[(self.store_assortment[self.ASSORTMENT_GROUP_FK]
                                                     == row.assortment_group_fk) & (self.store_assortment
                                                                                    [
                                                                                        'start_date'] <= self.current_date)][
                    filters]
                assortments['in_store'] = 0
                if assortment_result.empty:
                    assortment_result = assortments
                else:
                    assortment_result = assortment_result.append(assortments, ignore_index=True)
        return assortment_result.drop_duplicates(subset=[self.ASSORTMENT_GROUP_FK,
                                                                      self.ASSORTMENT_FK, 'product_fk'], keep='last')

    def calculate_lvl3_assortment(self):
        assortment_result = self.get_lvl3_relevant_ass()
        products_in_session = self.match_product_in_scene['product_fk'].values
        assortment_result.loc[assortment_result['product_fk'].isin(products_in_session), 'in_store'] = 1
        return assortment_result

    def calculate_lvl2_assortment(self, lvl3_assortment):
        lvl2_res = pd.DataFrame(columns=self.LVL2_HEADERS)
        assortments = lvl3_assortment[[self.ASSORTMENT_GROUP_FK, self.ASSORTMENT_FK,
                                   self.ASSORTMENT_SUPER_GROUP_FK]].drop_duplicates(subset=
                                   [self.ASSORTMENT_GROUP_FK, self.ASSORTMENT_FK,
                                    self.ASSORTMENT_SUPER_GROUP_FK], keep='last')
        filters = self.LVL2_HEADERS
        if 'passes' in filters:
            filters.remove('passes')
        if 'total' in filters:
            filters.remove('total')
        lvl2_res[filters] = lvl3_assortment[filters]
        lvl2_res = lvl2_res.drop_duplicates(subset=[self.ASSORTMENT_SUPER_GROUP_FK,
                                                    self.ASSORTMENT_GROUP_FK, 'target'],
                                            keep='last')
        for ass in assortments.itertuples():
            lvl2_res.loc[(lvl2_res[self.ASSORTMENT_FK] == ass.assortment_fk) &
                         (lvl2_res[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk), 'passes'] = \
                len(lvl3_assortment[(lvl3_assortment[self.ASSORTMENT_FK] == ass.assortment_fk) &
                                    (lvl3_assortment[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk) &
                                    (lvl3_assortment['in_store'] == 1)])
            lvl2_res.loc[(lvl2_res[self.ASSORTMENT_FK] == ass.assortment_fk) &
                         (lvl2_res[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk), 'total'] = \
                len(lvl3_assortment[(lvl3_assortment[self.ASSORTMENT_FK] == ass.assortment_fk) &
                                    (lvl3_assortment[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk)])
        return lvl2_res

    def calculate_lvl1_assortment(self, lvl2_result):
        lvl1_res = pd.DataFrame(columns=self.LVL1_HEADERS)
        ass_super_groups = lvl2_result[~lvl2_result['kpi_fk_lvl1'].isnull()][self.ASSORTMENT_SUPER_GROUP_FK]\
            .unique()
        filters = self.LVL1_HEADERS
        filters.remove('passes')
        filters.remove('total')
        lvl1_res[filters] = \
            lvl2_result[lvl2_result[self.ASSORTMENT_SUPER_GROUP_FK].isin(ass_super_groups)][filters]
        lvl1_res = lvl1_res.drop_duplicates(subset=[self.ASSORTMENT_SUPER_GROUP_FK],
                                            keep='last')
        for group in ass_super_groups:
            lvl1_res.loc[lvl1_res[self.ASSORTMENT_SUPER_GROUP_FK] == group, 'passes'] = \
                len(lvl2_result[(lvl2_result[self.ASSORTMENT_SUPER_GROUP_FK] == group) &
                                (lvl2_result['passes'] >= lvl2_result['target'])])
            lvl1_res.loc[lvl1_res[self.ASSORTMENT_SUPER_GROUP_FK] == group, 'total'] = \
                len(lvl2_result[lvl2_result[self.ASSORTMENT_SUPER_GROUP_FK] == group])
        return lvl1_res