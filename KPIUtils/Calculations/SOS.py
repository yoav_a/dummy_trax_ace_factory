from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Algo.Calculations.Core.Shortcuts import BaseCalculationsGroup
from Trax.Utils.Logging.Logger import Log


class SOS(object):

    EXCLUDE_FILTER = 0
    INCLUDE_FILTER = 1
    CONTAIN_FILTER = 2
    EXCLUDE_EMPTY = False
    INCLUDE_EMPTY = True

    STRICT_MODE = ALL = 1000

    EMPTY = 'Empty'
    DEFAULT = 'Default'
    TOP = 'Top'
    BOTTOM = 'Bottom'

    def __init__(self, data_provider, output, ignore_stacking=False):
        self.data_provider = data_provider
        self.ignore_stacking = ignore_stacking
        self.k_engine = BaseCalculationsGroup(data_provider, output)
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.facings_field = 'facings' if not self.ignore_stacking else 'facings_ign_stack'
        self.scenes_info = self.data_provider[Data.SCENES_INFO]

    @staticmethod
    def merge_two_dicts(x, y):
        z = x.copy()  # start with x's keys and values
        z.update(y)  # modifies z with y's keys and values & returns None
        return z


    def get_filter_condition(self, df, **filters):
        """
        :param df: The data frame to be filters.
        :param filters: These are the parameters which the data frame is filtered by.
                       Every parameter would be a tuple of the value and an include/exclude flag.
                       INPUT EXAMPLE (1):   manufacturer_name = ('Diageo', DIAGEOAUREDGENERALToolBox.INCLUDE_FILTER)
                       INPUT EXAMPLE (2):   manufacturer_name = 'Diageo'
        :return: a filtered Scene Item Facts data frame.
        """
        if not filters:
            return df['pk'].apply(bool)
        if self.facings_field in df.keys():
            filter_condition = (df[self.facings_field] > 0)
        else:
            filter_condition = None
        for field in filters.keys():
            if field in df.keys():
                if isinstance(filters[field], tuple):
                    value, exclude_or_include = filters[field]
                else:
                    value, exclude_or_include = filters[field], self.INCLUDE_FILTER
                if not value:
                    continue
                if not isinstance(value, list):
                    value = [value]
                if exclude_or_include == self.INCLUDE_FILTER:
                    condition = (df[field].isin(value))
                elif exclude_or_include == self.EXCLUDE_FILTER:
                    condition = (~df[field].isin(value))
                elif exclude_or_include == self.CONTAIN_FILTER:
                    condition = (df[field].str.contains(value[0], regex=False))
                    for v in value[1:]:
                        condition |= df[field].str.contains(v, regex=False)
                else:
                    continue
                if filter_condition is None:
                    filter_condition = condition
                else:
                    filter_condition &= condition
            else:
                Log.warning('field {} is not in the Data Frame'.format(field))

        return filter_condition

    def calculate_sos_facing_by_scene(self,sos_filters ,include_empty=False, **general_filters):
        sos_by_scene_results = { }
        if include_empty == self.EXCLUDE_EMPTY and 'product_type' not in sos_filters.keys() + general_filters.keys():
            general_filters['product_type'] = (self.EMPTY, self.EXCLUDE_FILTER)

        try:
            for scene in self.scenes_info['scene_fk'].tolist():

                scene_filter = {'scene_fk': scene}
                general_filters = self.merge_two_dicts(general_filters, scene_filter)
                #  denominator
                pop_filter = self.get_filter_condition(self.scif, **general_filters)
                #  nominator
                subset_filter = self.get_filter_condition(self.scif, **sos_filters)

                try:
                    sos_by_scene_results[scene] = self.k_engine.calculate_sos_by_facings(pop_filter=pop_filter, subset_filter=subset_filter)
                except Exception as e:
                    Log.debug('calculate_sos_facing_by_scene can not be calculated for scene {} : {}'.format(scene, e.message))
            Log.info('calculate_sos_facing_by_scene results: {}'.format(sos_by_scene_results))
            return sos_by_scene_results

        except Exception as e:
            Log.debug('calculate_sos_facing_by_scene can not be calculated for scene {} : {}'.format(scene, e.message))
