from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Utils.Testing.Case import MockingTestCase, TestCase
from mock import MagicMock, mock
from KPIUtils.Calculations import Survey
import data.survey_test_data as testdata



class CalculationSurveyTest(TestCase):

    #@mock.patch('Projects.CCBOTTLERSUS.REDSCORE.KPIToolBox.ProjectConnector')
    def setUp(self):
        self.data_provider_mock = MagicMock()
        self.data_provider_mock.project_name = 'ccbottlersus'
        self.data_provider_mock.rds_conn = MagicMock()
        self.data_provider_mock.survey_responses = testdata.get_survey_test_date()
        self.output = MagicMock()
        self.survey = Survey.Survey(self.data_provider_mock, self.output)

    def test_get_survey_answer(self):
        survey_data = MagicMock()
        testdata.get_survey_test_date()
        self.assertIsNone(self.survey.get_survey_answer(survey_data))
        self.data_provider_mock.survey_responses = testdata.get_survey_test_date()
        self.assertEqual(self.survey.get_survey_answer(('question_fk', 96)), u'NO')

        self.assertIsNone(self.survey.get_survey_answer(('question_fk', 1111)))

