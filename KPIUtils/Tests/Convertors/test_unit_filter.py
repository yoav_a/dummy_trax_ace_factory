import os

from Trax.Algo.Calculations.Core.DataProvider import KEngineDataProvider
from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Testing.Case import MockingTestCase
from mock import MagicMock, patch

from Projects.CCBOTTLERSUS.REDSCORE.Const import Const
from KPIUtils.Utils.Convertors.FilterHandler import FilterGenerator


class FilterTests(MockingTestCase):
    @property
    def import_path(self):
        return 'KPIUtils.Utils.Convertors.FilterHandler'

    def test_convert_column_to_scif_filter(self):
        filter_gen = FilterGenerator()

        self.assertEquals(filter_gen._convert_column_to_scif_filter(Const.SCENE_TYPE), 'template_name')

