from datetime import datetime
import pandas as pd


from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Cloud.Services.Connector.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Data.Utils.MySQLservices import get_table_insertion_query as insert
from Trax.Utils.Logging.Logger import Log

from KPIUtils.Utils.Helpers.LogHandler import log_handler
from KPIUtils.DB.Queries import Queries

__author__ = 'Ilan'

PSERVICE_CUSTOM_SCIF = 'pservice.custom_scene_item_facts'


def log_runtime(description, log_start=False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            calc_start_time = datetime.utcnow()
            if log_start:
                Log.info('{} started at {}'.format(description, calc_start_time))
            result = func(*args, **kwargs)
            calc_end_time = datetime.utcnow()
            Log.info('{} took {}'.format(description, calc_end_time - calc_start_time))
            return result

        return wrapper

    return decorator


class Common(object):
    def __init__(self, data_provider, kpi=None):

        self.data_provider = data_provider
        self.kpi = kpi
        self.project_name = self.data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.session_id = self.data_provider.session_id
        self.store_id = self.data_provider[Data.STORE_FK]
        self.visit_date = self.data_provider[Data.VISIT_DATE] if self.data_provider[Data.STORE_FK] else None
        self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.kpi_static_data = self.get_kpi_static_data()
        self.kpi_results_queries = []
        self.kpi_results_new_tables_queries = []
        self.custom_scif_queries = []
        self.queries = Queries
        self.New_kpi_static_data = self.get_new_kpi_static_data()
        self.LEVEL1 = 1
        self.LEVEL2 = 2
        self.LEVEL3 = 3
        self.KPI_RESULT = 'report.kpi_results'
        self.KPK_RESULT = 'report.kpk_results'
        self.KPS_RESULT = 'report.kps_results'
        self.KPI_NEW_TABLE = 'report.kpi_level_2_results'

    def get_kpi_static_data(self):
        """
        This function extracts the static KPI data and saves it into one global data frame.
        The data is taken from static.kpi / static.atomic_kpi / static.kpi_set.
        """
        if self.kpi:
            query = Queries.get_all_kpi_data_by_kpi_set(self.kpi)
        else:
            query = Queries.get_all_kpi_data()
        kpi_static_data = pd.read_sql_query(query, self.rds_conn.db)
        return kpi_static_data

    def get_new_kpi_static_data(self):
        """
            This function extracts the static new KPI data (new tables) and saves it into one global data frame.
            The data is taken from static.kpi_level_2.
            """
        query = self.queries.get_new_kpi_data()
        local_con = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        kpi_static_data = pd.read_sql_query(query, local_con.db)
        # query = Queries.get_new_kpi_data()
        # kpi_static_data = pd.read_sql_query(query, self.rds_conn.db)
        return kpi_static_data

    def write_to_db_result(self, fk, level, score, **kwargs):
        """
        This function creates the result data frame of every KPI (atomic KPI/KPI/KPI set),
        and appends the insert SQL query into the queries' list, later to be written to the DB.
        """
        if kwargs:
            kwargs['score'] = score
            attributes = self.create_attributes_dict(level=level, **kwargs)
        else:
            attributes = self.create_attributes_dict(fk=fk, score=score, level=level)


        if level == self.LEVEL1:
            table = self.KPS_RESULT
        elif level == self.LEVEL2:
            table = self.KPK_RESULT
        elif level == self.LEVEL3:
            table = self.KPI_RESULT
        else:
            return
        query = insert(attributes, table )
        self.kpi_results_queries.append(query)

    def get_session_id(self, session_pk):
        query = Queries.get_session_id_query(session_pk)
        session_id = pd.read_sql_query(query, self.rds_conn.db)
        return session_id

    def create_basic_dict(self,fk=None , **kwargs):
        data = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]
        # atomic_kpi_name = data['atomic_kpi_name'].values[0]
        kpi_fk = data['kpi_fk'].values[0]
        kpi_set_name = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]['kpi_set_name'].values[
            0]

        kwargs['session_uid'] = self.session_uid
        kwargs['kps_name'] = kpi_set_name
        kwargs['store_fk'] = self.store_id
        kwargs['visit_date'] = self.visit_date.isoformat()
        kwargs['calculation_time'] = datetime.utcnow().isoformat()
        kwargs['kpi_fk'] = kpi_fk
        kwargs['atomic_kpi_fk'] = fk

        return kwargs

    def create_attributes_dict(self,score, fk=None,level=None, **kwargs):
        """
        This function creates a data frame with all attributes needed for saving in KPI results tables.
        or
        you can send dict with all values in kwargs

        """

        if level == self.LEVEL1:
            if kwargs:
                kwargs['score'] = score
                values = [val for val in kwargs.values()]
                col = [col for col in kwargs.keys()]
                attributes = pd.DataFrame(values,
                                          columns=col)
            else:
                kpi_set_name = self.kpi_static_data[self.kpi_static_data['kpi_set_fk'] == fk]['kpi_set_name'].values[0]
                attributes = pd.DataFrame([(kpi_set_name, self.session_uid, self.store_id, self.visit_date.isoformat(),
                                            format(score, '.2f'), fk)],
                                          columns=['kps_name', 'session_uid', 'store_fk', 'visit_date', 'score_1',
                                                   'kpi_set_fk'])
        elif level == self.LEVEL2:
            if kwargs:
                kwargs['score'] = score
                values = [val for val in kwargs.values()]
                col = [col for col in kwargs.keys()]
                attributes = pd.DataFrame(values,
                                          columns=col)
            else:
                kpi_name = self.kpi_static_data[self.kpi_static_data['kpi_fk'] == fk]['kpi_name'].values[0]
                attributes = pd.DataFrame([(self.session_uid, self.store_id, self.visit_date.isoformat(),
                                            fk, kpi_name, score)],
                                          columns=['session_uid', 'store_fk', 'visit_date', 'kpi_fk', 'kpk_name', 'score'])
        elif level == self.LEVEL3:
            if kwargs:
                kwargs['score'] = score
                values = tuple([val for val in kwargs.values()])
                col = [col for col in kwargs.keys()]
                attributes = pd.DataFrame([values], columns=col)
            else:
                data = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]
                atomic_kpi_name = data['atomic_kpi_name'].values[0]
                kpi_fk = data['kpi_fk'].values[0]
                kpi_set_name = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]['kpi_set_name'].values[
                    0]

                attributes = pd.DataFrame([(atomic_kpi_name, self.session_uid, kpi_set_name, self.store_id,
                                            self.visit_date.isoformat(), datetime.utcnow().isoformat(),
                                            score, kpi_fk, fk)],
                                          columns=['display_text', 'session_uid', 'kps_name', 'store_fk', 'visit_date',
                                                   'calculation_time', 'score', 'kpi_fk', 'atomic_kpi_fk'])
        else:
            attributes = pd.DataFrame()
        return attributes.to_dict()

    @log_handler.log_runtime('Saving to DB')
    def commit_results_data(self):
        """
        This function writes all KPI results to the DB, and commits the changes.
        """
        insert_queries = self.merge_insert_queries(self.kpi_results_queries)
        cur = self.rds_conn.db.cursor()
        delete_queries = Queries.get_delete_session_results_query(self.session_uid)
        for query in delete_queries:
            cur.execute(query)
        for query in insert_queries:
            cur.execute(query)
        self.rds_conn.db.commit()

    @log_handler.log_runtime('Saving to DB')
    def commit_results_data_without_delete(self):
        """
        This function writes all KPI results to the DB, and commits the changes.
        """
        insert_queries = self.merge_insert_queries(self.kpi_results_queries)
        cur = self.rds_conn.db.cursor()
        for query in insert_queries:
            cur.execute(query)
        self.rds_conn.db.commit()

    @log_handler.log_runtime('Saving to DB')
    def commit_custom_scif(self):
        if not self.rds_conn.is_connected:
            self.rds_conn.connect_rds()
        cur = self.rds_conn.db.cursor()
        delete_query = Queries.get_delete_session_custom_scif(self.session_id)
        cur.execute(delete_query)
        self.rds_conn.db.commit()
        queries = self.merge_insert_queries(self.custom_scif_queries)
        for query in queries:
            try:
                cur.execute(query)
            except:
                print 'could not run query: {}'.format(query)
        self.rds_conn.db.commit()

    @log_handler.log_runtime('Saving to DB')
    def delete_results_data_by_kpi_set(self):
        """
        This function writes all KPI results to the DB, and commits the changes.
        """
        if self.kpi:
            kpi_set_fk = self.get_kpi_fk_by_kpi_name(self.kpi, self.LEVEL1)
            cur = self.rds_conn.db.cursor()
            delete_queries = Queries.get_delete_session_results_query_by_kpi(self.session_uid, kpi_set_fk)
            for query in delete_queries:
                cur.execute(query)
            self.rds_conn.db.commit()

    @staticmethod
    def merge_insert_queries(insert_queries):
        query_groups = {}
        for query in insert_queries:
            static_data, inserted_data = query.split('VALUES ')
            if static_data not in query_groups:
                query_groups[static_data] = []
            query_groups[static_data].append(inserted_data)
        merged_queries = []
        for group in query_groups:
            merged_queries.append('{0} VALUES {1}'.format(group, ',\n'.join(query_groups[group])))
        return merged_queries

    def get_kpi_fk_by_kpi_name_new_tables(self, kpi_name):
        return self.New_kpi_static_data.loc[self.New_kpi_static_data['client_name'] == kpi_name]['pk'].values[0]


    def get_kpi_fk_by_kpi_name(self, kpi_name, kpi_level):
        '''
        convert kpi name to kpi_fk
        for all level

        :param kpi_name: string
        :return:
        fk

        '''

        assert isinstance(kpi_name, (unicode, basestring)), "name is not a string: %r" % kpi_name

        if kpi_level == self.LEVEL1:
            column_key = 'kpi_set_fk'
            column_value = 'kpi_set_name'

        elif kpi_level == self.LEVEL2:
            column_key = 'kpi_fk'
            column_value = 'kpi_name'

        elif kpi_level == self.LEVEL3:
            column_key = 'atomic_kpi_fk'
            column_value = 'atomic_kpi_name'

        else:
            raise ValueError, 'invalid level'

        try:
            if column_key and column_value:
                return self.kpi_static_data[self.kpi_static_data[column_value] == kpi_name][column_key].values[0]

        except IndexError:
            Log.info('Kpi name: {}, isnt equal to any kpi name in static table'.format(kpi_name))
            return None

    def write_to_db_result_new_tables(self, fk, numerator_id, numerator_result, result, denominator_id=0,
                                      denominator_result=0, score=0, score_after_actions=0, context_id=None,
                                      denominator_result_after_actions=None):
        """
            This function creates the result data frame of new rables KPI,
            and appends the insert SQL query into the queries' list, later to be written to the DB.
            """
        table = self.KPI_NEW_TABLE
        attributes = self.create_attributes_dict_new_tables(fk, numerator_id, numerator_result, denominator_id,
                                                            denominator_result, result, score, score_after_actions,
                                                            context_id, denominator_result_after_actions)
        query = insert(attributes, table)
        self.kpi_results_new_tables_queries.append(query)

    def create_attributes_dict_new_tables(self, kpi_fk, numerator_id, numerator_result, denominator_id,
                                          denominator_result, result, score, score_after_actions, context_id,
                                          denominator_result_after_actions):
        """
        This function creates a data frame with all attributes needed for saving in KPI results new tables.
        """
        attributes = pd.DataFrame([(kpi_fk, self.session_id, numerator_id, numerator_result, denominator_id,
                                    denominator_result, result, score, score_after_actions, context_id,
                                    denominator_result_after_actions)], columns=['kpi_level_2_fk',
                                                                                 'session_fk', 'numerator_id',
                                                                                 'numerator_result', 'denominator_id',
                                                                                 'denominator_result', 'result',
                                                                                 'score', 'score_after_actions',
                                                                                 'context_id',
                                                                                 'denominator_result_after_actions'])
        return attributes.to_dict()

    @log_runtime('Saving to DB')
    def commit_results_data_to_new_tables(self):
        """
        This function writes all KPI results to the DB, and commits the changes.
        """
        insert_queries = self.merge_insert_queries(self.kpi_results_new_tables_queries)
        delete_query = self.queries.get_delete_session_results_query_from_new_tables(self.session_id)
        Log.info('Start committing results')
        local_con = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        cur = local_con.db.cursor()
        # for query in delete_queries:
        cur.execute(delete_query)
        for query in insert_queries:
            cur.execute(query)
        local_con.db.commit()
        Log.info('Finish committing results')

    def get_custom_scif_query(self, scene_fk, product_fk, in_assortment_OSA, oos_osa, mha_in_assortment, mha_oos,
                             length_mm_custom):

        """
        This gets the query for insertion to PS custom scif
        :param length_mm_custom:
        :param mha_oos:
        :param mha_in_assortment:
        :param oos_osa:
        :param in_assortment_OSA:
        :param scene_fk:
        :param product_fk:
        :return:
        """
        attributes = pd.DataFrame([(
            self.session_id, scene_fk, product_fk, in_assortment_OSA, oos_osa, mha_in_assortment,
            mha_oos, length_mm_custom)],
            columns=['session_fk', 'scene_fk', 'product_fk', 'in_assortment_OSA', 'oos_osa',
                     'mha_in_assortment', 'mha_oos', 'length_mm_custom'])

        query = insert(attributes.to_dict(), PSERVICE_CUSTOM_SCIF)
        self.custom_scif_queries.append(query)

# if __name__ == '__main__':
#     c = Common('r')
#
#     d = {
#         'test': 't',
#          'test2': 't2'
#     }
#     c.create_attributes_dict(**d)

