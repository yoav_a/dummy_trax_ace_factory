__author__ = 'Ilan'


class FilterGenerator(object):

    @staticmethod
    def get_filter(conversion_map_dict_function,filtered_template):
        result_filter = {}
        filtered_template_dict = filtered_template.to_dict()
        for key in filtered_template_dict.keys():
            if filtered_template.get(key):
                result_filter[str(conversion_map_dict_function(key))] = str(filtered_template_dict.get(key))

        return result_filter
