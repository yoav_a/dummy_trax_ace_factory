from Trax.Utils.Logging.Logger import Log


class KpiConverter(object):
    def __init__(self,data_provider):
        self.kpi_static_data = data_provider

    def get_kpi_fk_by_kpi_name(self, kpi_name):
        '''
        convert kpi name to atomic_kpi_fk

        :param kpi_name: string
        :return:
        atomic_kpi_fk

        '''

        assert isinstance(kpi_name, unicode), "name is not a string: %r" % kpi_name
        try:
            return \
            self.kpi_static_data[self.kpi_static_data['atomic_kpi_name'] == kpi_name]['atomic_kpi_fk'].values[0]
        except IndexError:
            Log.info('Kpi name: {}, isnt equal to any kpi name in static table'.format(kpi_name))
            return None