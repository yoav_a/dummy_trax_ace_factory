# -*- coding: utf-8 -*-
import sys
import pandas as pd
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Utils.Logging.Logger import Log
#from Trax.Cloud.Services.Connector.Logger import LoggerInitializer


reload(sys)
sys.setdefaultencoding('UTF8')

__author__ = 'Sanad'
DB = 'db'
EXCEL = 'excel'
TABLE = 'table'
COLUMN = 'column'
SQL = 'sql'
PRAMS = 'prams'
ALL_RIGHT = 'alright'


class TemplateValidator():
    def __init__(self, project_name, helper_path, file_path, validate_file_path):
        self.project = project_name
        self.helper_path = helper_path
        self.file_path = file_path
        self.validate_file_path = validate_file_path
        self.rds_conn = ProjectConnector(self.project, DbUsers.Ps)

    def read_from_DB(self, Db, table, column, elements, limit):
        if elements and type(Db) != float and type(table) != float and type(column) != float:
            select = """select distinct {} from {}.{} where binary {} IN ({}) limit {}""".format(column, Db, table,
                                                                                                 column,
                                                                                                 elements, limit)
            data = pd.read_sql_query(select, self.rds_conn.db)
            return data

        elif elements and type(column) != float and type(table) != float:
            elements_split = elements.split(',')
            if len(elements_split) > 1:
                x = 1
                for element in elements_split:
                    elem = element[1:-2]
                    elem = elem.replace("'", "\\'")
                    select = """select distinct '{}' as name from static_new.product where json_contains(labels, '{{"{}": "{}"}}') limit {}""".format(
                        elem, table, elem, 1)
                    if x == 1:
                        data = pd.read_sql_query(select, self.rds_conn.db)
                        x += 1
                    else:
                        result_data = pd.read_sql_query(select, self.rds_conn.db)
                        data = data.append(result_data, ignore_index=True)

            else:
                elem = elements_split[0][1:-2]
                elem = elem.replace("'", "\\'")
                select = """select distinct '{}' as name from static_new.product where json_contains(labels, '{{"{}": "{}"}}') limit {}""".format(
                    elem, table, elem, limit)
                data = pd.read_sql_query(select, self.rds_conn.db)
            return data

    def get_sheet_names_column(self, path):
        excel = pd.ExcelFile(path)
        sheet_names = excel.sheet_names
        hashMap = dict()
        for sheet in sheet_names:
            parse = excel.parse(sheet)
            hashMap[sheet] = parse.keys()
        return hashMap

    def read_excel_file(self, filePath, sheetname):
        ecxel_helper = pd.read_excel(filePath, sheetname=sheetname)
        return ecxel_helper

    def get_data_by_column(self):
        sheets = self.get_sheet_names_column(self.file_path)
        helper = self.read_excel_file(self.helper_path, SQL)
        validate_dict = dict()
        for sheet in sheets:
            columns = sheets[sheet]
            data = self.read_excel_file(self.file_path, sheet)
            appendData = pd.DataFrame()
            for column in columns:
                row_exist = helper[EXCEL] == column
                row_exist = helper[row_exist].values
                if len(row_exist) > 0:
                    row_data = row_exist[0]
                    if row_data[0] == column:
                        drop_null = data.dropna(subset=[column])
                        list = drop_null[column].unique().tolist()
                        new_list = self.convert_list(list, row_data[4])
                        sql_data = self.read_from_DB(row_data[1], row_data[2], row_data[3], new_list[0], new_list[1])
                        new_column = pd.DataFrame()
                        # add new column
                        new_column[column] = self.validate_data(sql_data, new_list[0], row_data[3])
                        # concat two  dataframe because each column have different size
                        appendData = pd.concat([appendData, new_column], axis=1)
                        # add new row
                        # appendData.loc[len(appendData)] = self.validate_data(sql_data,new_list[0])
            validate_dict[sheet] = appendData
        return validate_dict

    def get_data_by_params_val(self):
        helper = self.read_excel_file(self.helper_path, PRAMS)
        sql_helper = self.read_excel_file(self.helper_path, SQL)
        validate_dict = dict()
        for index, row in helper.iterrows():
            myDict = dict()
            # print row['sheet'], row['params'] , row['val']
            template = self.read_excel_file(self.file_path, row['sheet'])
            template = template.dropna(subset=[row['params'], row['val']])
            for templateIndex, templatRow in template.iterrows():
                params = templatRow[row['params']]
                val = templatRow[row['val']]
                params = str(params).split(',')
                val = str(val).split(',')
                for i in xrange(len(params)):
                    temp = list()
                    key = params[i]
                    value = val[i]
                    if key in myDict:
                        temp = myDict[key]
                        temp.append(value)
                        myDict[key] = temp
                    else:
                        temp.append(value)
                        myDict[key] = temp
            appendData = pd.DataFrame(columns=[row['params'], row['val']])
            for key in myDict.keys():
                myList = myDict[key]
                str_list = self.convert_list(myList, None)
                row_exist = sql_helper[EXCEL] == key
                row_exist = sql_helper[row_exist].values
                if len(row_exist) > 0:
                    row_data = row_exist[0]
                    if row_data[0] == key:
                        sql_data = self.read_from_DB(row_data[1], row_data[2], row_data[3], str_list[0], str_list[1])
                        result = self.validate_data(sql_data, str_list[0], row_data[3])
                        # appendData[row['params']]=key
                        # appendData[row['val']]=str(result)
                        appendData.loc[len(appendData)] = [key, str(result)]
            if row['sheet'] in validate_dict:
                data = validate_dict[row['sheet']]
                newData = pd.concat([data, appendData], axis=1)
                validate_dict[row['sheet']] = newData
            else:
                validate_dict[row['sheet']] = appendData
        return validate_dict

    def get_data_by_excel(self):
        helper = self.read_excel_file(self.helper_path, EXCEL)
        validate_dict = dict()
        for index, row in helper.iterrows():
            sheet = row['sheet']
            column = row['column']
            target_sheet = row['target_sheet']
            target_column = row['target_column']
            sheet_data = self.read_excel_file(self.file_path, sheet)
            drop_null = sheet_data.dropna(subset=[column])
            list = drop_null[column].unique().tolist()
            new_list = self.convert_list(list)
            target_sheet_data = self.read_excel_file(self.file_path, target_sheet)
            target_drop_null = target_sheet_data.dropna(subset=[target_column])
            target_list = target_drop_null[column].unique().tolist()
            target_new_list = self.convert_list(target_list)
            result = self.validate_excel_data(target_new_list[0], new_list[0])
            dataFrame = pd.DataFrame(result, columns=[column])
            if sheet in validate_dict:
                data = validate_dict[sheet]
                newData = pd.concat([data, dataFrame], axis=1)
                validate_dict[sheet] = newData
            else:
                validate_dict[sheet] = dataFrame
        return validate_dict

    def create_validate_excel(self, dataFrame):
        writer = pd.ExcelWriter(self.validate_file_path)

        for frame in dataFrame:
            data = dataFrame.get(frame)
            data.to_excel(writer, frame, index=False)
        writer.save()

    def validate_data(self, sql_data, excel_data, column):

        if not (sql_data is None) and not (excel_data is None):
            excel_data = excel_data.split(',')
            exist = False
            column = str(column)
            if len(sql_data) == len(excel_data):
                done_list = list()
                done_list.append(ALL_RIGHT)
                return done_list
            else:
                faild_list = list()
                sql_data = sql_data[column].values.tolist()
                for e in excel_data:
                    exist = False
                    if len(str(e)) > 0 and ('"' in str(e)[0]):
                        value = str(e).replace('"', '')
                    else:
                        value = str(e).replace("'", '')
                    value = value[:-1]
                    for s in sql_data:
                        if isinstance(s, unicode):
                            s = s.encode('utf8')
                        if len(str(s)) > 0 and ('"' in str(s)[0]):
                            s_value = str(s).replace('"', '')
                        else:
                            s_value = str(s).replace("'", '')
                        s_value.strip()
                        if s_value == value:
                            exist = True
                    if not exist:
                        faild_list.append(value)

            return faild_list

    def validate_excel_data(self, data, excel_data):
        if not (data is None) and not (excel_data is None):
            excel_data = excel_data.split(',')
            data = data.split(',')
            faild_list = list()
            for e in excel_data:
                if e not in data:
                    faild_list.append(e)
            if faild_list:
                faild_list.append('All Right')
        return faild_list

    def convert_list(self, list_elements, split_check):
        str_list = list()
        mySet = set()
        length = None
        for x in list_elements:
            x = x
            if isinstance(x, unicode):
                x = x.encode('utf8')
            if str(x).find('Not') > -1:
                x = str(x).replace('Not', '')

            if str(x) == 'string':
                continue
            if str(x).find(',') == -1:
                value = str(x)
                str_list.append(value)
                continue
            elif split_check != 'no':
                in_list = str(x).split(',')
                str_list.extend(in_list)
        for st in str_list:
            st = st.strip()
            mySet.add(st)
        length = len(str_list)
        string = str()
        for e in mySet:
            if '"' in e:
                temp_string = " '" + e + "' " + ','
            else:
                temp_string = ' "' + e + '" ' + ','
            string += temp_string.strip()
        string = string[:-1]
        return string, length

    def run(self):
        data_by_column = self.get_data_by_column()
        data_by_params = self.get_data_by_params_val()
        data_by_excel = self.get_data_by_excel()
        my_dict = dict()
        for key in data_by_column.keys():
            frame_column = data_by_column[key]
            appendData = frame_column
            if key in data_by_params:
                frame_params = data_by_params[key]
                appendData = pd.concat([frame_column, frame_params], axis=1)
            if key in data_by_excel:
                data = data_by_excel[key]
                appendData = pd.concat([appendData, data], axis=1)
            my_dict[key] = appendData
        self.create_validate_excel(my_dict)
        print '!!!Done!!!!'


# if __name__ == '__main__':
#     # pay attention if there any number column in template like product en please put string in the last column
#     LoggerInitializer.init('test')
#     project_name = 'pngamerica'
#     helper_path = '/home/Israel/Desktop/validation/VAL US/helper.xlsx'
#     file_path = '/home/Israel/Desktop/validation/VAL US/Template_v2.xlsx'
#     validate_file_path = '/home/Israel/Desktop/validation/VAL US/validate_pngamer.xlsx'
#     validation_object = TemplateValidator(project_name, helper_path, file_path, validate_file_path)
#     validation_object.run()
