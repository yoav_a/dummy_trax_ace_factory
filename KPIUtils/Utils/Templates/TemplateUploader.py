import argparse
import datetime
import pandas as pd
from Trax.Cloud.Services.Connector.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Conf.Configuration import Config
from Trax.Utils.Logging.Logger import Log
#from Trax.Cloud.Services.Connector.Logger import LoggerInitializer
import json

SHEET_ASSORTMENT_DEFINITION = 'Granular Groups'
SHEET_STORE_ATTR_TO_ASSORT = 'Granular Groups link to stores'
SHEET_ASSORTMENT_GROUP = 'Super Group'

COLUMN_GRANULAR_GROUP = 'Granular Group Name'
COLUMN_ASSORTMENT_TYPE = 'Assortment type'
COLUMN_EAN_CODE = 'EAN Code'
COLUMN_START_DATE = 'Start date'
COLUMN_END_DATE = 'End date'
COLUMN_TARGET = 'Target'
COLUMN_STORE_ATT_1_NAME = 'Store_Att_1 Name'
COLUMN_STORE_ATT_1_VALUE = 'Store_Att_1 Value'
COLUMN_STORE_ATT_2_NAME = 'Store_Att_2 Name'
COLUMN_STORE_ATT_2_VALUE = 'Store_Att_2 Value'
COLUMN_STORE_ATT_3_NAME = 'Store_Att_3 Name'
COLUMN_STORE_ATT_3_VALUE = 'Store_Att_3 Value'
COLUMN_STORE_ATT_4_NAME = 'Store_Att_4 Name'
COLUMN_STORE_ATT_4_VALUE = 'Store_Att_4 Value'
COLUMN_STORE_ATT_5_NAME = 'Store_Att_5 Name'
COLUMN_STORE_ATT_5_VALUE = 'Store_Att_5 Value'
COLUMN_JSON_POLICY = 'policy'

COLUMNS_ASSORTMENT_DEFINITION_SHEET = [COLUMN_GRANULAR_GROUP,
                                       COLUMN_ASSORTMENT_TYPE,
                                       COLUMN_EAN_CODE,
                                       COLUMN_START_DATE,
                                       COLUMN_END_DATE]

COLUMNS_ATTRS = [[COLUMN_STORE_ATT_1_NAME, COLUMN_STORE_ATT_1_VALUE],
                 [COLUMN_STORE_ATT_2_NAME, COLUMN_STORE_ATT_2_VALUE],
                 [COLUMN_STORE_ATT_3_NAME, COLUMN_STORE_ATT_3_VALUE],
                 [COLUMN_STORE_ATT_4_NAME, COLUMN_STORE_ATT_4_VALUE],
                 [COLUMN_STORE_ATT_5_NAME, COLUMN_STORE_ATT_5_VALUE]]

COLUMNS_STORE_ATTRIBUTES_TO_ASSORT = [COLUMN_GRANULAR_GROUP,
                                      COLUMN_STORE_ATT_1_NAME, COLUMN_STORE_ATT_1_VALUE,
                                      COLUMN_STORE_ATT_2_NAME, COLUMN_STORE_ATT_2_VALUE,
                                      COLUMN_STORE_ATT_3_NAME, COLUMN_STORE_ATT_3_VALUE,
                                      COLUMN_STORE_ATT_4_NAME, COLUMN_STORE_ATT_4_VALUE,
                                      COLUMN_STORE_ATT_5_NAME, COLUMN_STORE_ATT_5_VALUE]

COLUMNS_ASSORTMENT_GROUPS = [COLUMN_ASSORTMENT_TYPE,
                             COLUMN_GRANULAR_GROUP,
                             COLUMN_TARGET,
                             COLUMN_START_DATE,
                             COLUMN_END_DATE]

dtype_dic = {COLUMN_STORE_ATT_1_VALUE: str}
converters = {COLUMN_STORE_ATT_1_VALUE: lambda x: str(x)}

ASSORTMENT_QUERY = "select pk from static.kpi_entity_type where table_name = 'pservice.assortment'"
PRODCUT_QUERY = "select pk from static.kpi_entity_type where table_name = 'static_new.product'"

DICT_ASSORTMENT_GROUP = [
    {
        "assort_type": "MPA",
        "assort_group_name": "MPA ASSORTMENT GROUP"
    },
    {
        "assort_type": "LMPA",
        "assort_group_name": "MPA LOCAL ASSORTMENT GROUP"
    },
    {
        "assort_type": "Distribution",
        "assort_group_name": "Distribution ASSORTMENT GROUP"
    },
    {
        "assort_type": "POSM",
        "assort_group_name": "POSM ASSORTMENT GROUP"
    },
    {
        "assort_type": "NPA",
        "assort_group_name": "NPA ASSORTMENT GROUP"
    },
    {
        "assort_type": "Could Stock",
        "assort_group_name": "Could Stock ASSORTMENT GROUP"
    },
    {
        "assort_type": "Must Stock",
        "assort_group_name": "Must Stock ASSORTMENT GROUP"
    },
    {
        "assort_type": "Must have",
        "assort_group_name": "Must have ASSORTMENT GROUP"
    },
    {
        "assort_type": "Brand Variant",
        "assort_group_name": "Brand Variant ASSORTMENT GROUP"
    },
    {
        "assort_type": "Eye Level",
        "assort_group_name": "Eye Level ASSORTMENT GROUP"
    }

]

LEVEL_2_ASSORTMENT = ['LMPA', 'LDLA', 'LPNA', 'Brand Variant']
LEVEL_3_ASSORTMENT = ['MPA', 'DLA', 'NPA', 'POSM', 'Distribution',
                      'Could Stock', 'Must Stock', 'Must have', 'Eye Level']
SKU_TEMPLATE = ' - SKU'

KPI_FAMILY_FK = 2
KPI_CALCULATION_STAGE_FK = 3


class AssortmentTemplateLoader:
    """
        https://confluence.trax-cloud.com/display/PS/Template
    """

    def __init__(self, project, ass_to_prod=None, ass_to_store=None, ass_to_del=None, start_date='', ass_group=None):
        """
            The constructor, loads the file into global variables, and intiates other
            configuration needed for the execution.
        """
        self.file = None
        self.project = project
        # self.parsed_arguments = AssortmentTemplateLoader.parse_arguments()
        # self.input_file = AssortmentTemplateLoader.read_excel(self.parsed_arguments.file, converters=converters)
        self.assort_def = ass_to_prod
        self.store_attr_to_assort = ass_to_store
        self.assort_group = ass_group
        self.assort_to_product_del = ass_to_del
        self.queries = []
        self.start_date = start_date
        if not self.start_date:
            self.start_date = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        self.end_date = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

    @staticmethod
    def parse_arguments():
        """
            In this method we have all the dedicated arguments this class receives
        :return: parse args object
        """
        parser = argparse.ArgumentParser(description='Loads the Assortment Template file '
                                                     'into the respective DB Schema')
        parser.add_argument('--file', '-f', type=str, required=True,
                            help='Assortment  template file')
        parser.set_defaults(sessions_file=None)
        parser.add_argument('--project', '-p', type=str, required=True, help='The project name')
        parser.add_argument('-e', '--env', type=str, required=True,
                            help='The environment - dev/int/prod')
        parser.add_argument('--no_kpi', dest="no_kpi", action="store_true",
                            help='This flag is used to avoid updates on kpi_level_2')
        parser.set_defaults(no_kpi=False)
        parser.add_argument('--no_clean', dest="no_clean", action="store_true",
                            help='This flag is used to assortment_to_product and'
                                 ' _assortment_to_assortment_group cleanup')
        parser.set_defaults(no_clean=False)
        return parser.parse_args()

    @staticmethod
    def read_excel(file_path, sheet_name=None, converters=None):
        """
            Reads the excel
        :param file_path: File Path
        :param sheet_name: Sheet Name
        :param converters: Used to normalize values in the sheet
        :return:
        """
        return pd.read_excel(file_path, sheetname=sheet_name, converters=converters)

    def create_assortment_policies(self):
        """
        "*****************************************SECTION 1*********************************"
         1.- Store attributes to assortment AUID Code -
            a.- create data in policy table - might be modifies
            b.- validate that all values in Assortment Definition(sheet) column A  exists in
                Store attributes to assortment AUID Code - if there are extra values in
                Assortment Definition(sheet) exit with error msg
            c.- Create static  KPI with  numerator and denominator type fk should be taken
                from kpi_entity_type = pservice.assortment    (if not exists )
                i.-  for MPA's type  - MPA - ASSORTMENT GROUP
                ii.- for Local type - MPA -LOCAL ASSORTMENT GROUP
            d.- Create row in assortment Granular Group Name should be connected to KPI to
                relevant KPI based on Assortment type (if not exists )
        :return:
        """
        df = self.store_attr_to_assort.copy()
        df[COLUMN_JSON_POLICY] = None
        for index, row in df.iterrows():
            df.set_value(index, COLUMN_JSON_POLICY, self.create_json_policy(row))

        assrot_def = self.assort_def.drop_duplicates(
            subset=[COLUMN_GRANULAR_GROUP, COLUMN_ASSORTMENT_TYPE], keep="first")

        merged_df = pd.merge(assrot_def, df, on=COLUMN_GRANULAR_GROUP)

        """ This for validates 1b, after merging bot df we have all the assortment definitions
        with all the policies"""
        policy_queries = []
        static_kpi_queries = []
        assortment_queries = []
        filtered_df = merged_df.drop_duplicates(
            subset=[COLUMN_GRANULAR_GROUP, COLUMN_ASSORTMENT_TYPE], keep="first")

        for index, row in filtered_df.iterrows():
            if not row[COLUMN_JSON_POLICY]:
                raise Exception()
            else:
                assortment_queries.append(self.insert_assortments(row))

        filtered_df = merged_df.drop_duplicates(subset=[COLUMN_GRANULAR_GROUP], keep="first")
        for index, row in filtered_df.iterrows():
            if not row[COLUMN_JSON_POLICY]:
                raise Exception()
            else:
                policy_queries.append(self.insert_into_pservice_policy(row))

        # if not self.parsed_arguments.no_kpi:
        #     assortments_set = set(merged_df[COLUMN_ASSORTMENT_TYPE].tolist())
        #     for assort in assortments_set:
        #         static_kpi_queries.append(self.create_assrotment_groups_in_kpi_level_2(assort))

        self.queries += policy_queries + static_kpi_queries + assortment_queries

    @staticmethod
    def insert_into_pservice_policy(row):
        """
            Creates the insert Query for the DB insertion
        :param row: Data Frame ROW
        :return:
        """
        attr_json = str(row[COLUMN_JSON_POLICY])
        return "INSERT INTO pservice.policy (`policy`,`policy_type`,`policy_name`) " \
               "VALUES ('{policy}', '{policy_type}', '{policy_name}') " \
               "ON DUPLICATE KEY UPDATE " \
               "`policy`=VALUES(`policy`), " \
               "`policy_type`=VALUES(`policy_type`), " \
               "`policy_name`=VALUES(`policy_name`)" \
            .format(policy=attr_json,
                    policy_type=1,
                    policy_name=row[COLUMN_GRANULAR_GROUP])

    @staticmethod
    def create_assrotment_groups_in_kpi_level_2(assortment_type):
        """
            In some cases KPIs needs to be created, as the assortment need to be related to it,
            This method creates this KPI
        :param assortment_type: Assortment type
        :return: SQL Query sentence
        """
        if assortment_type in LEVEL_2_ASSORTMENT:
            client_name = AssortmentTemplateLoader.get_assortment_group_name(assortment_type)
        else:
            client_name = assortment_type

        if not client_name:
            raise Exception()
        return "INSERT IGNORE INTO static.kpi_level_2 " \
               "(`client_name`,`kpi_family_fk`,`version`,`numerator_type_fk`," \
               "`denominator_type_fk`,`initiated_by`,`kpi_calculation_stage_fk`) " \
               "VALUES " \
               "('{client_name}',{kpi_family_fk},''," \
               "({numerator_type_fk}),({numerator_type_fk}),'',3);" \
            .format(client_name=client_name,
                    kpi_family_fk=KPI_FAMILY_FK,
                    numerator_type_fk=ASSORTMENT_QUERY)

    @staticmethod
    def insert_assortments(row):
        """
            Once the KPI is created or updated. the Assrotments should be inserted.
            This fucntion creates the assortment, and relates it to the KPI
        :param row: Data Frame row
        :return: SQL Query sentence
        """
        if row.get(COLUMN_ASSORTMENT_TYPE) in LEVEL_2_ASSORTMENT:
            client_name = AssortmentTemplateLoader.get_assortment_group_name(
                row.get(COLUMN_ASSORTMENT_TYPE))
        else:
            client_name = row.get(COLUMN_ASSORTMENT_TYPE)

        if not client_name:
            raise Exception()

        store_policy_group_fk = "SELECT pk FROM pservice.policy where " \
                                "policy_name = '{policy_name}'". \
            format(policy_name=row[COLUMN_GRANULAR_GROUP])
        kpi_fk = "SELECT pk FROM static.kpi_level_2 where client_name = '{client_name}'". \
            format(client_name=client_name)

        return "INSERT IGNORE INTO pservice.assortment " \
               "(`assortment_name`,`kpi_fk`,`store_policy_group_fk`) " \
               "VALUES ('{assortment_name}',({kpi_fk}),({store_policy_group_fk}))" \
            .format(assortment_name=row[COLUMN_GRANULAR_GROUP],
                    kpi_fk=kpi_fk,
                    store_policy_group_fk=store_policy_group_fk)

    @staticmethod
    def create_json_policy(row):
        """
        :param row: This function Creates the desired JSON
        :return: JSON
        """
        attr_json = {}
        for key in COLUMNS_ATTRS:
            attr_name = row[key][0]
            attr_value = row[key][1]
            if not pd.isnull(attr_name) and not pd.isnull(attr_value):
                attr_name = str(attr_name).replace("Additional Attribute ",
                                                   "additional_attribute_")
                attr_json.update({str(attr_name): map(str.strip, str(attr_value).split(","))})
        return json.dumps(attr_json)

    @staticmethod
    def get_assortment_group_name(assort_type):
        """

        :param assort_type: Return the assortment group name based on the assortment
        received as parameter
        :return: Assortment group name.
        """
        for assortment_conf in DICT_ASSORTMENT_GROUP:
            if assortment_conf.get("assort_type") == assort_type:
                return assortment_conf.get("assort_group_name")

    def create_super_groups(self):
        """
        *****************************************SECTION 2*********************************
        Assortment Groups
            check that all values in coulmn b exists in assortment table
            check that column A exists in KPI table and that numortaor type fk should be taken
            from kpi_entity_type = pservice.assortment  denominator = empty  if not exit
            check that all values in coulmn a exists in assortment table  if not create
            create relation between assortment and assortment group in table
            assortment_and_assortment
            update target if row exists  assortment and assortment group
            remove relation that does not exits in sheet (by adding end date)
        :return: Void
        """
        df = self.assort_group.copy()
        filtered_df = set(df[COLUMN_GRANULAR_GROUP])
        assortment_queries = []
        assortment_to_assortment_queries = []
        for assort_type in filtered_df:
            if assort_type in LEVEL_2_ASSORTMENT:
                assortment_queries.append(self.insert_assortment_without_policy(assort_type))

        for index, row in self.assort_group.iterrows():
            assortment_to_assortment_queries.append(self.insert_assortment_to_assortment(row))

        self.queries += assortment_queries + assortment_to_assortment_queries

    def insert_assortment_to_assortment(self, row):
        """
            For assortment of LMPA type, it relates the assortment with their respective
            assorment group
        :param row: DataFrame row
        :return: SQL Query
        """
        select_assortment_by_name = "SELECT pk FROM pservice.assortment " \
                                    "where assortment_name = '{assortment_name}'"
        assortment_group_fk = select_assortment_by_name.format(
            assortment_name=row[COLUMN_GRANULAR_GROUP])
        assortment_fk = "SELECT pk FROM pservice.assortment " \
                        "where assortment_name = '{assortment_name}' and kpi_fk =" \
                        "(select pk from static.kpi_level_2 " \
                        "where client_name = '{kpi_name}') ". \
            format(assortment_name=row[COLUMN_ASSORTMENT_TYPE],
                   kpi_name=self.get_assortment_group_name(row[COLUMN_GRANULAR_GROUP]))

        return "INSERT IGNORE INTO pservice.assortment_to_assortment_group" \
               " (`assortment_group_fk`,`assortment_fk`,`target`,`start_date`) " \
               "VALUES (({assortment_group_fk}),({assortment_fk}),{target},'{start_date}' )" \
            .format(assortment_group_fk=assortment_group_fk,
                    assortment_fk=assortment_fk,
                    target=row[COLUMN_TARGET],
                    start_date=self.start_date)

    @staticmethod
    def insert_assortment_without_policy(assort_type):
        """
            For assroment of LMPA type, and assorment like "LMPA" should be created
            into the assorment table, and this should not be related to any KPI.
            This function creates this assroment into the DB
        :param assort_type: Assrotment type
        :return: SQL query
        """
        kpi_fk = "SELECT pk FROM static.kpi_level_2 where client_name = '{client_name}'". \
            format(client_name=assort_type)
        return "INSERT IGNORE INTO pservice.assortment (`assortment_name`,`kpi_fk`) " \
               "VALUES ('{assortment_name}',({kpi_fk}))" \
            .format(assortment_name=assort_type,
                    kpi_fk=kpi_fk)

    def create_granular_groups(self):
        """
                *****************************************SECTION 3*********************************
        Assortment Definition
        check that Assortment type  exists in KPI  (column b)
        Granular Group Name exists in Assortment table and is related to a KPI  (column A)
        for Assortment type  exists in MPA , DLA, NPA ,POSM then :
        create row in assortment products based on column B and C
        create create relation between assortment and assortment group in table
        assortment_and_assortment if does not exits

        for Assortment type  exists in LMPA , LDLA, LPNA then :
        check that exists KPI with name of Assortment type column B - if not exit
        Assortment Name should be connected to KPI of MPA -LOCAL ASSORTMENT GROUP
        create static KPI (lvl3) with  numortaor  = (kpi_entity_type = static_new.product)
          and denominator type fk should be taken from kpi_entity_type = pservice.assortment
             name should be LMPA - SKU
        create sku assortment (lvl3) for  the new KPI (Baileys 01 -sku)
        create relation between assortment and assortment group in table
        assortment_and_assortment (lvl 2 -> lvl3 )
        assign assortment to product where new kpi assigned  new sku assortment
        ( product -> lvl3 assortment)

        :return: VOID
        """
        level_3_queries = []
        level_2_queries = []

        assortment_type = self.assort_def.drop_duplicates(
            subset=[COLUMN_ASSORTMENT_TYPE], keep="first")
        granular_assortment = self.assort_def.drop_duplicates(
            subset=[COLUMN_GRANULAR_GROUP, COLUMN_ASSORTMENT_TYPE], keep="first")

        # if not self.parsed_arguments.no_kpi:
        #     for index, row in assortment_type.iterrows():
        #         if row[COLUMN_ASSORTMENT_TYPE] in LEVEL_2_ASSORTMENT:
        #             level_2_queries.append(self.create_sku_kpi_level_2(row))
        #         elif row[COLUMN_ASSORTMENT_TYPE] in LEVEL_3_ASSORTMENT:
        #             level_3_queries.append(self.create_sku_kpi_level_2(row))

        for index, row in granular_assortment.iterrows():
            if row[COLUMN_ASSORTMENT_TYPE] in LEVEL_2_ASSORTMENT:
                level_2_queries.append(self.create_sku_assortments(row))
                level_2_queries.append(self.relate_assort_level2_to_level3(row))
            elif row[COLUMN_ASSORTMENT_TYPE] in LEVEL_3_ASSORTMENT:
                level_3_queries.append(self.create_sku_assortments(row))
                level_3_queries.append(self.relate_assort_level2_to_level3(row))

        for index, row in self.assort_def.iterrows():
            if row[COLUMN_ASSORTMENT_TYPE] in LEVEL_2_ASSORTMENT:
                level_2_queries.append(self.insert_assortment_to_product(row))
            elif row[COLUMN_ASSORTMENT_TYPE] in LEVEL_3_ASSORTMENT:
                level_3_queries.append(self.insert_assortment_to_product(row))

        self.queries += level_2_queries + level_3_queries

    def create_sku_kpi_level_2(self, row):
        """
        :param row:
        :return:
        """
        kpi_name = self.append_sku_template_to_name(row[COLUMN_ASSORTMENT_TYPE])

        return "INSERT IGNORE INTO static.kpi_level_2 " \
               "(`client_name`,`kpi_family_fk`,`version`,`numerator_type_fk`," \
               "`denominator_type_fk`,`initiated_by`,`kpi_calculation_stage_fk`) " \
               "VALUES " \
               "('{client_name}',{kpi_family_fk},''," \
               "({numerator_type_fk}),({denominator_type_fk}),'',3);" \
            .format(client_name=kpi_name,
                    kpi_family_fk=KPI_FAMILY_FK,
                    denominator_type_fk=ASSORTMENT_QUERY,
                    numerator_type_fk=PRODCUT_QUERY)

    def create_sku_assortments(self, row):
        """
            Creates the SKU assorment.
        :param row: DF row
        :return: SQL Query
        """
        assortment_name_sku = self.append_sku_template_to_name(row[COLUMN_GRANULAR_GROUP])
        kpi_name = self.append_sku_template_to_name(row[COLUMN_ASSORTMENT_TYPE])

        kpi_fk = "SELECT pk FROM static.kpi_level_2 where client_name = '{client_name}'". \
            format(client_name=kpi_name)
        return "INSERT IGNORE INTO pservice.assortment (`assortment_name`,`kpi_fk`) " \
               "VALUES ('{assortment_name}',({kpi_fk}))" \
            .format(assortment_name=assortment_name_sku,
                    kpi_fk=kpi_fk)

    def insert_assortment_to_product(self, row):
        """
            Relates the SKU Assroment, to the product.
        :param row: Data Frame row
        :return: SQL Query
        """
        assortment_name = self.append_sku_template_to_name(row[COLUMN_GRANULAR_GROUP])
        select_assortment_by_name = "SELECT pk FROM pservice.assortment " \
                                    "where assortment_name = '{assortment_name}' and kpi_fk =" \
                                    "(select pk from static.kpi_level_2 where " \
                                    "client_name = '{kpi_name}') ". \
            format(assortment_name=assortment_name,
                   kpi_name=self.append_sku_template_to_name(row[COLUMN_ASSORTMENT_TYPE]))

        product_fk = "select pk from static_new.product where ean_code =\'{ean_code}\' and " \
                     "(is_active=1 and delete_date is null)" \
            .format(ean_code=row[COLUMN_EAN_CODE])

        return "INSERT IGNORE INTO pservice.assortment_to_product " \
               "(`assortment_fk`,`product_fk`,`start_date`) " \
               "VALUES " \
               "(({assort_name}),({product_fk}),'{start_date}');".format(
            assort_name=select_assortment_by_name,
            product_fk=product_fk,
            start_date=row[COLUMN_START_DATE])

    def relate_assort_level2_to_level3(self, row):
        """
            Creates the relation between the assrotment.
            For LMPA it creates Baileys SKU to Baileys
        :param row: Data Frame Row
        :return: SQL Query
        """

        select_assortment_group_by_name = "SELECT pk FROM pservice.assortment " \
                                          "where assortment_name = '{assortment_name}' " \
                                          "and kpi_fk =(select pk from static.kpi_level_2 " \
                                          "where client_name = '{kpi_name}' )"

        if row[COLUMN_ASSORTMENT_TYPE] in LEVEL_2_ASSORTMENT:
            kpi_name = AssortmentTemplateLoader. \
                get_assortment_group_name(row[COLUMN_ASSORTMENT_TYPE])
        else:
            kpi_name = row[COLUMN_ASSORTMENT_TYPE]
        assortment_group_fk = select_assortment_group_by_name. \
            format(assortment_name=row[COLUMN_GRANULAR_GROUP],
                   kpi_name=kpi_name)
        assortment_name_sku = self.append_sku_template_to_name(row[COLUMN_GRANULAR_GROUP])
        assortment_fk = select_assortment_group_by_name. \
            format(assortment_name=assortment_name_sku,
                   kpi_name=self.append_sku_template_to_name(row[COLUMN_ASSORTMENT_TYPE]))
        return "INSERT IGNORE INTO pservice.assortment_to_assortment_group" \
               " (`assortment_group_fk`,`assortment_fk`,`start_date`) " \
               "VALUES (({assortment_group_fk}),({assortment_fk}),'{start_date}' )" \
            .format(assortment_group_fk=assortment_group_fk,
                    assortment_fk=assortment_fk,
                    start_date=self.start_date)

    @staticmethod
    def append_sku_template_to_name(name):
        """
            This method attach a sufix to the name pased as parameter
        :param name: Name to attach (- SKU to it)
        :return: String
        """
        return str(name) + SKU_TEMPLATE

    def excecute_queries(self):
        """
            This is the method that loops into the queries array and excecutes them.
        :return: VOID
        """
        file_warning = open("warning.log", "w")
        file_error = open("error.log", "w")
        rds_conn = ProjectConnector(self.project, DbUsers.CalculationEng)
        try:
            cursor = rds_conn.db.cursor()
            index = 0
            for query in self.queries:
                index += 1
                try:
                    res = cursor.execute(str(query))
                    if res == 0:
                        file_warning.write(query)
                        Log.warning(query)
                except Exception as e:
                    file_error.write(query)
                    Log.error(e)
                if index == 1000:
                    rds_conn.db.commit()
                    index = 0
            rds_conn.db.commit()
        except Exception as e:
            Log.error(e)
        finally:
            rds_conn.disconnect_rds()

    def encode_dataframes(self):
        """
            Template, in some cases can contain nos regular Ascii characters, so this method is in
            charge to encode them into UNICODE in order to avoid errors.
        :return:
        """
        self.assort_def[COLUMN_GRANULAR_GROUP] = map(lambda x: x.encode('utf-8', 'strict'),
                                                     self.assort_def[COLUMN_GRANULAR_GROUP])

        self.store_attr_to_assort[COLUMN_GRANULAR_GROUP] = map(lambda x: x.encode('utf-8', 'strict'),
                                                               self.store_attr_to_assort[COLUMN_GRANULAR_GROUP])
        self.store_attr_to_assort[COLUMN_STORE_ATT_5_VALUE] = map(lambda x: x.encode('utf-8', 'strict'),
                                                                  self.store_attr_to_assort[COLUMN_STORE_ATT_5_VALUE])
        self.assort_group[COLUMN_GRANULAR_GROUP] = map(lambda x: x.encode('utf-8', 'strict'),
                                                       self.assort_group[COLUMN_GRANULAR_GROUP])
        self.assort_group[COLUMN_ASSORTMENT_TYPE] = map(lambda x: x.encode('utf-8', 'strict'),
                                                        self.assort_group[COLUMN_ASSORTMENT_TYPE])

    def delete_from_ass(self):
        """
            This method sets end_date to for
            - assortment_to_product
            - assortment_to_assortment_group
        :return: VOID
        """
        clean_up_queries = []
        for index, row in self.assort_to_product_del.iterrows():
            product_fk = "select pk from static_new.product where ean_code ='{}' and " \
                         "(is_active=1 and delete_date is null)".format(row[COLUMN_EAN_CODE])
            assortment_fk = "select pk from pservice.assortment where assortment_name = '{}'" \
                .format(row[COLUMN_GRANULAR_GROUP] + ' - SKU')
            clean_up_queries.append("update pservice.assortment_to_product set end_date = '{}' where product_fk = "
                                    "({}) and assortment_fk = ({})".format(self.start_date, product_fk, assortment_fk))
        # clean_up_queries.append("update pservice.assortment_to_assortment_group " \
        #                         "set end_date = '{}' where end_date is null".format(self.start_date))
        self.queries += clean_up_queries

    def remove_products_null(self):
        """
            This method sets start_date to the visit_date for
            - assortment_to_product
        :return: VOID
        """
        query = ["delete from pservice.assortment_to_product where product_fk = 0"]
        # clean_up_queries.append("update pservice.assortment_to_assortment_group " \
        #                         "set end_date = '{}' where end_date is null".format(self.start_date))
        self.queries += query

    def clean_up_tables(self):
        """
            This method sets end_date to for
            - assortment_to_product
            - assortment_to_assortment_group
        :return: VOID
        """
        clean_up_queries = []
        for index, row in self.assort_def.iterrows():
            product_fk = "select pk from static_new.product where ean_code ='{}' and " \
                         "(is_active=1 and delete_date is null)".format(row[COLUMN_EAN_CODE])
            assortment_fk = "select pk from pservice.assortment where assortment_name = '{}'" \
                .format(row[COLUMN_GRANULAR_GROUP] + ' - SKU')
            clean_up_queries.append("update pservice.assortment_to_product set end_date = '{}' where product_fk = "
                                    "({}) and assortment_fk = ({})".format(self.end_date, product_fk, assortment_fk))
        self.queries += clean_up_queries

    def upload_assortment_template(self):
        # self.encode_dataframes()
        self.clean_up_tables()
        self.create_assortment_policies()
        if self.assort_group is not None:
            self.create_super_groups()
        self.create_granular_groups()
        if self.assort_to_product_del is not None and not self.assort_to_product_del.empty:
            self.delete_from_ass()
        self.remove_products_null()
        self.excecute_queries()

    def main(self):
        """
            This is the main method, here all the phases are executed.
        :return: VOID
        """
        Log.info("Loading template file")
        self.upload_assortment_template()


"""
    This is the excec method
"""
# if __name__ == '__main__':
#     Config.init()
#     LoggerInitializer.init("Assortment Template")
#     assortment = AssortmentTemplateLoader()
#     assortment.main()