from Trax.Utils.Logging.Logger import Log
from KPIUtils.GlobalProjects.JNJ.Utils.KPIToolBox import JNJToolBox, log_runtime

__author__ = 'Shani'


class JNJGenerator:

    def __init__(self, data_provider, output, common):
        self.data_provider = data_provider
        self.output = output
        self.project_name = data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.common = common
        self.tool_box = JNJToolBox(self.data_provider, self.output, self.common)

    @log_runtime('Total Calculations', log_start=True)
    def secondary_placement_location_quality(self, survey_template):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        try:
            if self.tool_box.scif.empty:
                Log.warning('Scene item facts is empty for this session')
            self.tool_box.secondary_placement_location_quality(survey_template)
        except Exception as err:
            Log.error('{}'.format(err))

    def secondary_placement_location_visibility_quality(self, survey_template):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        try:
            if self.tool_box.scif.empty:
                Log.warning('Scene item facts is empty for this session')
            self.tool_box.secondary_placement_location_visibility_quality(survey_template)
        except Exception as err:
            Log.error('{}'.format(err))

    def calculate_auto_assortment(self):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        try:
            if self.tool_box.scif.empty:
                Log.warning('Scene item facts is empty for this session')
            else:
                self.tool_box.assortment_per_store_calc(2)
        except Exception as err:
            Log.error('{}'.format(err))

    def promo_calc(self):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        try:
            if self.tool_box.scif.empty:
                Log.warning('Scene item facts is empty for this session')
            else:
                self.tool_box.promo_calc()
        except Exception as err:
            Log.error('{}'.format(err))

    def eye_hand_level_sos_calculation(self):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        try:
            if self.tool_box.scif.empty:
                Log.warning('Scene item facts is empty for this session')
            else:
                self.tool_box.eye_hand_level_sos_calculation()
        except Exception as err:
            Log.error('{}'.format(err))
