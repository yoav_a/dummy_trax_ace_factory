from datetime import datetime
import json
import pandas as pd
import os
import numpy as np
from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Cloud.Services.Connector.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Logging.Logger import Log
import KPIUtils.Utils.Templates.TemplateUploader as assTemplate
from Trax.Data.Utils.MySQLservices import get_table_insertion_query as insert
# from KPIUtils.GlobalDataProvider.PsDataProvider import PsDataProvider
from KPIUtils_v2.GlobalDataProvider.PsDataProvider import PsDataProvider
from KPIUtils.GlobalProjects.JNJ.Utils.GeneralToolBox import JNJGENERALToolBox
# from KPIUtils.Calculations.Assotment import Assortment
from KPIUtils.Calculations.Assortment import Assortment

__author__ = 'Shani'

KPI_RESULT = 'report.kpi_results'
KPK_RESULT = 'report.kpk_results'
KPS_RESULT = 'report.kps_results'
KPI_NEW_TABLE = 'report.kpi_level_2_results'


def log_runtime(description, log_start=False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            calc_start_time = datetime.utcnow()
            if log_start:
                Log.info('{} started at {}'.format(description, calc_start_time))
            result = func(*args, **kwargs)
            calc_end_time = datetime.utcnow()
            Log.info('{} took {}'.format(description, calc_end_time - calc_start_time))
            return result

        return wrapper

    return decorator


class JNJToolBox:

    SP_LOCATION_KPI = 'secondary placement location quality'
    SP_LOCATION_QUALITY_KPI = 'secondary placement location visibility quality'
    LVL3_HEADERS = ['assortment_group_fk', 'assortment_fk', 'target', 'product_fk',
                         'in_store', 'kpi_fk_lvl1', 'kpi_fk_lvl2', 'kpi_fk_lvl3',
                         'group_target_date', 'assortment_super_group_fk', 'super_group_target']
    LVL2_HEADERS = ['assortment_super_group_fk', 'assortment_group_fk', 'assortment_fk',
                         'target', 'passes', 'total', 'kpi_fk_lvl1', 'kpi_fk_lvl2',
                         'group_target_date', 'super_group_target']
    LVL1_HEADERS = ['assortment_super_group_fk', 'assortment_group_fk',
                         'super_group_target', 'passes', 'total', 'kpi_fk_lvl1']
    ASSORTMENT_FK = 'assortment_fk'
    ASSORTMENT_GROUP_FK = 'assortment_group_fk'
    ASSORTMENT_SUPER_GROUP_FK = 'assortment_super_group_fk'
    EYE_LVL_BY_BAY_SIZE = {4: [8, 9], 3: [5, 6, 7], 2: [4], 1: [3]}
    # GROCERY_CONVENIENCE_DISCOUNTERS_WHOLESALE = [{'store_type': 'Grocery'}, {'store_type': 'Convenience'},
    #                                              {'store_type': 'Discounters'}, {'store_type': 'Wholesale'}]
    # BOOTS_SUPERDRUG_PHARMACY = [{'retailer': 'Boots'}, {'retailer': 'Superdrug'}, {'store_type': 'Pharmacy'}]
    EYE_HAND_LVL_KPI = 'eye_hand_level_sos'
    JNJ = 'JOHNSON & JOHNSON'
    PRODUCT_PRESENCE_IN_CATEGORY_CUSTOM = 'PRODUCT_PRESENCE_IN_CATEGORY_CUSTOM'
    DST_BY_CATEGORY = 'DST_BY_CATEGORY'
    TYPE_SKU = 'SKU'
    STORE_ASSORT = 'store_assortment - SKU'
    OOS_KPI = 'OOS_BY_DYNAMIC_ASSORT'
    OOS_CAT_KPI = 'OOS_BY_CATEGORY'


    def __init__(self, data_provider, output, common):
        self.output = output
        self.data_provider = data_provider
        self.project_name = self.data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.session_id = self.data_provider.session_id
        self.products = self.data_provider[Data.PRODUCTS]
        self.all_products = self.data_provider[Data.ALL_PRODUCTS]
        self.match_product_in_scene = self.data_provider[Data.MATCHES]
        self.visit_date = self.data_provider[Data.VISIT_DATE]
        self.session_info = self.data_provider[Data.SESSION_INFO]
        self.scene_info = self.data_provider[Data.SCENES_INFO]
        self.store_id = self.data_provider[Data.STORE_FK]
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.templates = self.data_provider[Data.ALL_TEMPLATES]
        self.survey_response = self.data_provider[Data.SURVEY_RESPONSES]
        self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.tools = JNJGENERALToolBox(self.data_provider, self.output, rds_conn=self.rds_conn)
        self.ps_data_provider = PsDataProvider(self.data_provider, self.output)
        self.common = common
        self.New_kpi_static_data = common.get_new_kpi_static_data()
        self.kpi_results_new_tables_queries = []
        self.all_products = self.ps_data_provider.get_sub_category(self.all_products, 'sub_category_local_name')
        self.store_assortment = self.ps_data_provider.store_ass
        self.store_info = self.data_provider[Data.STORE_INFO]
        self.store_info = self.ps_data_provider.get_ps_store_info(self.store_info)
        self.current_date = datetime.now()
        self.store_sos_policies = self.ps_data_provider.get_store_policies()
        self.labels = self.ps_data_provider.get_labels()
        self.product_promotion = self.ps_data_provider.get_products_in_promotion()
        self.products_to_ass = pd.DataFrame(columns=assTemplate.COLUMNS_ASSORTMENT_DEFINITION_SHEET)
        self.assortment_policy = pd.DataFrame(columns=assTemplate.COLUMNS_STORE_ATTRIBUTES_TO_ASSORT)
        self.ass_deleted_prod = pd.DataFrame(columns=[assTemplate.COLUMN_GRANULAR_GROUP, assTemplate.COLUMN_EAN_CODE])
        self.assortment = Assortment(self.data_provider, self.output, self.ps_data_provider)
        self.products_to_remove = []
        self.ignore_from_top = 1
        self.start_shelf = 3

    def get_df_from_excel_path(self, template_path):
        """
        this method finds the template path and converts it to Data frame
        :return: Data frame
        """
        # path to template file
        excel_path = template_path+'/Data/SurveyTemplate.xlsx'
        # convert template to Data frame
        excel_df = pd.read_excel(excel_path, sheetname='Sheet1')
        return excel_df

    def secondary_placement_location_quality(self, survey_template):
        self.secondary_placement_quality_generic('Gold location', survey_template)

    def secondary_placement_location_visibility_quality(self, survey_template):
        self.secondary_placement_quality_generic('branded, visible & accessible', survey_template)

    def secondary_placement_quality_generic(self, survey_param, survey_template):
        """
            This function calculates the following:
            # of J&J secondary placement (SP) of the @param survey_param / Total # of J&J SP
        """
        survey_numerator = 0
        survey_denominator = 0
        for index, row in self.survey_response.iterrows():
            try:
                question = survey_template.loc[survey_template['Question_Text'] == row['question_text']]
                if question['Reference'].values[0] == survey_param:
                    survey_numerator += row['number_value']
                elif question['Reference'].values[0] == 'Total Secondary Placements':
                    survey_denominator += row['number_value']
            except IndexError:
                Log.info("The question: {} doesn't exist in Data/SurveyTemplate.xlsx".format(row['question_text']))
                continue
        # Writing results to DB:
        kpi_fk = 0
        if survey_param == 'Gold location':
            kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.SP_LOCATION_KPI]['pk'].values[0]
        elif survey_param == 'branded, visible & accessible':
            kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.SP_LOCATION_QUALITY_KPI]['pk'].values[0]
        survey_result = survey_numerator/survey_denominator if survey_denominator != 0 else 0
        if survey_result > 1:
            survey_result = 1
        self.common.write_to_db_result_new_tables(fk=kpi_fk, numerator_id=999, numerator_result=survey_numerator,
                                                  denominator_id=999, denominator_result=survey_denominator,
                                                  result=survey_result, score=survey_result)
        return

    def calculate_auto_assortment(self, visits_to_count):
        Log.info("starting calculate_auto_assortment")
        assTemplate.LEVEL_3_ASSORTMENT.append('store_level_assortment')
        last_visits = self.ps_data_provider.get_store_last_x_visit_date(visits_to_count)
        num_of_visits = len(last_visits)
        last_x_visit_date = last_visits
        products_to_ignore = self.store_assortment[(self.store_assortment['assortment_name'] == 'store_fk_' +
                                                   str(self.store_id) + ' - SKU') & (self.store_assortment['start_date']
                                                                                     == self.visit_date)]['ean_code'].drop_duplicates().values
        products_in_store = self.all_products[(self.all_products['product_fk'].isin(
            self.match_product_in_scene['product_fk'].drop_duplicates().values)) & (self.all_products['product_fk'].isin(
            self.scif[self.scif['manufacturer_name'] == self.JNJ]['product_fk'].drop_duplicates().values)) &
                                              (self.all_products['product_type'] == self.TYPE_SKU)]['product_ean_code'].drop_duplicates().values
        self.products_to_ass[assTemplate.COLUMN_EAN_CODE] = list(set(products_in_store) - set(products_to_ignore))
            # return
        if num_of_visits != 0:
        # else:
            products_in_ass = self.all_products[self.all_products['product_fk'].isin(
                self.store_assortment[self.store_assortment['assortment_name'] == 'store_fk_' + str(self.store_id) +
                                      ' - SKU']['product_fk'].drop_duplicates().values)]['product_ean_code'].\
                drop_duplicates().values
            products_in_ass_not_store = list(set(products_in_ass) - set(products_in_store))
            # products_in_store_not_ass = list(set(products_in_store) - set(products_in_ass))
            # self.products_to_ass[assTemplate.COLUMN_EAN_CODE] = list(set(products_in_store))
            if num_of_visits > 1:
                for product in products_in_ass_not_store:
                    product_fk = self.all_products[self.all_products['product_ean_code'] == product]['product_fk'].\
                        drop_duplicates().values[0]
                    product_ass_date = self.store_assortment[(self.store_assortment['product_fk'] ==
                                                             product_fk) & (self.store_assortment['assortment_name'] ==
                                                             'store_fk_' + str(self.store_id) + ' - SKU')]['start_date']
                    if datetime.date(datetime.utcfromtimestamp((product_ass_date.values[0] -
                                            np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's'))) < \
                            last_x_visit_date.values[1][0]:
                        self.products_to_remove.append(product)
                self.ass_deleted_prod[assTemplate.COLUMN_EAN_CODE] = self.products_to_remove
                self.ass_deleted_prod[assTemplate.COLUMN_GRANULAR_GROUP] = 'store_fk_' + str(self.store_id)
        self.products_to_ass[assTemplate.COLUMN_GRANULAR_GROUP] = 'store_fk_' + str(self.store_id)
        self.products_to_ass[assTemplate.COLUMN_ASSORTMENT_TYPE] = 'store_level_assortment'
        self.products_to_ass[assTemplate.COLUMN_START_DATE] = self.visit_date
        self.assortment_policy[assTemplate.COLUMN_GRANULAR_GROUP] = ['store_fk_' + str(self.store_id)]
        self.assortment_policy[assTemplate.COLUMN_STORE_ATT_1_NAME] = 'store_fk'
        self.assortment_policy[assTemplate.COLUMN_STORE_ATT_1_VALUE] = self.store_id
        assortment_template_loader = assTemplate.AssortmentTemplateLoader(self.project_name, self.products_to_ass,
                                                                          self.assortment_policy, self.ass_deleted_prod,
                                                                          self.visit_date)
        assortment_template_loader.upload_assortment_template()
        Log.info("finishing calculate_auto_assortment")

    def assortment_per_store_calc(self, num_of_visits):
        self.calculate_auto_assortment(num_of_visits)
        Log.info("starting assortment_per_store_calc")
        lvl3_result = self.assortment.calculate_lvl3_assortment()
        self.assortment_per_category(lvl3_result)
        kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] ==
                                          self.STORE_ASSORT]['pk'].drop_duplicates().values
        for result in lvl3_result.itertuples():
            score = result.in_store * 100
            self.common.write_to_db_result_new_tables(result.kpi_fk_lvl3, result.product_fk, result.in_store,
                                                      score, result.assortment_group_fk, 1, score)
            if kpi_fk.size != 0:
                self.common.write_to_db_result_new_tables(kpi_fk[0], result.product_fk, result.in_store,
                                                          score, self.store_id, 1, score)
        if not lvl3_result.empty:
            jnj_fk = self.scif[self.scif['manufacturer_name'] == self.JNJ]['manufacturer_fk']. \
                drop_duplicates().values[0]
            oos_kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.OOS_KPI]['pk'].values[0]
            if oos_kpi_fk.size != 0:
                oos_numerator = len(lvl3_result[lvl3_result['in_store'] == 0])
                denominator = len(lvl3_result['in_store'])
                oos_res = np.divide(float(oos_numerator), float(denominator))
                self.common.write_to_db_result_new_tables(oos_kpi_fk, jnj_fk, oos_numerator, oos_res,
                                                   denominator_result=denominator,
                                                   score=oos_res)
            lvl2_result = self.assortment.calculate_lvl2_assortment(lvl3_result)
            for result in lvl2_result.itertuples():
                denominator_res = result.total
                if result.target and result.group_target_date <= self.current_date:
                    denominator_res = result.target
                res = np.divide(float(result.passes), float(denominator_res))
                if res >= 1:
                    score = 100
                else:
                    score = 0
                self.common.write_to_db_result_new_tables(result.kpi_fk_lvl2, jnj_fk,
                                                          result.passes,
                                                          res, None, denominator_res,
                                                          score)
        Log.info("finishing assortment_per_store_calc")
        return

    def assortment_per_category(self, lvl3_result):
        Log.info("starting assortment_per_category")
        categories = self.all_products['category_fk'].drop_duplicates().values
        kpi_fk_lvl_3 = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] ==
                                          self.PRODUCT_PRESENCE_IN_CATEGORY_CUSTOM]['pk'].drop_duplicates().values[0]
        kpi_fk_lvl_2 = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] ==
                                                self.DST_BY_CATEGORY]['pk'].drop_duplicates().values[0]
        for category in categories:
            products_in_cat = self.all_products[self.all_products['category_fk'] == category]['product_fk'].drop_duplicates().values
            relevant_for_ass = lvl3_result[lvl3_result['product_fk'].isin(products_in_cat)]
            for result in relevant_for_ass.itertuples():
                score = result.in_store * 100
                self.common.write_to_db_result_new_tables(kpi_fk_lvl_3, result.product_fk, result.in_store,
                                                          score, category, 1, score)
            if not relevant_for_ass.empty:
                jnj_fk = self.scif[self.scif['manufacturer_name'] == self.JNJ]['manufacturer_fk']. \
                    drop_duplicates().values[0]
                oos_kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.OOS_CAT_KPI]['pk'].values[0]
                if oos_kpi_fk.size != 0:
                    oos_numerator = len(relevant_for_ass[relevant_for_ass['in_store'] == 0])
                    denominator = len(relevant_for_ass['in_store'])
                    oos_res = np.divide(float(oos_numerator), float(denominator))
                    self.common.write_to_db_result_new_tables(oos_kpi_fk, jnj_fk, oos_numerator, oos_res, category,
                                                              denominator,
                                                              oos_res)
                lvl2_result = self.assortment.calculate_lvl2_assortment(relevant_for_ass)
                for result in lvl2_result.itertuples():
                    denominator_res = result.total
                    if result.target and result.group_target_date <= self.current_date:
                        denominator_res = result.target
                    res = np.divide(float(result.passes), float(denominator_res))
                    if res >= 1:
                        score = 100
                    else:
                        score = 0

                    self.common.write_to_db_result_new_tables(kpi_fk_lvl_2, jnj_fk,
                                                              result.passes,
                                                              res, category, denominator_res,
                                                              score)
        Log.info("finishing assortment_per_category")
        return

    def promo_calc(self):
        lvl3_assortment = self.assortment.get_lvl3_relevant_ass()
        products_in_session = self.product_promotion[self.product_promotion['is_promotion'] == 1]['product_fk'].values
        if products_in_session.size != 0:
            lvl3_assortment.loc[lvl3_assortment['product_fk'].isin(products_in_session), 'in_store'] = 1
        for result in lvl3_assortment.itertuples():
            score = result.in_store * 100
            self.common.write_to_db_result_new_tables(result.kpi_fk_lvl3, result.product_fk, result.in_store,
                                                      score, result.assortment_group_fk, 1, score)
        if not lvl3_assortment.empty:
            lvl2_res = pd.DataFrame(columns=self.LVL2_HEADERS)
            assortments = lvl3_assortment[[self.ASSORTMENT_GROUP_FK, self.ASSORTMENT_FK,
                                           self.ASSORTMENT_SUPER_GROUP_FK]].drop_duplicates(subset=
                                                                                            [self.ASSORTMENT_GROUP_FK,
                                                                                             self.ASSORTMENT_FK,
                                                                                             self.ASSORTMENT_SUPER_GROUP_FK],
                                                                                            keep='last')
            filters = self.LVL2_HEADERS
            if 'passes' in filters:
                filters.remove('passes')
            if 'total' in filters:
                filters.remove('total')
            lvl2_res[filters] = lvl3_assortment[filters]
            lvl2_res = lvl2_res.drop_duplicates(subset=[self.ASSORTMENT_SUPER_GROUP_FK,
                                                        self.ASSORTMENT_GROUP_FK, 'target'],
                                                keep='last')
            for ass in assortments.itertuples():
                lvl2_res.loc[(lvl2_res[self.ASSORTMENT_FK] == ass.assortment_fk) &
                             (lvl2_res[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk), 'passes'] = \
                    len(lvl3_assortment[(lvl3_assortment[self.ASSORTMENT_FK] == ass.assortment_fk) &
                                        (lvl3_assortment[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk) &
                                        (lvl3_assortment['in_store'] == 1)])
                lvl2_res.loc[(lvl2_res[self.ASSORTMENT_FK] == ass.assortment_fk) &
                             (lvl2_res[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk), 'total'] = \
                    len(lvl3_assortment[(lvl3_assortment[self.ASSORTMENT_FK] == ass.assortment_fk) &
                                        (lvl3_assortment[self.ASSORTMENT_GROUP_FK] == ass.assortment_group_fk)])
            for result in lvl2_res.itertuples():
                denominator_res = result.total
                if result.target and result.group_target_date <= self.current_date:
                    denominator_res = result.target
                res = np.divide(float(result.passes), float(denominator_res)) * 100
                if res >= 100:
                    score = 100
                else:
                    score = 0
                self.common.write_to_db_result_new_tables(result.kpi_fk_lvl2, result.assortment_group_fk,
                                                          result.passes,
                                                          res, result.assortment_super_group_fk, denominator_res,
                                                          score)

    def calc_eye_level_shelves(self, eye_level_length):
        end_shelf = self.start_shelf + eye_level_length
        final_shelves = range(self.start_shelf, end_shelf)
        return final_shelves

    def eye_hand_level_sos_calculation(self):
        relevant_products = self.scif[self.scif['manufacturer_name'] == self.JNJ]['product_fk'].drop_duplicates().values
        bays = self.match_product_in_scene['bay_number'].drop_duplicates().values
        numerator_val = 0
        for bay in bays:
            total_num_of_shelves = self.match_product_in_scene[self.match_product_in_scene['bay_number'] == bay]['shelf_number_from_bottom'].max()
            if total_num_of_shelves < 3:
                continue
            # if store_att in self.GROCERY_CONVENIENCE_DISCOUNTERS_WHOLESALE and 4 <= total_num_of_shelves <= 5:
            #     eye_level_length = 1
            elif total_num_of_shelves >= 10:
                eye_level_length = 5
            else:
                eye_level_length = [key for key, value in self.EYE_LVL_BY_BAY_SIZE.iteritems() if total_num_of_shelves
                                    in value][0]
            shelves_in_eye_lvl = self.calc_eye_level_shelves(eye_level_length)
            linear_eye_hand_lvl_sos = self.match_product_in_scene[(self.match_product_in_scene['bay_number'] == bay) &
                                                                  (self.match_product_in_scene['shelf_number_from_bottom']
                .isin(shelves_in_eye_lvl)) & (self.match_product_in_scene['product_fk']
                .isin(relevant_products)) & (self.match_product_in_scene['stacking_layer'] == 1)]['width_mm_advance'].sum()
            numerator_val += linear_eye_hand_lvl_sos
        denominator_val = self.match_product_in_scene[(self.match_product_in_scene['product_fk']
                .isin(relevant_products)) & (self.match_product_in_scene['stacking_layer'] == 1)]['width_mm'].sum()
        res = np.divide(float(numerator_val), float(denominator_val)) * 100
        kpi_fk_lvl2 = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.EYE_HAND_LVL_KPI]['pk'].values[0]
        jnj_fk = self.scif[self.scif['manufacturer_name'] == self.JNJ]['manufacturer_fk']. \
            drop_duplicates().values[0]
        self.common.write_to_db_result_new_tables(kpi_fk_lvl2, jnj_fk, numerator_val,
                                                  res, denominator_result=denominator_val,
                                                  score=res)
        return




    def get_product_fk(self, product_ean_code):
        """
            This function gets product_fk by the product_ean_code.
            """
        product_fk = self.all_products[self.all_products['product_ean_code'] == product_ean_code]['product_fk']
        if product_fk.empty:
            return None
        return product_fk.values[0]
