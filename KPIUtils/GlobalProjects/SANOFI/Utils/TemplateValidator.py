__author__ = 'yoava'

import os
from KPIUtils.Utils.Validators.Template.TemplateValidator import TemplateValidator
from Trax.Utils.Logging.Logger import Log
#from Trax.Cloud.Services.Connector.Logger import LoggerInitializer
import datetime


# return the excel helper file path
def get_helper_path():
    return "~/dev/trax_ace_factory/KPIUtils/GlobalProjects/SANOFI/Utils/helper.xlsx"


class SanofiTemplateValidator:

    def __init__(self, path):
        self.path = path

    # return current project name in lowercase
    # assuming that all sanofi libraries sorted like this : SANOFI -> DATA -> TemlplateValidator ,
    # this method will get the project name
    def get_project_name(self):
        path_lst = self.path.split("/")
        return path_lst[len(path_lst) - 2].lower()

    # return the excel template file path
    def get_template_path(self):
        return os.path.join(self.path, "Template.xlsx")

    @staticmethod
    # create new directory for validation file
    def make_dir_for_validate_file():
        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        os.makedirs(os.path.join("validations", str(date)))
        return os.path.join("validations", str(date))

    def run(self):
        # pay attention if there any number column in template like product en please put string in the last column
        Log.init('start template validate')
        project_name = self.get_project_name()
        helper_path = get_helper_path()
        template_path = self.get_template_path()
        validate_dir = self.make_dir_for_validate_file()
        validate_file_path = os.path.join(validate_dir, "validate.xlsx")
        # run template validator
        TemplateValidator(project_name, helper_path, template_path, validate_file_path).run()
