from datetime import datetime
import pandas as pd
from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Logging.Logger import Log
from Trax.Data.Utils.MySQLservices import get_table_insertion_query as insert
from KPIUtils.GlobalProjects.SANOFI.Utils.Fetcher import SANOFIQueries
from KPIUtils.GlobalProjects.SANOFI.Utils.GeneralToolBox import SANOFIGENERALToolBox
from KPIUtils.GlobalProjects.SANOFI.Utils.ParseTemplates import parse_template


__author__ = 'Shani'

KPI_RESULT = 'report.kpi_results'
KPK_RESULT = 'report.kpk_results'
KPS_RESULT = 'report.kps_results'
KPI_NEW_TABLE = 'report.kpi_level_2_results'


def log_runtime(description, log_start=False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            calc_start_time = datetime.utcnow()
            if log_start:
                Log.info('{} started at {}'.format(description, calc_start_time))
            result = func(*args, **kwargs)
            calc_end_time = datetime.utcnow()
            Log.info('{} took {}'.format(description, calc_end_time - calc_start_time))
            return result
        return wrapper
    return decorator


class SANOFIToolBox:

    LEVEL1 = 1
    LEVEL2 = 2
    LEVEL3 = 3

    SET_NAME = 'Perfect Store Compliance'

    def __init__(self, data_provider, output, template_path):
        self.output = output
        self.data_provider = data_provider
        self.project_name = self.data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.session_id = self.data_provider.session_id
        self.products = self.data_provider[Data.PRODUCTS]
        self.all_products = self.data_provider[Data.ALL_PRODUCTS]
        self.match_product_in_scene = self.data_provider[Data.MATCHES]
        self.visit_date = self.data_provider[Data.VISIT_DATE]
        self.session_info = self.data_provider[Data.SESSION_INFO]
        self.scene_info = self.data_provider[Data.SCENES_INFO]
        self.store_id = self.data_provider[Data.STORE_FK]
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.templates = self.data_provider[Data.ALL_TEMPLATES]
        self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.tools = SANOFIGENERALToolBox(self.data_provider, self.output, rds_conn=self.rds_conn)
        self.kpi_static_data = self.get_kpi_static_data()
        self.New_kpi_static_data = self.get_new_kpi_static_data()
        self.kpi_results_queries = []
        self.kpi_results_new_tables_queries = []
        self.template_data = parse_template(template_path, 'KPIs')
        self.msl_data = parse_template(template_path, 'MSL', lower_headers_row_index=1)
        self.Primary_shelf_location_data = parse_template(template_path, 'Primary Shelf_Location',
                                                          lower_headers_row_index=1)
        self.Primary_brand_blocking_data = parse_template(template_path, 'Primary_Brand_Blocking',
                                                          lower_headers_row_index=1)
        self.Primary_Secondary_POSM_data = parse_template(template_path, 'Primary&Secondary_POSM',
                                                          lower_headers_row_index=1)
        self.Primary_Secondary_Facings_data = parse_template(template_path, 'Primary&Secondary_Facings',
                                                             lower_headers_row_index=1)
        self.store_info = self.data_provider[Data.STORE_INFO]
        self.store_type = self.store_info['store_type'].values[0]

    def get_kpi_static_data(self):
        """
        This function extracts the static KPI data and saves it into one global data frame.
        The data is taken from static.kpi / static.atomic_kpi / static.kpi_set.
        """
        query = SANOFIQueries.get_all_kpi_data()
        kpi_static_data = pd.read_sql_query(query, self.rds_conn.db)
        return kpi_static_data

    def get_new_kpi_static_data(self):
        """
            This function extracts the static new KPI data (new tables) and saves it into one global data frame.
            The data is taken from static.kpi_level_2.
            """
        query = SANOFIQueries.get_new_kpi_data()
        kpi_static_data = pd.read_sql_query(query, self.rds_conn.db)
        return kpi_static_data

    def calculate_perfect_store(self):
        """
        This function calculates the KPI results.
        """

        set_results = {'result': 0, 'target': 0}
        result_2 = None
        main_children = self.template_data[self.template_data['KPI Group'] == 'Perfect Store']
        for c in xrange(len(main_children)):
            main_results = {'result': 0, 'target': 0}
            main_child = main_children.iloc[c]
            children = self.template_data[self.template_data['KPI Group'] == main_child['Tested Group']]
            for i in xrange(len(children)):
                child = children.iloc[i]
                kpi_type = child['KPI Type']
                if kpi_type == 'Shelf Level Per Brand':
                    results = self.calculate_shelf_level(child)
                elif kpi_type == 'Product Availability Per SKU':
                    results = self.calculate_availability_per_sku(child)
                elif kpi_type == 'Blocked Together Per Brand':
                    results = self.calculate_block_together_skus(child)
                else:
                    Log.warning("KPI of type '{}' is not supported".format(kpi_type.encode('utf8')))
                    continue
                if results:
                    main_results['result'] += results['result']
                    main_results['target'] += results['target']
                    atomic_fk = self.get_atomic_fk(main_child, child)
                    kpi_fk_new_table = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] ==
                                                                child['KPI Name']]['pk'].values[0]
                    result = float(results['result'])
                    target = results['target']
                    if target:
                        score = (result/target)*100
                    else:
                        score = 0
                    if child['KPI Group'] == 'MSL':
                        result_2 = 1-score
                        self.write_to_db_result(atomic_fk, (result, score, result, target, result_2), level=self.LEVEL3)
                    else:
                        self.write_to_db_result(atomic_fk, (result, score, result, target, result_2), level=self.LEVEL3)
                        self.write_to_db_result_new_tables(kpi_fk_new_table, numerator_id=None, numerator_result=result,
                                                           denominator_id=None, denominator_result=target,
                                                           result=result, score=score)
            kpi_name = main_child['KPI Name']
            kpi_fk = self.kpi_static_data[self.kpi_static_data['kpi_name'] == kpi_name]['kpi_fk'].values[0]
            kpi_fk_new_table = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] ==
                                                        kpi_name]['pk'].values[0]
            set_results['result'] += main_results['result']
            set_results['target'] += main_results['target']
            main_result = float(main_results['result'])
            main_target = main_results['target']
            if main_target:
                main_score = (main_result/main_target)*100
                self.write_to_db_result(kpi_fk, (main_score, main_result, main_target), level=self.LEVEL2)
                self.write_to_db_result_new_tables(kpi_fk_new_table, numerator_id=None, numerator_result=main_result,
                                                   denominator_id=None, denominator_result=main_target,
                                                   result=main_result, score=main_score)

        set_result = float(set_results['result'])
        set_target = set_results['target']
        kpi_fk_new_table = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] ==
                                                    self.SET_NAME]['pk'].values[0]
        if set_target:
            set_score = (set_result / set_target)*100
            set_fk = self.kpi_static_data.loc[self.kpi_static_data['kpi_fk'] == kpi_fk]['kpi_set_fk'].values[0]
            self.write_to_db_result(set_fk, (set_result, set_score, set_result, set_target), level=self.LEVEL1)
            self.write_to_db_result_new_tables(kpi_fk_new_table, numerator_id=None, numerator_result=set_result,
                                               denominator_id=None, denominator_result=set_target, result=set_result,
                                               score=set_score)

        return

    def calculate_shelf_level(self, kpi_data):
        """
            This function calculates if the product wasn't recognized on top/ bottom shelf
        """
        results = {}
        sheet_name = kpi_data['Sheet']
        template_group = kpi_data['Template Group']
        match_product_includes_ean = self.match_product_in_scene.merge(self.products, on=['product_fk'])
        match_product_includes_scene_fk = match_product_includes_ean.merge(self.scene_info, on=['scene_fk'])
        matches = match_product_includes_scene_fk.merge(self.templates, on=['template_fk'])
        data = self.get_relevant_data_per_kpi(kpi_data['KPI Name'], sheet_name)
        if not data.empty:
            results = {'result': 0, 'target': 0}
            products_list = data['Product EAN Code'].tolist()
            for product in products_list:
                product_fk = self.get_product_fk(product)
                if product_fk:
                    filters_1 = {'product_ean_code': product, 'shelf_number': 1, 'template_group': template_group}
                    result_1 = self.tools.calculate_availability(matches, **filters_1)
                    filters_2 = {'product_ean_code': product, 'shelf_number_from_bottom': 1,
                                 'template_group': template_group}
                    result_2 = self.tools.calculate_availability(matches, **filters_2)
                    kpi_fk = self.get_level_4(kpi_data)
                    if result_1 + result_2 == 0:
                        results['result'] += 1
                        self.write_to_db_result_new_tables(kpi_fk, numerator_id=product_fk, numerator_result=1,
                                                           denominator_id=None, denominator_result=None, result=1,
                                                           score=100)
                    else:
                        self.write_to_db_result_new_tables(kpi_fk, numerator_id=product_fk, numerator_result=0,
                                                           denominator_id=None, denominator_result=None, result=0,
                                                           score=0)
                    results['target'] += 1

        return results

    def calculate_availability_per_sku(self, kpi_data):
        """
            This function calculates availability of specifc SKU or POSM
        """
        results = {}
        filters = {}
        sheet_name = kpi_data['Sheet']
        if kpi_data['Template Group']:
            template_group = kpi_data['Template Group']
            filters = {'template_group': template_group}
        data = self.get_relevant_data_per_kpi(kpi_data['KPI Name'], sheet_name)
        if not data.empty:
            results = {'result': 0, 'target': 0}
            products_list = data['Product EAN Code'].tolist()
            for product in products_list:
                product_fk = self.get_product_fk(product)
                if product_fk:
                    filters['product_ean_code'] = str(product)
                    result = self.tools.calculate_availability(data=None, **filters)
                    kpi_fk = self.get_level_4(kpi_data)
                    if result >= int(str(data[data['Product EAN Code'] == product][self.store_type].values[0])):
                        results['result'] += 1
                        self.write_to_db_result_new_tables(kpi_fk, numerator_id=product_fk, numerator_result=1,
                                                           denominator_id=None, denominator_result=None, result=1,
                                                           score=100)
                    else:
                        self.write_to_db_result_new_tables(kpi_fk, numerator_id=product_fk, numerator_result=0,
                                                           denominator_id=None, denominator_result=None, result=0,
                                                           score=0)
                    results['target'] += 1
        return results

    def calculate_block_together_skus(self, kpi_data):
        """
        This function calculates every block-together-typed KPI from the relevant sets, and returns the set final score.
        """
        filters = {}
        results = {}
        sheet_name = kpi_data['Sheet']
        if kpi_data['Template Group']:
            template_group = kpi_data['Template Group']
            filters = {'template_group': template_group}
        data = self.get_relevant_data_per_kpi(kpi_data['KPI Name'], sheet_name)
        if not data.empty:
            results = {'result': 0, 'target': 0}
            products_list = data['Product EAN Code'].tolist()
            kpi_fk = self.get_level_4(kpi_data)
            for products in products_list:
                valid_products = 0
                products_for_block_check = products.split(",")
                result = self.tools.calculate_block_together(product_ean_code=products_for_block_check,
                                                             **filters)
                if result == 1:
                    results['result'] += 1
                    for product in products_for_block_check:
                        product_fk = self.get_product_fk(product)
                        if product_fk:
                            valid_products = 1
                            self.write_to_db_result_new_tables(kpi_fk, numerator_id=product_fk, numerator_result=1,
                                                               denominator_id=None, denominator_result=None, result=1,
                                                               score=100)
                else:
                    for product in products_for_block_check:
                        product_fk = self.get_product_fk(product)
                        if product_fk:
                            valid_products = 1
                            self.write_to_db_result_new_tables(kpi_fk, numerator_id=product_fk, numerator_result=0,
                                                               denominator_id=None, denominator_result=None,
                                                               result=0, score=0)
                if valid_products:
                    results['target'] += 1
        return results

    def get_relevant_data_per_kpi(self, kpi_name, sheet_name):
        """
            This function return the relevant sheet from template with the relevant store_type data to check
        """
        data = pd.DataFrame()
        try:
            if sheet_name == 'Primary Shelf_Location':
                data = self.Primary_shelf_location_data.loc[(self.Primary_shelf_location_data[self.store_type] != '0') & (
                    self.Primary_shelf_location_data['KPI Name'] == kpi_name)]
            elif sheet_name == 'Primary&Secondary_POSM':
                data = self.Primary_Secondary_POSM_data.loc[(self.Primary_Secondary_POSM_data[self.store_type] != '0') & (
                    self.Primary_Secondary_POSM_data['KPI Name'] == kpi_name)]
                # todo: update the template with the posm products
            elif sheet_name == 'Primary&Secondary_Facings':
                data = self.Primary_Secondary_Facings_data.loc[
                    (self.Primary_Secondary_Facings_data[self.store_type] != '0') & (
                        self.Primary_Secondary_Facings_data['KPI Name'] == kpi_name)]
            elif sheet_name == 'Primary_Brand_Blocking':
                data = self.Primary_brand_blocking_data.loc[
                    (self.Primary_brand_blocking_data[self.store_type] != '0') & (
                        self.Primary_brand_blocking_data['KPI Name'] == kpi_name)]
            elif sheet_name == 'MSL':
                data = self.msl_data.loc[
                    (self.msl_data[self.store_type] != '0') & (
                        self.msl_data['KPI Name'] == kpi_name)]
        except:
            Log.error('Store type "{}" is not set to calculation'.format(
                self.store_type))  # todo add all supported store types
        return data

    def get_atomic_fk(self, pillar, params):
        """
        This function gets an Atomic KPI's FK out of the template data.
        """
        atomic_name = params['KPI Name']
        kpi_name = pillar['KPI Name']
        atomic_fk = self.kpi_static_data[(self.kpi_static_data['kpi_name'] == kpi_name) &
                                         (self.kpi_static_data['atomic_kpi_name'] == atomic_name)]['atomic_kpi_fk']
        if atomic_fk.empty:
            return None
        return atomic_fk.values[0]

    def get_level_4(self, params):
        """
        This function gets the KPI's FK of the forth level (kpi by SKU) from the new tables.
        """
        kpi_name = str(params['KPI Name'])+' By SKU'
        kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == kpi_name]['pk']
        if kpi_fk.empty:
            return None
        return kpi_fk.values[0]

    def get_product_fk(self, product_ean_code):
        """
            This function gets product_fk by the product_ean_code.
            """
        product_fk = self.all_products[self.all_products['product_ean_code'] == product_ean_code]['product_fk']
        if product_fk.empty:
            return None
        return product_fk.values[0]

    def write_to_db_result(self, fk, score, level):
        """
        This function creates the result data frame of every KPI (atomic KPI/KPI/KPI set),
        and appends the insert SQL query into the queries' list, later to be written to the DB.
        """
        attributes = self.create_attributes_dict(fk, score, level)
        if level == self.LEVEL1:
            table = KPS_RESULT
        elif level == self.LEVEL2:
            table = KPK_RESULT
        elif level == self.LEVEL3:
            table = KPI_RESULT
        else:
            return
        query = insert(attributes, table)
        self.kpi_results_queries.append(query)

    def write_to_db_result_new_tables(self, fk, numerator_id, numerator_result, denominator_id, denominator_result,
                                      result, score):
        """
            This function creates the result data frame of new rables KPI,
            and appends the insert SQL query into the queries' list, later to be written to the DB.
            """
        table = KPI_NEW_TABLE
        attributes = self.create_attributes_dict_new_tables(fk, numerator_id, numerator_result, denominator_id,
                                                            denominator_result, result, score)
        query = insert(attributes, table)
        self.kpi_results_queries.append(query)

    def create_attributes_dict(self, fk, score, level):
        """
        This function creates a data frame with all attributes needed for saving in KPI results tables.

        """
        if level == self.LEVEL1:
            kps_result, score_1, score_2, score_3 = score
            kpi_set_name = self.kpi_static_data[self.kpi_static_data['kpi_set_fk'] == fk]['kpi_set_name'].values[0]
            attributes = pd.DataFrame([(kpi_set_name, self.session_uid, self.store_id, self.visit_date.isoformat(),
                                        kps_result, format(score_1, '.2f'), score_2, score_3, fk)],
                                      columns=['kps_name', 'session_uid', 'store_fk', 'visit_date', 'kps_result',
                                               'score_1', 'score_2', 'score_3', 'kpi_set_fk'])
        elif level == self.LEVEL2:
            score, score_2, score_3 = score
            kpi_name = self.kpi_static_data[self.kpi_static_data['kpi_fk'] == fk]['kpi_name'].values[0]
            attributes = pd.DataFrame([(self.session_uid, self.store_id, self.visit_date.isoformat(),
                                        fk, kpi_name, score, score_2, score_3)],
                                      columns=['session_uid', 'store_fk', 'visit_date', 'kpi_fk', 'kpk_name',
                                               'score', 'score_2', 'score_3'])
        elif level == self.LEVEL3:
            result, score, score_2, target, result_2 = score
            data = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]
            atomic_kpi_name = data['atomic_kpi_name'].values[0]
            kpi_fk = data['kpi_fk'].values[0]
            kpi_set_name = self.kpi_static_data[self.kpi_static_data['atomic_kpi_fk'] == fk]['kpi_set_name'].values[0]
            attributes = pd.DataFrame([(atomic_kpi_name, self.session_uid, kpi_set_name, self.store_id,
                                        self.visit_date.isoformat(), datetime.utcnow().isoformat(),
                                        kpi_fk, fk, result, score, score_2, target, result_2)],
                                      columns=['display_text', 'session_uid', 'kps_name', 'store_fk', 'visit_date',
                                               'calculation_time', 'kpi_fk', 'atomic_kpi_fk', 'result', 'score',
                                               'score_2', 'threshold', 'result_2'])
        else:
            attributes = pd.DataFrame()
        return attributes.to_dict()

    def create_attributes_dict_new_tables(self, kpi_fk, numerator_id, numerator_result, denominator_id,
                                          denominator_result, result, score):
        """
        This function creates a data frame with all attributes needed for saving in KPI results new tables.
        """
        attributes = pd.DataFrame([(kpi_fk,  self.session_id, numerator_id, numerator_result, denominator_id,
                                    denominator_result, result, score)], columns=['kpi_level_2_fk', 'session_fk',
                                                                                  'numerator_id', 'numerator_result',
                                                                                  'denominator_id',
                                                                                  'denominator_result', 'result',
                                                                                  'score'])
        return attributes.to_dict()

    @log_runtime('Saving to DB')
    def commit_results_data(self):
        """
        This function writes all KPI results to the DB, and commits the changes.
        """
        insert_queries = self.merge_insert_queries(self.kpi_results_queries)
        cur = self.rds_conn.db.cursor()
        delete_queries = SANOFIQueries.get_delete_session_results_query(self.session_uid, self.session_id)
        for query in delete_queries:
            cur.execute(query)
        for query in insert_queries:
            cur.execute(query)
        self.rds_conn.db.commit()

    @staticmethod
    def merge_insert_queries(insert_queries):
        query_groups = {}
        for query in insert_queries:
            static_data, inserted_data = query.split('VALUES ')
            if static_data not in query_groups:
                query_groups[static_data] = []
            query_groups[static_data].append(inserted_data)
        merged_queries = []
        for group in query_groups:
            merged_queries.append('{0} VALUES {1}'.format(group, ',\n'.join(query_groups[group])))
        return merged_queries
