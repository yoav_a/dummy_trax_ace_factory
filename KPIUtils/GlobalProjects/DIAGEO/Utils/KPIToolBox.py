from datetime import datetime
import json
import pandas as pd
import numpy as np
from Trax.Algo.Calculations.Core.DataProvider import Data
from Trax.Utils.Conf.Keys import DbUsers
from Trax.Data.Projects.Connector import ProjectConnector
from Trax.Utils.Logging.Logger import Log
from Trax.Data.Utils.MySQLservices import get_table_insertion_query as insert
from KPIUtils.GlobalDataProvider.PsDataProvider import PsDataProvider
from KPIUtils.GlobalProjects.DIAGEO.Utils.Fetcher import DIAGEOQueries
from KPIUtils.GlobalProjects.DIAGEO.Utils.GeneralToolBox import DIAGEOGENERALToolBox
from KPIUtils.Calculations.Assortment import Assortment
from KPIUtils.DB.Common import Common

__author__ = 'Shani'

KPI_RESULT = 'report.kpi_results'
KPK_RESULT = 'report.kpk_results'
KPS_RESULT = 'report.kps_results'
KPI_NEW_TABLE = 'report.kpi_level_2_results'


def log_runtime(description, log_start=False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            calc_start_time = datetime.utcnow()
            if log_start:
                Log.info('{} started at {}'.format(description, calc_start_time))
            result = func(*args, **kwargs)
            calc_end_time = datetime.utcnow()
            Log.info('{} took {}'.format(description, calc_end_time - calc_start_time))
            return result

        return wrapper

    return decorator


class DIAGEOToolBox:
    LEVEL1 = 1
    LEVEL2 = 2
    LEVEL3 = 3
    LVL3_HEADERS = ['assortment_group_fk', 'assortment_fk', 'target', 'product_fk',
                    'in_store', 'kpi_fk_lvl1', 'kpi_fk_lvl2', 'kpi_fk_lvl3', 'group_target_date',
                    'assortment_super_group_fk']
    LVL2_HEADERS = ['assortment_group_fk', 'assortment_fk', 'target', 'passes', 'total',
                    'kpi_fk_lvl1', 'kpi_fk_lvl2', 'group_target_date']
    LVL1_HEADERS = ['assortment_group_fk', 'target', 'passes', 'total', 'kpi_fk_lvl1']
    ASSORTMENT_FK = 'assortment_fk'
    ASSORTMENT_GROUP_FK = 'assortment_group_fk'
    ASSORTMENT_SUPER_GROUP_FK = 'assortment_super_group_fk'
    BRAND_VARIENT = 'brand_varient'
    NUMERATOR = 'numerator'
    DENOMINATOR = 'denominator'
    DISTRIBUTION_KPI = 'Distribution - SKU'
    OOS_SKU_KPI = 'OOS - SKU'
    OOS_KPI = 'OOS'

    def __init__(self, data_provider, output, common):
        self.common = common
        self.New_kpi_static_data = common.get_new_kpi_static_data()
        self.output = output
        self.data_provider = data_provider
        self.project_name = self.data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.session_id = self.data_provider.session_id
        self.products = self.data_provider[Data.PRODUCTS]
        self.all_products = self.data_provider[Data.ALL_PRODUCTS]
        self.match_product_in_scene = self.data_provider[Data.MATCHES]
        self.visit_date = self.data_provider[Data.VISIT_DATE]
        self.session_info = self.data_provider[Data.SESSION_INFO]
        self.scene_info = self.data_provider[Data.SCENES_INFO]
        self.store_id = self.data_provider[Data.STORE_FK]
        self.scif = self.data_provider[Data.SCENE_ITEM_FACTS]
        self.templates = self.data_provider[Data.ALL_TEMPLATES]
        self.rds_conn = ProjectConnector(self.project_name, DbUsers.CalculationEng)
        self.tools = DIAGEOGENERALToolBox(self.data_provider, self.output, rds_conn=self.rds_conn)
        self.ps_data_provider = PsDataProvider(self.data_provider, self.output)
        self.kpi_results_new_tables_queries = []
        self.all_products = self.ps_data_provider.get_sub_category(self.all_products)
        self.store_assortment = self.ps_data_provider.get_store_assortment()
        self.store_sos_policies = self.ps_data_provider.get_store_policies()
        self.labels = self.ps_data_provider.get_labels()
        self.store_info = self.data_provider[Data.STORE_INFO]
        self.store_info = self.ps_data_provider.get_ps_store_info(self.store_info)
        self.current_date = datetime.now()
        self.assortment = Assortment(self.data_provider, self.output)

    def main_assortment_calculation(self):
        """
        This function calculates the KPI results.
        """
        lvl3_result = self.assortment.calculate_lvl3_assortment()
        dist_kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.DISTRIBUTION_KPI]['pk'].values[0]
        for result in lvl3_result.itertuples():
            score = result.in_store * 100
            self.common.write_to_db_result_new_tables(result.kpi_fk_lvl3, result.product_fk, result.in_store,
                                               score, result.assortment_group_fk, 1, score)
        dist_results = lvl3_result[lvl3_result['kpi_fk_lvl3'] == dist_kpi_fk]
        for res in dist_results.itertuples():
            kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.OOS_SKU_KPI]['pk'].values[
                0]
            parent_kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.OOS_KPI]['pk'].values[0]
            is_oos = 0
            if not res.in_store:
                is_oos = 1
            self.common.write_to_db_result_new_tables(kpi_fk, res.product_fk, is_oos, is_oos, parent_kpi_fk, 1, is_oos)
        if not lvl3_result.empty:
            oos_kpi_fk = self.New_kpi_static_data[self.New_kpi_static_data['client_name'] == self.OOS_KPI]['pk'].values[0]
            oos_numerator = len(dist_results[dist_results['in_store'] == 0])
            denominator = len(dist_results['in_store'])
            oos_res = np.divide(float(oos_numerator), float(denominator)) * 100
            self.common.write_to_db_result_new_tables(oos_kpi_fk, oos_kpi_fk, oos_numerator, oos_res,
                                               denominator_result=denominator,
                                               score=oos_res)
            lvl2_result = self.assortment.calculate_lvl2_assortment(lvl3_result)
            for result in lvl2_result.itertuples():
                denominator_res = result.total
                if result.target and result.group_target_date <= self.current_date:
                    denominator_res = result.target
                res = np.divide(float(result.passes), float(denominator_res)) * 100
                if res >= 100:
                    score = 100
                else:
                    score = 0
                self.common.write_to_db_result_new_tables(result.kpi_fk_lvl2, result.assortment_group_fk, result.passes,
                                               res, result.assortment_super_group_fk, denominator_res, score)
            if not lvl2_result.empty:
                lvl1_result = self.assortment.calculate_lvl1_assortment(lvl2_result)
                for result in lvl1_result.itertuples():
                    denominator_res = result.super_group_target
                    if not result.super_group_target:
                        denominator_res = result.total
                    res = np.divide(float(result.passes), float(denominator_res)) * 100
                    if res >= 100:
                        score = 100
                    else:
                        score = 0
                    self.common.write_to_db_result_new_tables(fk=result.kpi_fk_lvl1, numerator_id=result.assortment_super_group_fk,
                                                   numerator_result=result.passes, denominator_result=denominator_res,
                                                   result=res, score=score)
        return

    def main_sos_calculation(self):
        relevant_stores = pd.DataFrame(columns=self.store_sos_policies.columns)
        for row in self.store_sos_policies.itertuples():
            policies = json.loads(row.store_policy)
            df = self.store_info
            for key, value in policies.items():
                try:
                    df = df[df[key].isin(value)]
                except KeyError:
                    continue
            if not df.empty:
                stores = self.store_sos_policies[(self.store_sos_policies['store_policy'] == row.store_policy)
                                      & (self.store_sos_policies['target_validity_start_date'] <= datetime.date(self.current_date))]
                if stores.empty:
                    relevant_stores = stores
                else:
                    relevant_stores = relevant_stores.append(stores, ignore_index=True)
        relevant_stores = relevant_stores.drop_duplicates(subset=['kpi', 'sku_name', 'target', 'sos_policy'], keep='last')
        for row in relevant_stores.itertuples():
            sos_policy = json.loads(row.sos_policy)
            numerator_key = sos_policy[self.NUMERATOR].keys()[0]
            denominator_key = sos_policy[self.DENOMINATOR].keys()[0]
            numerator_val = sos_policy[self.NUMERATOR][numerator_key]
            denominator_val = sos_policy[self.DENOMINATOR][denominator_key]
            if numerator_key == 'manufacturer':
                numerator_key = numerator_key + '_name'
            numerator = self.scif[(self.scif[numerator_key] == numerator_val) & (self.scif[denominator_key] ==
                                                                                 denominator_val)]['facings'].sum()
            denominator = self.scif[self.scif[denominator_key] == denominator_val]['facings'].sum()
            if numerator_key == self.BRAND_VARIENT:
                numerator_id = self.labels[(self.labels['label_name'] == self.BRAND_VARIENT) &
                                           (self.labels['label_value'] == numerator_val)]['pk'].values[0]
            else:
                numerator_id = self.all_products[self.all_products[numerator_key] == numerator_val][numerator_key.split
                                                                                            ('_')[0] + '_fk'].values[0]
            denominator_id = self.all_products[self.all_products[denominator_key] == denominator_val][denominator_key +
                                                                                                      '_fk'].values[0]
            sos = 0
            if numerator and denominator:
                sos = np.divide(float(numerator), float(denominator))*100
            score = 0
            target = row.target*100
            if sos >= target:
                score = 100
            self.common.write_to_db_result_new_tables(fk=row.kpi, numerator_id=numerator_id, numerator_result=numerator,
                                           denominator_id=denominator_id, denominator_result=denominator,
                                           result=sos, score=score, denominator_result_after_actions=target)
        return

    def get_product_fk(self, product_ean_code):
        """
            This function gets product_fk by the product_ean_code.
            """
        product_fk = self.all_products[self.all_products['product_ean_code'] == product_ean_code]['product_fk']
        if product_fk.empty:
            return None
        return product_fk.values[0]
