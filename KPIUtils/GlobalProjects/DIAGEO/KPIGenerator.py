
from Trax.Utils.Logging.Logger import Log
from KPIUtils.GlobalProjects.DIAGEO.Utils.KPIToolBox import DIAGEOToolBox, log_runtime
from KPIUtils.DB.Common import Common

__author__ = 'Shani'


class DIAGEOGenerator:

    def __init__(self, data_provider, output, common):
        self.data_provider = data_provider
        self.output = output
        # self.template=template
        self.project_name = data_provider.project_name
        self.session_uid = self.data_provider.session_uid
        self.common = common
        self.tool_box = DIAGEOToolBox(self.data_provider, self.output, self.common)

    @log_runtime('Total Calculations', log_start=True)
    def diageo_global_assortment_function(self):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        try:
            # Log.info('In KPI generator DIAGEOTW-SAND')
            if self.tool_box.scif.empty:
                Log.warning('Scene item facts is empty for this session')
            self.tool_box.main_assortment_calculation()
            # self.common.commit_results_data_to_new_tables()
        except Exception as e:
            Log.error('{}'.format(e))

    @log_runtime('Total Calculations', log_start=True)
    def diageo_global_share_of_shelf_function(self):
        """
        This is the main KPI calculation function.
        It calculates the score for every KPI set and saves it to the DB.
        """
        try:
            # Log.info('In KPI generator DIAGEOTW-SAND')
            if self.tool_box.scif.empty:
                Log.warning('Scene item facts is empty for this session')
            self.tool_box.main_sos_calculation()
            # self.common.commit_results_data_to_new_tables()
        except Exception as e:
            Log.error('{}'.format(e))
